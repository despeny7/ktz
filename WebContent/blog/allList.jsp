<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">

</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>个人博客
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>blog/add.html" style="float: right;">+&nbsp;添加博客</a>
						</s:if>
					</div>
				</div>
				<div class="items">
				<s:if test="null==list||0==list.size()">
					<div class="norecord">
						暂时没有发表任何博客！
					</div>
				</s:if>
				<s:else>
					<s:iterator value="list" id="item">
						<div class="item" id="n<s:property value="#item.id" />">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td style="width: 80%">
										<span class="title">
											<a href="<%=CnLang.BASEPATH%>blog/info/<s:property value="#item.id" />.html"><s:property value="#item.title" /></a>
										</span>
									</td>
									<td style="width: 20%; text-align: right;">
										<span>
											<a href="<%=CnLang.BASEPATH%>blog/info/<s:property value="#item.id" />.html">详情<em class="gtgt">&gt;&gt;</em>
											</a> 
										</span>
									</td>
								</tr>
							</table>
						</div>
					</s:iterator>
					<%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						String u = CnLang.BASEPATH + "blog/allList";
						u = u + "/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>