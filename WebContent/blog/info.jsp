<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css" type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/plugins/code/prettify.css"
	type="text/css"></link>
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var id = '<s:property value="info.id" />';
		var page = 1;
	
		$("#cancel").click(function() {
			var url ="../list.html";
			location.href = url;
		});
		
		$("#btn-send").click(function() {
			var c = $("#input_detail").val();
			var data = "t="+Math.random() + "&id=" + id + "&content=" + c;
			$.post("blog_addReply", data, function(data) {
				if (data.status == 1) {
					//alert("添加成功")
					more(1, id);
					$("#input_detail").val("");
					$("#btn-send").attr("disabled", "disabled");
					$("#btn-send").attr("class", "btn-send-none");
				}else{
				}
			});
		});
		
		more(1, id);
		
		$(".input_detail").keyup(function() {
			var v = $(this).val();
			getText(v);
		});
		
		$(".input_detail").keydown(function() {
			var v = $(this).val();
			getText(v);
		});
		
		//$(".input_detail").watermark("回复的内容...");
		
		function getText(v) {
			var len = getLength(v);
			if (len > 0) {
				$("#btn-send").attr("class", "btn-send");
				$("#btn-send").removeAttr("disabled");
			} else {
				$("#btn-send").attr("class", "btn-send-none");
				$("#btn-send").attr("disabled", "disabled");
			}
		}
	});
	
	function more(page, id) {
		$.get("blog_listReply?t=" +Math.random() + "&id=" + id + "&p=" + page, function(data) {
			$(".mitems").html(data);
		});
	}
	

	function getLength(str, shortUrl) {
		if (true == shortUrl) {
			return Math
					.ceil(str
							.replace(
									/((news|telnet|nttp|file|http|ftp|https):\/\/){1}(([-A-Za-z0-9]+(\.[-A-Za-z0-9]+)*(\.[-A-Za-z]{2,5}))|([0-9]{1,3}(\.[0-9]{1,3}){3}))(:[0-9]*)?(\/[-A-Za-z0-9_\$\.\+\!\*\(\),;:@&=\?\/~\#\%]*)*/ig,
									'http://goo.gl/fkKB ').replace(/^\s+|\s+$/ig,
									'').replace(/[^\x00-\xff]/ig, 'xx').length / 2);
		} else {
			return Math.ceil(str.replace(/^\s+|\s+$/ig, '').replace(
					/[^\x00-\xff]/ig, 'xx').length / 2);
		}
	};
</script>
<style type="text/css">
/*======send======*/
.send {
	width: 100%;
	margin-bottom: 15px;
}

.send .title {
	height: 30px;
	overflow: hidden;
	padding: 10px 5px 0;
	font: normal 14px/31px "MicroSoft YaHei", "SimHei";
	color: #999;
}

.send .title .font {
	float: left;
	width: 60%;
}

.send .title .count {
	width: 37%;
	float: right;
	text-align: right;
	font-size: 12px;
}

.send .title .count b {
	position: relative;
	font-family: Tahoma, Arial;
	font-size: 14px;
	font-weight: bold;
	color: #666;
	padding: 0px 4px;
}

.send .input {
	height: 88px;
	position: relative;
	border-radius: 3px;
	margin: 4px 0px;
	background-color: #FFFFFF;
	border-radius: 2px;
	box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
	border: solid 2px #ccc;
}

.send .input .input_detail {
	background: transparent;
	width: 654px;
	padding: 5px;
	height: 80px;
	margin: 2px 0px 0px 2px;
	font-size: 14px;
	font-family: Tahoma, 瀹嬩綋;
	word-wrap: break-word;
	line-height: 18px;
	overflow-y: auto;
	overflow-x: hidden;
	outline: none;
	border-style: none;
	margin: 2px 0px 0px 2px;
}

.send .input-fun {
	width: 100%;
	height: 33px;
	overflow: hidden;
	padding: 5px 0px 0px 0px;
	color: #999;
}

.send .input-fun .funlist {
	float: left;
	width: 68%;
}

.send .input-fun .funlist a {
	color: #666;
}

.send .input-fun .funlist a:hover {
	text-decoration: none;
	color: #0361ab;
}

.send .input-fun .funlist .funlist-item {
	float: left;
	width: 45px;
	height: 21px;
	text-align: right;
	margin-right: 6px;
	position: relative;
}

.send .input-fun .sendfun {
	width: 76px;
	float: right;
	text-align: right;
	font-size: 12px;
}
</style>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>blog/list.html">个人博客</a>
						</s:if>
						<s:else>
						<a href="<%=CnLang.BASEPATH%>blog/<s:property value="info.usrId" />.html">个人博客</a>
						</s:else>
						<em class="gtgt">&gt;&gt;</em>详情
					</div>
				</div>
				<div class="items">
				<div class="info">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="title"><s:property
											value="info.title" /></td>
						</tr>
						<tr>							
							<td style=" text-align: right;">作者：
									<span class="category">
										<a href="<%=CnLang.BASEPATH%>user/<s:property value="info.usrId" />.html" target="blank"><s:property value="info.usrName" /></a>
										<s:if test="1==info.usrRole">&nbsp;&nbsp;<span style="font-size: 16px; font-weight: bold; color: red;">课题组</span></s:if>
									</span>
									&nbsp;&nbsp;&nbsp;&nbsp;日期：<s:property value="info.at" />
							</td>
						</tr>
						<tr>
							<td align="left" >
								<div class="short2 newline" style="line-height: 24px;padding:6px 0px;max-width: 690px; overflow: auto;">
									<s:property value="info.content" escapeHtml="false" />
								</div>
							</td>
						</tr>
					</table>
					</div>
					
					<br />
					<div class="mitems"></div>
					<div class="comment">
					<div class="send clearfix">
						<div class="title">
							<p class="font">回复</p>
							<p class="count" style="display:none;">
								还能输入<b class="c-b">300</b>字
							</p>
						</div>
						<div class="input">
							<textarea id ="input_detail" class="input_detail " tabindex="1" title="回复"></textarea>
				
						</div>
	
						<div class="input-fun clearfix">
							<div class="funlist">
							</div>
							<div class="sendfun">
								<input class="btn-send-none" id="btn-send" disabled="disabled"/>
							</div>
						</div>
						<div id="sa" class="sa">
							<div class="sa-item"></div>
						</div>
					</div>
					</div>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>