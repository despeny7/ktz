<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/uploadify.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.Jcrop/css/jquery.Jcrop.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/swfobject.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.Jcrop/js/jquery.Jcrop.js"></script>
	
<script type="text/javascript">
	$(document).ready(function() {
		var entityName = $('input[name="entityName"]').val();
		var entityId = $('input[name="entityId"]').val();
		if(entityId == '' || entityId == null || entityId == 0){
	   		//生成随机数当附件外键，当论文表保存成功生成id后，再替换随机生成的外键
	   		entityId = Date.parse(new Date()) + "" + Math.floor(Math.random() * 1000 + 1);
	   		$('input[name="entityId"]').val(entityId);
	   	}
		$('#uploadFile').uploadify({
              'uploader': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/uploadify.swf',
              'script': '<%=CnLang.BASEPATH%>fileUpload_uploadAttachment',
              'scriptData':{'entityId':entityId ,'entityName':entityName},
              'fileDataName':'cfile',
              'method': 'get',
              'fileObjName':'uploadFile',
              'cancelImg': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/cancel.png',
              'sizeLimit': 1024 * 1024 * 10, //10M
              'multi' : false,
		      'auto' : true,
			  'queueID' : 'fileQueue',
			  'onComplete' : callback,
              'buttonImg': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/browse.jpg',
              'height' : 20,
              'width': 80									
		});
		
		if(entityId!= null && entityId != ''){
			$.get("fileUpload_fileList?entityId=" + entityId + "&entityName=" + entityName, function(data) {
				$(data.list).each(function(i) {
					var htmlStr = "<a href='javascript:void(0)' class='post_file_del' title='" + data.list[i].fileName 
								+ "' data-id='" + data.list[i].id + "'>" + data.list[i].shortFileName + "</a>";
					$("#fileList").append(htmlStr);
				});
				$("#fileList").find("a").click(function() {
					if (confirm("是否删除该附件？")) {
						var fileId = $(this).attr('data-id');
						$(this).remove();
						$.get("fileUpload_delFile?id=" + fileId, function(data) {
							
						})
					}
				});
			});
		}
	});	
	
	function callback(event, queueID, fileObj, response, data){
		if (response != "") {
			var data = eval('(' + response + ')');
			if(data.state == 1){
        		var htmlStr = "<a href='javascript:void(0)' class='post_file_del' title='" + data.list[0].fileName + "' data-id='" + data.list[0].id + "'>" 
        					    + data.list[0].shortFileName + "</a>&nbsp;&nbsp;";
				$("#fileList").append(htmlStr);
				$("#fileList").find("a").click(function() {
					if (confirm("是否删除该附件？")) {
						var fileId = $(this).attr('data-id');
						$(this).remove();
						$.get("fileUpload_delFile?id=" + fileId, function(data) {
							
						})
					}
				});
          	}else{
          		alert(data.html);
          	}
		}else{
			alert("上传失败");
		}
	}
</script>

<input name="uploadFile" id="uploadFile" type="file" value="上传附件"/>不支持exe格式及大于10M的附件
<input value="<s:property value="entityId" />" name="entityId" type="hidden" />
<input value="${param.paramEntityName}" name="entityName" type="hidden" />
<p id="fileQueue"></p>
<div id="fileList">

</div>
