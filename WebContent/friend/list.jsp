<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});

	function del(id) {
		if (confirm("确定删除该好友？")) {
			$.get("frid_del?id=" + id, function(data) {
				if (data.status == 1) {
					alert("删除成功！");
					$("#n" + id).remove();
				}
				else{
					alert("删除失败！")
				}
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>好友列表
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>user/search.html"
								style="float: right;">+&nbsp;查找好友</a>
						</s:if>
					</div>
				</div>
				<div class="items">
					<s:if test="null==list||0==list.size()">
						<div class="norecord">你的好友太少！</div>
					</s:if>
					<s:else>
						<s:iterator value="list" id="item">
							<div class="item">
								<table border="0" cellpadding="0" cellspacing="0"
									id="n<s:property value="#item.id" />">
									<tr>
										<td rowspan="2" width="60px" valign="top" align="left">
											<div class="img-s">
												<a
													href="<%=CnLang.BASEPATH%>user/<s:property value="#item.fridId"  />.html"><img
													src="<s:property value="#item.friInfo.usrHdString" />" /> </a>
											</div>
										</td>
										<td><span class="title2"><a
												href="<%=CnLang.BASEPATH%>user/<s:property value="#item.fridId" />.html"><s:property
														value="#item.friInfo.usrName" /> </a> </span>
										<s:if test="#item.friInfo.sex==1">&nbsp;&nbsp;<span title="男">男</span>
											</s:if> <s:elseif test="#item.friInfo.sex==0">&nbsp;&nbsp;<span
													title="女">女</span>
											</s:elseif> <s:else>&nbsp;&nbsp;<span title="保密"></span>
											</s:else></td>
										<td style="width: 130px; text-align: right;"><s:property
												value="#item.at" /></td>
									</tr>
									<tr>
										<td>大学：<b><s:property value="#item.friInfo.universityString" />,<s:property
													value="#item.friInfo.universityString2" escape="true" /></b>&nbsp;&nbsp;&nbsp;&nbsp;学历：<span
											class="category"><b><s:property
														value="#item.friInfo.degreeString" /> </b> </span></td>
										<td align="right"><a
											href="javascript:del('<s:property
											value="#item.id" />')"><b>删除好友
											</b></a></td>
									</tr>
								</table>
							</div>
						</s:iterator>
						<%
							Object p = request.getAttribute("page");
								Object r = request.getAttribute("record");
								Object s = request.getAttribute("size");
								String u = CnLang.BASEPATH + "friend/p@pageIndex.html";
								String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
										+ "&u=" + u;
						%>
						<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>