<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>页面维护中……</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css" type="text/css"></link>
</head>
<body>
<div class="head">
	<div class="main">
		<div class="logo"></div>
		<div class="menu clearfix"></div>
		<div class="usr">		
		</div>
	</div>
</div>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<div class="debug" style="height: 400px;">
				<p class="t">页面维护中，敬请期待……</p>
					<div class="c">
						&nbsp;
				</div>
			</div>
		</div>
		
<div class="foot mt10">
	<p></p>
	<p><a href="#">关于我们</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">联系我们</a></p>
</div>

	</div>

</body>
</html>
