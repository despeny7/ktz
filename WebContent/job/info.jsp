<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css" type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/plugins/code/prettify.css"
	type="text/css"></link>
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#cancel").click(function() {
			var url ="../list.html";
			location.href = url;
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>job/list.html">招聘信息</a>
						</s:if>
						<s:else>
						<a href="<%=CnLang.BASEPATH%>job/<s:property value="info.usrId" />.html">招聘信息</a>
						</s:else>
						<em class="gtgt">&gt;&gt;</em>详情
					</div>
				</div>
				<div class="items">
				<div class="info">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<div class="title newline">
									<s:property value="info.title" />
								</div>
							</td>
						</tr>
						<tr>							
							<td style="text-align: right;"><span style="font-weight: bold;">发布人：</span>
								<span class="category">
									<a href="<%=CnLang.BASEPATH%>user/<s:property value="info.usrId" />.html" target="blank"><s:property value="info.usrName" /></a>
									<s:if test="1==info.usrRole">&nbsp;&nbsp;<span style="font-size: 16px; font-weight: bold; color: red;">课题组</span></s:if>
								</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight: bold;">日期：</span><s:property value="info.at" />
							</td>
						</tr>
						<tr>							
							<td style="text-align: left;"><span style="font-weight: bold;">职称要求：</span>
								<span class="category"><s:property value="info.professTitleName" /></span>
							</td>
						</tr>
						<tr>
							<td align="left" ><span style="font-weight: bold;">学科方向：</span>
								<div class="short newline" style="line-height: 20px; padding:2px 0px">
									<s:property value="info.jobDesc" escapeHtml="false" />
								</div>
							</td>
						</tr>
						<tr>
							<td align="left" ><span style="font-weight: bold;">职位详情：</span>
								<div class="short  newline" style="line-height: 20px;padding:2px 0px">
									<s:property value="info.jobRequire" escapeHtml="false" />
								</div>
							</td>
						</tr>
					</table>
					</div>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>