<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});

	function del(id) {
		if (confirm("确定删除该记录？")) {
			$.get("job_del?id=" + id, function(data) {
				if (data.status == 1) {
					$("#n" + id).remove();
				}
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>招聘
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>job/add.html" style="float: right;">+&nbsp;发布招聘信息</a>
						</s:if>
					</div>
				</div>
				<div class="items">
				<s:if test="null==list||0==list.size()">
					<div class="norecord">
						暂时没有发表任何招聘信息！
					</div>
				</s:if>
				<s:else>
					<s:iterator value="list" id="item">
						<div class="item" id="n<s:property value="#item.id" />">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td style="width: 80%">
										<div class="title hiddenline" style="width: 450px;">
											<s:property value="#item.usrName" /><s:if test="#item.usrRole == 1">课题组</s:if>&nbsp;
											<a href="<%=CnLang.BASEPATH%>job/info/<s:property value="#item.id" />.html"><s:property value="#item.title" /></a>
										</div>
									</td>
									<td align="right" style="width: 20%">
										<s:property value="#item.at" />
									</td>
								</tr>
								<tr>
									<td align="left">
										<div class="short newline">
											<s:property value="#item.shortContent" escapeHtml="false" />
										</div>
									</td>
									<td align="right">
										<span class="link" style="display: none;">
											<s:if test="#item.usrId==#session.user_info.id">
												<a href="javascript:del('<s:property value="#item.id" />')">删除</a>&nbsp;&nbsp;
												<a href="<%=CnLang.BASEPATH%>job/add/<s:property value="#item.id" />.html">编辑</a>&nbsp;&nbsp;
											</s:if>
											<a href="<%=CnLang.BASEPATH%>job/info/<s:property value="#item.id" />.html">详情<em class="gtgt">&gt;&gt;</em>
											</a> 
										</span>
									</td>
								</tr>
							</table>
						</div>
					</s:iterator>
					<%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						Object id = request.getParameter("id");
						String u = CnLang.BASEPATH + "job/";
						if (null == id || id.equals("")) {
							u = u + "list";
						} else {
							u = u + id;
						}
						u = u + "/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>