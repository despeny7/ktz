function init_editor() {
	KindEditor.ready(function(K) {
		K.create('textarea["#content"]', {
			height : "200px",
			width : "600px",
			resizeType : 1,
			shadowMode : true,
			autoSetDataMode : false,
			syncType : 'form', // auto: 每次修改时都会同步; form:提交form时同步; 空:不会自动同步;
			allowPreviewEmoticons : true,
			allowUpload : true,
			allowFileManager : false,
			cssPath : 'http://scholargroup.net/ktz.im/plugins/code/prettify.css',
			uploadJson : 'http://scholargroup.net/ktz.im/jsp/upload_json.jsp',
			fileManagerJson : 'file_manager_json.jsp',
			items : [ 'forecolor', 'bold', 'italic', 'underline','|', 'justifyleft', 'justifycenter',
					'justifyright', 'insertorderedlist', 'insertunorderedlist',
					'|', 'image', 'link' ]
		});
	});
//	prettyPrint();
}



function init_editorByName(name) {
	KindEditor.ready(function(K) {
		K.create("textarea[name='" + name + "']", {
			height : "200px",
			width : "600px",
			resizeType : 1,
			shadowMode : true,
			autoSetDataMode : false,
			syncType : 'form', // auto: 每次修改时都会同步; form:提交form时同步; 空:不会自动同步;
			allowPreviewEmoticons : true,
			allowUpload : false,
			allowFileManager : false,
			imageUploadJson : '/KindEditor/UpLoad',
			fileManagerJson : '/KindEditor/List',
			items : [ 'forecolor', 'bold', 'italic', 'underline','|', 'justifyleft', 'justifycenter',
					'justifyright', 'insertorderedlist', 'insertunorderedlist',
					'|', 'link' ]
		});
	});
}

// -- , 'emoticons', 'image', 'map'
function init_editor2() {
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			height : "100px",
			width : "600px",
			resizeType : 1,
			shadowMode : true,
			autoSetDataMode : false,
			syncType : 'auto', // auto: 每次修改时都会同步; form:提交form时同步; 空:不会自动同步;
			allowPreviewEmoticons : true,
			allowUpload : false,
			allowFileManager : false,
			imageUploadJson : '/KindEditor/UpLoad',
			fileManagerJson : '/KindEditor/List',
			items : [ 'source', '|', 'forecolor', 'hilitecolor', 'bold',
					'italic', 'underline', 'removeformat', '|', 'justifyleft',
					'justifycenter', 'justifyright' ]
		});
	});
}
//-- , 'emoticons', 'image', 'map'
function init_editor3() {
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="area"]', {
			height : "100px",
			width : "600px",
			resizeType : 1,
			shadowMode : true,
			autoSetDataMode : false,
			syncType : 'form', // auto: 每次修改时都会同步; form:提交form时同步; 空:不会自动同步;
			allowPreviewEmoticons : true,
			allowUpload : false,
			allowFileManager : false,
			imageUploadJson : '/KindEditor/UpLoad',
			fileManagerJson : '/KindEditor/List',
			items : [ 'source', 'fullscreen', '|', 'fontname', 'fontsize', '|',
					'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
					'removeformat', '|', 'justifyleft', 'justifycenter',
					'justifyright', 'insertorderedlist', 'insertunorderedlist',
					'|', 'hr', 'code', 'link' ]
		});
	});
}

function bindNoteCategory(cid) {
	var selectid = "#selCategory";
	$.post("note_category", function(data) {
		var dataList = data.list;
		$(selectid + " option").remove();
		$(dataList).each(
				function(i) {
					var id = dataList[i].categoryId;
					var tname = dataList[i].categoryName;
					$(selectid).append(
							" <option value='" + id + "'>" + tname
									+ "</option>");
				});
		$(selectid).val(cid);
	});
	
}
function bindTypoCategory(cid) {
	var selectid = "#selCategory";
	$.post("typo_category", function(data) {
		var dataList = data.list;
		$(selectid + " option").remove();
		$(dataList).each(
				function(i) {
					var id = dataList[i].categoryId;
					var tname = dataList[i].categoryName;
					$(selectid).append(
							" <option value='" + id + "'>" + tname
									+ "</option>");
				});
		$(selectid).val(cid);
	});
	
}