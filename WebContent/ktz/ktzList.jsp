<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.css"
	type="text/css"></link>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css" type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var count = document.getElementById("table1").rows.length;	
		//保证table有10行，不够添加空行
		var tableHtml = '';
		for( var i = count; i <= 10; i++){
			var classPro = "";
			if(i%2==1){
				classPro = "userTableRow2";
			}else{
				classPro = "userTableRow1";
			}
			tableHtml = tableHtml + "<tr class='" + classPro + "'>"
				         + "<td></td>"
				         + "<td></td>"
				         + "<td></td>"
				         + "<td></td>"
				         + "<td></td>"
				         + "<td></td>"
			         	 + "</tr>";
		}
		$("#table1").append(tableHtml);
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>home.html">个人主页</a><em class="gtgt">&gt;&gt;</em>课题组成员
					</div>
				</div>
				<div class="items">
					<form action="user_ktzList" name="myform" id="myform" enctype="application/x-www-form-urlencoded" method="post">
						<table class="userTableBorder  tableText" style="border-collapse: separate; border-spacing: 1px; width: 100%;" 
									align="center" border="0">
					        <tr>
					            <td class="userTableRow2" width="30%">
					           	     姓名：<input type="text" name="uname" value="<s:property value="uname" />"/>
					           	</td>
					               
					            <td class="userTableRow2" width="33%">
						  		      大学名称:<input type="text" name="univName" value="<s:property value="univName" />"/></td>
						        <td class="userTableRow2" width="20%">
						        	<input type="hidden" name="id" value="<s:property value="id" />" />
									<input type="submit" id="search" value="组内成员搜索" />
						        </td>
					        </tr>
					        <tr style="height: 10px">
					            <td class="userTableRow1" colspan="3">
				                </td>
					        </tr>
					       </table>
				    </form>
				    
					<table class="userTableBorder tableText"  id="table1"
							style=" border-collapse: separate; border-spacing: 1px; width: 100%;" align="center"
				        border="0">
				        <tr style="text-align: center;" class="userTableRow1">
				            <td width="5%">
				                                         序号	
				            </td>
				            <td width="14%">
				      		          姓名
				            </td>
				            <td width="6%">
				      		          性别
				            </td>
				            <td width="16%">
				               	职称
				            </td>
				            <td width="12%">
				               	身份
				            </td>
				            <td width="32%">
				               	大学院系
				            </td>
				        </tr>
				       <s:iterator value="list" id="user" status="item">
				          <tr <s:if test="#item.count%2==1">class="userTableRow2"</s:if>
				              <s:if test="#item.count%2==0">class="userTableRow1"</s:if>
				              id='n<s:property value="#user.id" />'>
					            <td style="text-align: center;">
					            	<s:property value="#item.count" />
					            </td>
					            <td>
					            	<span class="usr-name">
										<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html" target="blank"><s:property value="#user.usrName" /></a>
									</span>&nbsp;&nbsp;
					            </td>
					            <td>
					            	<s:property value="#user.sexString" />
					            </td>
					            <td>
					            	<s:property value="#user.degreeString" />
					            </td>
					            <td>
					            	<s:if test="#user.headManFlag==1">
					            		<span style="color: red;">组长</span>
					            	</s:if>
					            	<s:if test="#user.headManFlag==2 ">
					            		合作导师
					            	</s:if>
					            	<s:if test="#user.headManFlag==3">
					            		合作学生
					            	</s:if>
					            </td>
					            <td>
					            	<s:property value="#user.universityString" />
					            </td>
				          </tr>
				      </s:iterator>
				    </table>
				   <%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						String u = CnLang.BASEPATH + "ktzList";
						Object id = request.getAttribute("id");
						if (null != id && !id.equals("")) {
							u = u + "/" + id;
						}
						u = u + "/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>