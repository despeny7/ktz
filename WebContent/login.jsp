<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#vcode2").click(function() {
			$("#vcode2").attr("src", "<%=CnLang.BASEPATH%>view/vcode.jsp?rd="+Math.random());
		});
		$("#uname").watermark("电子邮箱");
		$("#upwd").watermark("您的密码");
		$("#vcode").watermark("右侧的数字");
		$(".myform").Validform({
			tiptype : 2,
			callback : function(form) {
				form[0].submit();
			}
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="w930">
				<div class="mlogin-title clearfix">
					<span class="mlogin">用户登录</span>
				</div>
				<s:if test="message!=null&&message!=''">
					<div class="message">
						<s:property value="message" />
					</div>
				</s:if>
				<div class="login">
					<form class="myform" method="post"
						action="<%=CnLang.BASEPATH%>user_login"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="tb-50 mt20">
							<tr>
								<td width="10px">&nbsp;</td>
								<td width="100px">登录名称：</td>
								<td width="220px"><input id="uname" name="ucode" tabindex="1"
									value="<s:property value="uname" />" class="txtusr w216"
									maxlength="32" datatype="*" nullmsg=" " errormsg=" " /></td>
								<td align="left" width="265px;"><div
										class="Validform_checktip"></div>
								</td>
								<td rowspan="3">
									<s:if test="userInfo != null">
										<div class="usr-profile-l subject-div">
											<table>
												<tr>
													<td style="width: 100%; height: 110px; color: #666; font: 12px/1.125 Arial, Helvetica, sans-serif, 微软雅黑;">
														<div style="width: 118px; height: 118px; float: left">
															<!-- <s:property value="#user.usrHdStringL" /> -->
															<img alt="" src="<s:property value="userInfo.usrHdStringM" />" style="width: 118px; height: 118px;"/>
														</div>
															<div class="profile" style="margin-left: 5px;">
															<div style="line-height:20px; height:25px;">
																<span class="usr-name">
																	<a href="<%=CnLang.BASEPATH%>user/<s:property value="userInfo.id" />.html" target="blank"><s:property value="userInfo.usrName" /></a>
																</span>
																&nbsp;&nbsp;&nbsp;<span style="font-size: 20px; font-weight: bold; color: red;">课题组</span>
															</div>
															<div style="line-height:20px; height:25px;" class="usr-us"><s:property value="userInfo.universityString" /></div>
															<div style="line-height:20px; height:25px; width: 140px;" class="hiddenline usr-us" title="<s:property value="userInfo.universityString2" />"><s:property value="userInfo.universityString2" /></div>
															<div style="line-height:20px; height:25px;" class="usr-degree">
																<s:if test="userInfo.degreeString != null">
																	<s:property value="userInfo.degreeString" />&nbsp;&nbsp;
																</s:if>
																<s:property value="userInfo.titleString" />
															</div>
														</div>
													</td>
												</tr>
												<tr>
													<td class="userDirectTd" valign="top" style="color: #666; font: 12px/1.125 Arial, Helvetica, sans-serif, 微软雅黑;">
														<div style="width: 100%;" class="newline">
															<s:property value="userInfo.researchDirect" />
														</div>
													</td>
												</tr>
											</table>
										</div>
									</s:if>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>登录密码：</td>
								<td><input type="password" tabindex="2" id="upwd" name="upwd" value=""
									class="txtpwd w216" maxlength="32" datatype="*" nullmsg=" "
									errormsg=" " />
								</td>
								<td align="left"><div class="Validform_checktip"></div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td></td>
								<td align="left" valign="bottom"><input type="submit"
									class="btn-90 mt6 w100" value="点击登录" />
									<input type="hidden" id="ts"
												name="ts" value="<%=System.currentTimeMillis() %>"/>
									&nbsp;&nbsp;<span style="font-size: 10px;"><a href="<%=CnLang.BASEPATH%>reg.html">没有帐号? 请注册</a></span>
								</td>
								<td></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			
			<!-- 课题组 -->
			<div id="showSubject" style="margin-top: 30px; border-top: 1px solid #d3d3d3;">
				<div class="items" style="width: 100%; margin-top: 10px; margin-bottom: 10px;">
					<table style="width: 100%">
						<tr>
							<td valign="top">
								<s:iterator value="subjectGroupList" id="user">
									<div class="usr-profile-l subject-div">
										<table style="width: 95%; height: 110px; margin-left: 10px;">
											<tr>
												<td style="width: 100%; height: 100px; ">
												<table width="100%" border="0">
												<tr>
												<td style="width: 120px; ">
														<!-- <s:property value="#user.usrHdStringL" /> -->
														<img alt="" src="<s:property value="#user.usrHdStringM" />" style="width: 118px; height: 118px;"/>
													</td>
												<td valign="top"><div class="profile" style="margin-left: 10px;">
														<p style="line-height:20px; height:25px;">
															<span class="usr-name">
																<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html"><s:property value="#user.usrName" /></a>
															</span>
															&nbsp;&nbsp;&nbsp;<span style="font-size: 20px; font-weight: bold; color: red;">课题组</span>
														</p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString2" /></span></p>
														<p style="line-height:20px; height:25px;"><span  class="usr-degree"><s:property value="#user.degreeString" />&nbsp;&nbsp;<s:property value="#user.titleString" /></span></p>
													</div></td>
												</tr>
												
												</table>
													
													
												</td>
											</tr>
											<tr>
												<td class="userDirectTd" valign="top">
													<div style="width:100%;" class="newline">
														<s:property value="#user.researchDirect" />
													</div>
												</td>
											</tr>
										</table>
									</div>
								</s:iterator>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>