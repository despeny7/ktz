<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/uploadify.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.Jcrop/css/jquery.Jcrop.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/swfobject.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.Jcrop/js/jquery.Jcrop.js"></script>

<script type="text/javascript">
  var api;   
  var rt=0;
	$(document).ready(function() {
		$('#browsefile').uploadify({
                'uploader': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/uploadify.swf',
                'script': '<%=CnLang.BASEPATH%>image_hp',
                'fileDataName':'cfile',
                'fileObjName':'browsefile',
                'cancelImg': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/cancel.png',
                'fileExt': '*.jpg;*.bmp;*.png',
                'fileDesc': '支持图片文件(*.gif;*.jpg;*.bmp;*.png)',
                'sizeLimit': 1024 * 1024 * 4, //4M
                'multi' : false,
				'auto' : true,
				'queueID' : 'fileQueue',
				'onComplete' : temp,
                'buttonImg': '<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/browse.jpg',			
                'height' : 20,
                'width': 80								
				});
			});
			
	function temp(event, queueID, fileObj, response, data) {
		if (response != "") {
			var jsonData = eval('(' + response + ')');
			$("#defaultImg").attr("src", jsonData.url);
			$("#largeImg").attr("src", jsonData.url);
			$("#smallImg").attr("src", jsonData.url);
			$("#imgid").val(jsonData.id);
			var w = parseInt(jsonData.width, 10);
			var h = parseInt(jsonData.height, 10);
			var w2 = 300;
			var h2 = 300;
			if (w > h) {
				h2 = (h * 300 / w);
				rt=w/300.0;
			} else {
				w2 = (w * 300 / h);
				rt=h/300;
			}

			$("#defaultImg").attr("width", w2);
			$("#defaultImg").attr("height", h2);
			$("#defaultImg").show();
			$("#hp-save").show();

			if (api != null) {
				api.destroy();
			}
			$("#defaultImg").Jcrop({
				onChange : showPreview,
				onSelect : showPreview,
				setSelect : [ 0, 0, w2, h2 ], //初始化选中区域
				aspectRatio : 1
			}, function() {
				api = this;
			});

			var $preview = $('#largeImg');
			var $preview2 = $('#smallImg');
			function showPreview(coords) {
				var ll = 192;
				var lm = 96;
				$("#x").val(coords.x*rt);
				$("#y").val(coords.y*rt);
				$("#x2").val(coords.w*rt);
				$("#y2").val(coords.h*rt);
				if (parseInt(coords.w) > 0) {
					var rx = ll / coords.w;
					var ry = ll / coords.h;

					$preview.css({
						width : Math.round(rx * w2) + 'px',
						height : Math.round(ry * h2) + 'px',
						marginLeft : '-' + Math.round(rx * coords.x) + 'px',
						marginTop : '-' + Math.round(ry * coords.y) + 'px'
					}).show();

					var rx = lm / coords.w;
					var ry = lm / coords.h;

					$preview2.css({
						width : Math.round(rx * w2) + 'px',
						height : Math.round(ry * h2) + 'px',
						marginLeft : '-' + Math.round(rx * coords.x) + 'px',
						marginTop : '-' + Math.round(ry * coords.y) + 'px'
					}).show();
				};
			}

			function hidePreview() {
				$preview.stop().fadeOut('fast');
			};

		} else {
			$("#default").html("文件上传出错");
		};
	};
</script>
<style type="text/css">
.default-item {
	width: 300px;
	height: 300px;
	border: solid 1px #ccc;
	background: #eee;
	margin-top: 10px;
	text-align: center;
	overflow: hidden;
}

.large-item {
	width: 192px;
	height: 192px;
	overflow: hidden;
	margin-left: 5px;
	border: solid 1px #ccc;
	background: #fff;
	margin: 10px 0px;
}

.small-item {
	width: 96px;
	height: 96px;
	border: solid 1px #ccc;
	background: #fff;
	margin: 10px 0px;
	overflow: hidden;
}

.hp-title {
	font-size: 12px;
	color: #999;
}
</style>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>home.html">个人主页</a><em class="gtgt">&gt;&gt;</em>个人头像
					</div>
				</div>
				<div class="items">
					<div class="mt30 ml30">
						<input type="file" id="browsefile" name="browsefile"
							style="display: block;" />
						<p class="gray">支持jpg、bmp、png格式，小于4M的图片</p>
						<p id="fileQueue"></p>

						<form method="post" action="<%=CnLang.BASEPATH%>hp/save.html"
							enctype="application/x-www-form-urlencoded">
							<table border="0" cellpadding="0" cellspacing="0"
								class="mt20">
								<tr>
									<td class="w332" style="text-align: left;height:350px;" valign="top"><span
										class="hp-title">原 图：</span>
										<div id="default" class="default-item">
											<img src="#" id='defaultImg' title='原图'
												style="display: none;" width="300px;" />
										</div><input type="submit" class="btn-90 mt20" id="hp-save"
										value="保 存" style="display: none;" /></td>
									<td class="w220" align="left" valign="top"><span
										class="hp-title">大 头 像(256*256)：</span>
										<div class="large-item">
											<img src="<s:property
										value="#session.user_info.usrHdStringL"/>" id="largeImg" width="100%" />
										</div></td>
									<td align="left" valign="top"><span class="hp-title">中
											头 像(128*128)：</span>
										<div class="small-item">
											<img src="<s:property
										value="#session.user_info.usrHdStringM"/>" id="smallImg" width="100%"/>
										</div> <input type="hidden" id="x" name="x" value="0" /> <input
										type="hidden" id="y" name="y" value="0" /> <input
										type="hidden" id="x2" name="x2" value="0" /> <input
										type="hidden" id="y2" name="y2" value="0" /> <input
										type="hidden" id="imgid" name="imgid" value="0" /> <input
										type="hidden" id="imgwidth" value="0" /><input type="hidden"
										id="imgheight" value="0" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>