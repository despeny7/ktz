<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#ttskill").watermark("您的技能特长\r\n1.XXXXXX\r\n2.XXXXXX");
					});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>详细资料
					</div>
				</div>
				<s:if test="message!=null&&message!=''">
					<s:if test="message==1">
						<div class="message-s">保存成功.</div>
					</s:if>
					<s:else>
						<div class="message-i">
							<s:property value="message" />
						</div>
					</s:else>
				</s:if>
				<div class="items">
					<form method="post" action="<%=CnLang.BASEPATH%>more/save.html"
						enctype="application/x-www-form-urlencoded" class="myform">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td></td>
								<td valign="top">研究内容及专长：</td>
								<td><textarea rows="7" class="tt w500 mt10 mb10"
										id="ttskill" name="skill"><s:property value="userInfo.skill" /></textarea>
										<br />
										(<span style="color: red;">多个专长用英文逗号（,）分隔</span>)
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn-90" value="保存" />&nbsp;</td>
								<td></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>