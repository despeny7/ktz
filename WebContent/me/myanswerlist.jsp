<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").show();
		}, function() {
			$(this).find(".link").hide();
		});
	});

	function del(id) {
		if (confirm("确定删除该记录？")) {
			$.get("qa_del?id=" + id, function(data) {
				if (data.status == 1) {
					$("#n" + id).remove();
				}
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>我的回答
					</div>
				</div>
				<div class="items">
					<s:if test="null==list||0==list.size()">
						<div class="norecord">
							暂时没有回复任何问答！
						</div>
					</s:if>
					<s:else>
						<s:iterator value="list" id="qa">
							<div class="item" id="n<s:property value="#qa.id" />">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><div class="title hiddenline" style="width: 580px;">
												<a href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html"><s:property
														value="#qa.title" /></a> 
											</div>
										</td>
										<td style="width: 100px; text-align: right;"><s:property
												value="#qa.at" /></td>
									</tr>
									<tr>
										<td>作者：<span class="category"><s:property
													value="#qa.usrName" /> </span>
										</td>
										<td align="right">
												<a href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html">详情</a>
										</td>
									</tr>
									<tr>
										<td align="left" colspan="2">
											<div class="qa-topics-post-feed-item-title newline" style="text-indent: 2em; max-height: 70px; width: 700px;">
												<s:property value="#qa.content" escapeHtml="false" />
											</div>
										</td>
										<td></td>
									</tr>
								</table>
							</div>
						</s:iterator>
						<%
							Object p = request.getAttribute("page");
							Object r = request.getAttribute("record");
							Object s = request.getAttribute("size");
							String u = CnLang.BASEPATH + "meanswer/p@pageIndex.html";
							String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
									+ "&u=" + u;
						%>
						<jsp:include page="<%=url%>"></jsp:include>
					    </s:else>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>