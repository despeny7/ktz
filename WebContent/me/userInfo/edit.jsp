<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.css"
	type="text/css"></link>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css" type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#univName").watermark("输入目前所在的大学名称.");

		$.get("pub_degree",function(data) {
			if (data.state == 1) {
				$("#selDegree option").remove();
				$("#selDegree").append("<option value='0'>未填</option>");
				$(data.list).each(function(i) {
					$("#selDegree").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selDegree").val('<s:property value="userInfo.degree" />');
			}
		});
		
		$.get("pub_title",function(data) {
			if (data.state == 1) {
				$("#selTitle option").remove();
				$("#selTitle").append("<option value='0'>未填</option>");
				$(data.list).each(function(i) {
					$("#selTitle").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selTitle").val('<s:property value="userInfo.title" />');
			}
		});
		
		$("#skillShow").find("a").click(function() {
			$(this).remove();
			var skillVal = "";
			$("#skillShow").find("a").each(function(i) {
				if(i == 0){
					skillVal = skillVal + $(this).html();
				}else{
					skillVal = skillVal + "," + $(this).html();
				}
			});
			$('input[name="userInfo.skill"]').val(skillVal);
		});
		
		$("#ttUsxxxxx").autocomplete("pub_deptx?id=<s:property value='userInfo.univId'/>", {
			dataType : "json",
			//autoFill : true,
			parse : function(data) {// 解释返回的数据，把其存在数组里
				var parsed = [];
				for ( var i = 0; i < data.list.length; i++) {
					parsed[parsed.length] = {
						data : data.list[i],
						value : data.list[i].categoryName,
						result : data.list[i].categoryName
					};
				}
				return parsed;
			},
			formatItem : function(item) {
				// 显示下拉列表的内容
				return item.categoryName;
			},
			formatMatch : function(item) {
				return item.categoryName;
			},
			formatResult : function(item) {
				return item.categoryName;
			}
		}).result(function(event, item, formatted) {
			$("#deptId").val(item.categoryId);
		});
	});
	
	function updateSubject(subjId, subjName){  
	    $("#subId").val(subjId);
	    $("#ttSub").val(subjName);
	}  
	
	function updateUniv(univId, univName, deptId, deptName, provName, provName, provId){  
	    $("#deptId").val(deptId);
	    $("#ttUs").val(deptName);
	    $("#univName").val(univName);
	    $("#univId").val(univId);
	    $("#provName").val(provName);
	    $("#provId").val(provId);
	}  
	
	function keyChange(event){ 
		var skillVal = $('input[name="userInfo.skill"]').val();
		if(skillVal == null || skillVal == ''){
			$("#skillShow").html("");
			return;
		}
		var reg=new RegExp(",","g"); //创建正则RegExp对象  
		skillVal = skillVal.replace(reg,"，");  //替换所有英文逗号,改成中文盗号，
		$('input[name="userInfo.skill"]').val(skillVal);
		if((event.keyCode ==188 || event.keyCode ==8) && skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		} 
	} 
	
	function skillChange(str){
		var arr = str.split("，");
		var htmlStr= "";
		$(arr).each(function(i){
			htmlStr = htmlStr + "<a class='post_tag_del' href='javascript:void(0)'>" + arr[i] + "</a>"
		});
		$("#skillShow").html(htmlStr);
		
		$("#skillShow").find("a").click(function() {
			$(this).remove();
			var skillVal = "";
			$("#skillShow").find("a").each(function(i) {
				if(i == 0){
					skillVal = skillVal + $(this).html();
				}else{
					skillVal = skillVal + "，" + $(this).html();
				}
			});
			$('input[name="userInfo.skill"]').val(skillVal);
		});
	}
	
	function mouseChange(){
		var skillVal = $('input[name="userInfo.skill"]').val();
		if(skillVal == null || skillVal == ''){
			$("#skillShow").html("");
			return;
		}
		var reg=new RegExp(",","g"); //创建正则RegExp对象  
		skillVal = skillVal.replace(reg,"，");  //替换所有英文逗号,改成中文盗号，
		$('input[name="userInfo.skill"]').val(skillVal);
		if(skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>个人资料
					</div>
				</div>
				<s:if test="message!=null&&message!=''">
					<s:if test="message==1">
						<div class="message-s">更新成功.</div>
					</s:if>
					<s:else>
						<div class="message-i">
							<s:property value="message" />
						</div>
					</s:else>
				</s:if>
				<div class="items">
					<form method="post" action="<%=CnLang.BASEPATH%>me/save.html"
						enctype="application/x-www-form-urlencoded" class="myform">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w30"></td>
								<td class="w100">姓名：</td>
								<td class="w310"><input id="txtName" name="userInfo.usrName"
									value="<s:property value="userInfo.usrName" />"
									class="txt-28 w200" maxlength="10"  /></td>
								<td align="left"><div class="Validform_checktip"></div></td>
							</tr>
							<tr>
								<td></td>
								<td>学历：</td>
								<td>
									<select id="selDegree" name="userInfo.degree" class="combo-30 w200">
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>职称：</td>
								<td>
									<select id="selTitle" class="combo-30 w110" name="userInfo.title">
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>性别：</td>
								<td>
									<select id="selSex" class="combo-30 w110" name="userInfo.sex">
										<option value="-1">保密</option>
										<option value="0" <s:if test="userInfo.sex==0">selected="selected"</s:if>>女</option>
										<option value="1" <s:if test="userInfo.sex==1">selected="selected"</s:if>>男</option>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>大学：</td>
								<td>
									<input id="univName" value="<s:property value="userInfo.universityString" />" class="txt-28 w300 thickbox" 
											readonly="readonly" alt="<%=CnLang.BASEPATH%>me/userInfo/editUniv.jsp?height=550&width=700&modal=true"/>
									<input id="univId" type="hidden" value="<s:property value='userInfo.univId'/>"/>
									<input id="provName" type="hidden" value="<s:property value='userInfo.provName'/>"/>
									<input id="provId" type="hidden" value="<s:property value='userInfo.provId'/>"/>
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>院系：</td>
								<td>
									<input class="txt-28 w300 thickbox" type="text" id="ttUs"  value="<s:property value='userInfo.universityString2'/>" 
											readonly="readonly" alt="<%=CnLang.BASEPATH%>me/userInfo/editUniv.jsp?height=550&width=700&modal=true"/>
									<input type="hidden" value="<s:property value='userInfo.deptId'/>" name="deptId" id="deptId"/>
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>学科：</td>
								<td>
									<input class="txt-28 w300 thickbox" type="text" id="ttSub" value="<s:property value='userInfo.subString'/>" 
											readonly="readonly" alt="<%=CnLang.BASEPATH%>pub_editSubject?height=470&width=800&modal=true"/>
									<input type="hidden" value="<s:property value='userInfo.subtId'/>" name="userInfo.subtId" id="subId"/>
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>研究内容及专长：</td>
								<td><input class="txt-28 w350" type="text" name="userInfo.skill" maxlength="120" 
											value="<s:property value='userInfo.skill'/>" onkeyup="keyChange(event)" onmouseout="mouseChange()"/>
								</td>
								<td><div style="color: red; font-size: 10px;">(多个专长用中文逗号（，）分隔)</div></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td colspan="2">
									<div style="width: 100%;" id="skillShow">
										<s:iterator value="skillList" id="skill">
											<a class="post_tag_del"><s:property value="#skill" escapeHtml="false" /></a>
										</s:iterator>
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn-90" value="更新" />&nbsp;</td>
								<td></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>