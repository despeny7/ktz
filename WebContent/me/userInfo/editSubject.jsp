<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var max = 0;
	$(document).ready(function() {
		var parentSubId = parent.$('#subId').val();
		$("#subtId").val(parentSubId);
		$("#subtName").val(parent.$('#ttSub').val());
	
		var fObj = $(".items").find("li").eq(0);
		fObj.attr("class", "current");
		var id = fObj.attr("id");
		showSub(id);

		$(".items").find("li").click(function() {
			$(".items").find("li").removeAttr("class");
			var id = $(this).attr("id");
			$(this).attr("class", "current");
			//alert(id);
			showSub(id);
		})

		function showSub(id) {
			$(".sub").find("li").hide();
			id = "." + id;
			$(id).show();
		}

		$(".sub").find("li").click(function() {
			var cl = $(this).attr("class");
			if (cl.indexOf("current") >= 0) {
				$(this).removeClass("current");
				setc();
			} else {
				if (max < 6) {
					$(this).addClass("current");
					setc();
				} else {
					alert("最多只能选择6个分类。");
				}
			}

		})
		

		function setc() {
			max =0;
			var ids = "";
			var names = "";
			var selectSubjectArr = new Array();
			$(".sub").find("li").each(function(i) {
				var cl = $(this).attr("class");
				if (cl.indexOf("current")>=0) {
					var id = $(this).attr("id");
					var subjectName = $(this).find("a").html();
					// setv(id);
					if(ids ==""){
						ids = id;
					}
					else{
						ids = ids+","+id;
					}
					
					if(names ==""){
						names = subjectName;
					}
					else{
						names = names+","+subjectName;
					}
					//取得第一个class名字
					parentId = cl.split(" ")[0];
					var object = {subjectName: subjectName, subjectId: id, parentId: parentId}; 
					selectSubjectArr[max] = object;
					
					max = max+1;
				}
			});
			$("#subtId").val(ids);
			$("#subtName").val(names);
			
			selectSubjectFunction(selectSubjectArr);
		}
		
		function selectSubjectFunction(selectSubjectArr){
			if(selectSubjectArr != ""){
				var html = "";
				$(selectSubjectArr).each(function(i){
					html = html + "<li class='" + selectSubjectArr[i].subjectId + "' name='" + selectSubjectArr[i].parentId + "'><a href='javascript:void(0)'>" + selectSubjectArr[i].subjectName + "</a></li>"
				})
				$("#selectSubjectUl").html(html);
				
				//重置绑定选中的学科点击后取消按钮
				$(".selectSubject").find("li").click(function() {
					$(this).remove();
					var clId = $(this).attr("class");
					//去掉上面选择学科的高亮显示
					$(".sub").find("li").each(function(){
						var subjectId = $(this).attr("id");
						if(clId == subjectId){
							$(this).removeClass("current");
							setc();
						}
					})
				})
			}
		}
		
		function setv(id){
			var ids = $("#subtId").val();
			if(ids=="" || ids=="0"){
			$("#subtId").val(id);
			}
			else{
				ids =ids+","+id;
				$("#subtId").val(ids);
			}
		}
		
		
		if(parentSubId != null && parentSubId != ""){
			var parentSubIdArr = parentSubId.split(",");
			max = parentSubIdArr.length;
			var html = "";
			var selectSubjectArr = new Array();
			var num = 0;
			$(parentSubIdArr).each(function(i){
				$(".sub").find("li").each(function(){
					var id = $(this).attr("id");
					if (id == parentSubIdArr[i]) {
						//显示二级学科
						var cl = $(this).attr("class");
						$("#" + id).addClass("current");
						
						
						//显示一级学科
						$(".items").find("li").removeAttr("class");
						$("#" + cl).addClass("current");
						
						showSub(cl);
						
						//初始化页面时，在下面显示传过来的默认学科
						var name = $(this).find("a").html()
						var object = {subjectName: name, subjectId: id, parentId: cl}; 
						selectSubjectArr[num] = object;
						num = num + 1;
					}
				})
			});
			selectSubjectFunction(selectSubjectArr);
		}
	});
	
	function cancel(){
		var r=confirm("是否确定取消?")
	    if (r==true)
	    {
	    	self.parent.tb_remove();
	    }
	}
	
	function save(){
		parent.updateSubject($('#subtId').val(), $('#subtName').val());
		self.parent.tb_remove();
	}
</script>
</head>
<body>
	<!-- 正文内容 -->
	<div class="reg">
		<div class="mainnav-2 clearfix">
			<div class="breadcrumb">
				<h6 style="margin:0px;">
					<b>选择学科</b>
				</h6>
			</div>
		</div>
		<div class="subject overflow-y" style="height: 300px;">
			<div class="items clearfix">
				<ul>
					<s:iterator value="list" id="item">
						<s:if test="#item.categoryId % 100==0">
							<li id="c<s:property value="#item.categoryId" />"><a
								href="javascript:void(0)"><s:property
										value="#item.categoryName" /></a></li>
						</s:if>
					</s:iterator>
				</ul>
			</div>
			<div class="sub clearfix">
				<ul>
					<s:iterator value="list" id="item">
						<s:if test="#item.categoryId % 100>0">
							<li class="c<s:property value="#item.categoryId/100*100" />"
								id="<s:property value="#item.categoryId" />"
								style="display: none;"><a href="javascript:void(0)"><s:property
										value="#item.categoryName" /></a></li>
						</s:if>
					</s:iterator>
				</ul>
			</div>
			<input type="hidden" name="subtId" value="0" id="subtId" />
			<input type="hidden" name="subtName" value="0" id="subtName" />
		</div>
		<div class="subject">
			<div class="selectSubject clearfix">
				<ul id="selectSubjectUl">
				</ul>
			</div>
		</div>
		<div style="text-align: right">
			<input type="button" class="btn-90 mt8 mb8" value="保存" onclick="save()"/>
						<input type="button" class="btn-90 mt8 mb8" value="关闭" onclick="cancel()"/>
		</div>
	</div>
</body>
</html>