<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});

	function del(id) {
		if (confirm("确定删除该记录？")) {
			$.get("message_del?id=" + id, function(data) {
				if (data.status == 1) {
					$("#n" + id).remove();
				}
			});
		}
	}

	function addFrid(id, senderId) {
		$.get("frid_add?id=" + id + "&senderId=" + senderId, function(data) {
			if (data.status == 1) {
				alert("操作成功！")
				$("#n" + id).remove();
			}
			else if (data.status == 99) {
				alert("已经是好友！")
				$("#n" + id).remove();
			}
			else{
				alert("操作失败！")
			}
		});
	}
	
	function agreeAddKtz(id) {
		$.get("user_agreeAddKtz?id=" + id, function(data) {
			var html = "";
			if (data.status == 1) {
				alert("加入课题组成功！")
				html = "已同意&nbsp;&nbsp;<a href='javascript:del('<s:property value='#item.id' />')'>删除</a>";
				$("#td" + id).html(html);
			}
			else if (data.status == 99) {
				alert("你已经是该课题组成员！")
				html = "<a href='javascript:del('<s:property value='#item.id' />')'>删除</a>";
				$("#td" + id).html(html);
			}
			else{
				alert("操作失败！")
			}
		});
	}
	
	function refuse(id){
		if (confirm("确定拒绝邀请？")) {
			$.get("message_refuse?id=" + id, function(data) {
				if (data.status == 1) {
					html = "已拒绝&nbsp;&nbsp;<a href='javascript:del('<s:property value='#item.id' />')'>删除</a>";
					$("#td" + id).html(html);
				}
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>消息列表
					</div>
				</div>
				<div class="items">
					<s:if test="null==list||0==list.size()">
						<div class="norecord">暂时没任何消息！</div>
					</s:if>
					<s:else>
						<s:iterator value="list" id="item">
							<div class="item" id="n<s:property value="#item.id" />" >
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<span class="title" <s:if test="#item.status==0">style="font-weight: normal; font-size: 12px;"</s:if>>
												<s:if test="#item.categoryId==1">
													申请好友	
												</s:if>
												<s:elseif test="#item.categoryId==3">
													课题组邀请
												</s:elseif>
												<s:else>
													消息
												</s:else>
											</span>
										</td>
										<td style="width: 100px; text-align: right;"><s:property
												value="#item.at" /></td>
									</tr>
									<tr>
										<td><span class="category">[<a
												href="<%=CnLang.BASEPATH%>user/<s:property value="#item.senderId" />.html" target="blank"><s:property
														value="#item.senderName" /></a>]&nbsp;&nbsp;<s:property value="#item.content" />
										</span></td>
										<td align="right" id='td<s:property value="#item.id" />'>
											<s:if test="#item.categoryId==1">
												<a href="javascript:addFrid('<s:property value="#item.senderId" />')">同意</a>&nbsp;&nbsp;
											</s:if>
											<s:if test="#item.categoryId==3">
												<a href="javascript:agreeAddKtz('<s:property value="#item.id" />')">同意</a>&nbsp;&nbsp;
											</s:if>
											<s:if test="#item.categoryId==1 || #item.categoryId==3">
												<a href="javascript:refuse('<s:property value="#item.id" />')">拒绝</a>&nbsp;&nbsp;
											</s:if>
											<s:if test="#item.categoryId==2">
													已同意&nbsp;&nbsp;
											</s:if>
											<s:if test="#item.categoryId==-1">
													已拒绝&nbsp;&nbsp;
											</s:if>
											<a href="javascript:del('<s:property value="#item.id" />')">删除</a>
										</td>
									</tr>
									<tr>
										<td align="left" colspan="2">
											<div class="short">
												<s:property value="#item.shortContent" escapeHtml="false" />
											</div>
										</td>
										<td></td>
									</tr>
								</table>
							</div>
						</s:iterator>
						<%
							Object p = request.getAttribute("page");
								Object r = request.getAttribute("record");
								Object s = request.getAttribute("size");
								Object id = request.getParameter("id");
								String u = CnLang.BASEPATH + "msg";
								u = u + "/p@pageIndex.html";
								String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
										+ "&u=" + u;
						%>
						<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>