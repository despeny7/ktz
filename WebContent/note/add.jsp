<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/uploadify.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.Jcrop/css/jquery.Jcrop.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/swfobject.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.uploadify-v2.1.4/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.Jcrop/js/jquery.Jcrop.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>My97DatePicker/WdatePicker.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		init_editor();
		$("#txtTitle").watermark("文章的标题.");
		$("#content").watermark("文章的内容.");
		$("#fileBtn").click(function() {
			$("input[name=uploadFile]").click();
		});
		
		$.get("pub_noteType",function(data) {
			if (data.state == 1) {
				$("#selType option").remove();
				$(data.list).each(function(i) {
					$("#selType").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selType").val('<s:property value="noteInfo.type" />');
				
				var noteType = $("#noteType").val();
				if(noteType == 1 || noteType == ''){
					$("#btText").html("标题：");
					$("#qkText").html("期刊名：");
					$("#fileText").show();
					$("#txText").show();
					$("#yearPageTr").show();
					$("#cbText").css("display","none");
					
					if($("#communAuthor").val() == null || $("#communAuthor").val() == ''){
						$("#communAuthor").val('<s:property value="#session.user_info.usrName" />');
					}
				}else if(noteType == 2){
					$("#btText").html("标题：");
					$("#qkText").html("专利号：");
					$("#fileText").show();
					$("#txText").css("display","none");
					$("#yearPageTr").css("display","none");
					$("#cbText").css("display","none");
				}else if(noteType == 3){
					$("#btText").html("书名：");
					$("#qkText").html("出版社：");
					$("#fileText").css("display","none");
					$("#yearPageTr").css("display","none");
					$("#txText").css("display","none");
					$("#cbText").show();
				}
			}
		});
	});	
	
	function changeType(type){
		if(type == 1){
			$("#btText").html("标题：");
			$("#qkText").html("期刊名：");
			$("#fileText").show();
			$("#txText").show();
			$("#yearPageTr").show();
			$("#cbText").css("display","none");
		}else if(type == 2){
			$("#btText").html("标题：");
			$("#qkText").html("专利号：");
			$("#fileText").show();
			$("#yearPageTr").css("display","none");
			$("#txText").css("display","none");
			$("#cbText").css("display","none");
		}else if(type == 3){
			$("#btText").html("书名：");
			$("#qkText").html("出版社：");
			$("#fileText").css("display","none");
			$("#yearPageTr").css("display","none");
			$("#txText").css("display","none");
			$("#cbText").show();
		}
		$("#txtTitle").val("");
		$("#periodical").val("");
		$("#yearPage").val("");
		$("#communAuthor").val("");
		$("#publishDate").val("");
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>note/list.html">论文</a><em class="gtgt">&gt;&gt;</em>添加
					</div>
				</div>
				<div class="items">
					<form id="addnote" method="post"
						action="<%=CnLang.BASEPATH%>note_save"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w100">&nbsp;</td>
								<td>
                   					<select id="selType" class="combo-30 w110" name="noteInfo.type" onchange="changeType(this.value)">
									</select>
                   					<input type="hidden" name="noteInfo.id" value="<s:property value="noteInfo.id" />"/>
                   					<input type="hidden" id="noteType" value="<s:property value="noteInfo.type" />"/>
                   		  		</td>
							</tr>
							<tr>
								<td class="w70" id="btText">标题：</td>
								<td><input id="txtTitle" name="noteInfo.title"
									value="<s:property value="noteInfo.title" />" class="txt-28 w570"
									maxlength="200" /></td>
							</tr>
							<tr>
								<td class="w70">全部作者：</td>
								<td>
									<input type="text" id="firstAuthor" name="noteInfo.firstAuthor" value="<s:property value="noteInfo.firstAuthor" />" class="txt-28 w400" maxlength="150"/>&nbsp;&nbsp;&nbsp;&nbsp;
									<span id="txText">
										通讯作者：<input type="text" id="communAuthor" name="noteInfo.communAuthor" value="<s:property value="noteInfo.communAuthor" />" class="txt-28 w90" maxlength="80"/>
									</span>
									<span style="display: none" id="cbText">
										出版时间：<input type="text" id="publishDate" name="noteInfo.publishDate" value="<fmt:formatDate value='${noteInfo.publishDate}' type='date'  pattern='yyyy-MM-dd'/>" class="txt-28 w90" readonly="readonly" onfocus="WdatePicker({skin:'whyGreen'})" />
									</span>
								</td>
							</tr>
							<tr>
								<td class="w70" id="qkText">期刊名：</td>
								<td><input id="periodical" name="noteInfo.periodical"
									value="<s:property value="noteInfo.periodical" />" class="txt-28 w570"
									maxlength="160" /></td>
							</tr>
							<tr id="yearPageTr">
								<td valign="top" class="w70">年卷期及页码：</td>
								<td><input id="yearPage" name="noteInfo.yearPage"
									value="<s:property value="noteInfo.yearPage" />" class="txt-28 w300"
									maxlength="40" /></td>
							</tr>
							<tr style="display: none;">
								<td valign="top">内容：</td>
								<td style="padding:8px 0px;"><textarea id="content" name="noteInfo.content"
										style="visibility: hidden; ">
										<s:property value="noteInfo.content" />
									</textarea>
								</td>
							</tr>
							<tr id="fileText">
								<td valign="top" class="w70">上传论文：</td>
								<td>
									<jsp:include page="/common/fileUpload.jsp">
										<jsp:param value="NoteEntity" name="paramEntityName"/>
										<jsp:param value="" name="paramEntityId"/>
									</jsp:include>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" class="btn-90 mt8 mb8" value="保存" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>