<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>
	
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
	
<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		init_editor();
		$("#txtTitle").watermark("问答的标题.");
		$("#content").watermark("问答的内容.");
	});
	
	function keyChange(event){ 
		var skillVal = $('input[name="qaInfo.qaLabel"]').val();
		if(skillVal != null && skillVal.indexOf(",") != -1){
			alert("研究内容及专长不能有英文逗号(,)");
			return;
		}
		if((event.keyCode ==188 || event.keyCode ==8) && skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		} 
	} 
	
	function skillChange(str){
		var arr = str.split("，");
		var htmlStr= "";
		$(arr).each(function(i){
			htmlStr = htmlStr + "<a class='post_tag_del' href='javascript:void(0)'>" + arr[i] + "</a>"
		});
		$("#labelShow").html(htmlStr);
		
		$("#labelShow").find("a").click(function() {
			$(this).remove();
			var skillVal = "";
			$("#labelShow").find("a").each(function(i) {
				if(i == 0){
					skillVal = skillVal + $(this).html();
				}else{
					skillVal = skillVal + "，" + $(this).html();
				}
			});
			$('input[name="qaInfo.qaLabel"]').val(skillVal);
		});
	}
	
	function mouseChange(){
		var skillVal = $('input[name="qaInfo.qaLabel"]').val();
		if(skillVal != null && skillVal.indexOf(",") != -1){
			alert("研究内容及专长不能有英文逗号(,)");
			return;
		}
		if(skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		} 
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em><a
							href="<%=CnLang.BASEPATH%>qa/list.html">问答</a><em class="gtgt">&gt;&gt;</em>添加
					</div>
				</div>
				<div class="items">
					<form id="addnote" method="post"
						action="<%=CnLang.BASEPATH%>qa_save"
						enctype="application/x-www-form-urlencoded">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w70">标题：</td>
								<td><input id="txtTitle" name="qaInfo.title"
									value="<s:property value="qaInfo.title" />" class="txt-28 w350"
									maxlength="50" /></td>
							</tr>
							<tr>
								<td valign="top">内容：</td>
								<td style="padding:8px 0px;"><textarea id="content" name="qaInfo.content"
										style="visibility: hidden; ">
										<s:property value="qaInfo.content" />
									</textarea>
								</td>
							</tr>
							<tr>
								<td>标签：</td>
								<td><input class="txt-28 w440" type="text" name="qaInfo.qaLabel" maxlength="100" onkeyup="keyChange(event)" onmouseout="mouseChange()"/>
									<span style="color: red; font-size: 10px;">(多个专长用中文逗号（，）分隔)</span>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<div style="width: 100%;" id="labelShow">
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" class="btn-90 mt8 mb8" value="保存" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>