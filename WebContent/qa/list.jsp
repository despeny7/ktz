<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".item").hover(function() {
			$(this).find(".link").show();
		}, function() {
			$(this).find(".link").hide();
		});
	});

	function del(id) {
		if (confirm("确定删除该记录？")) {
			$.get("qa_del?id=" + id, function(data) {
				if (data.status == 1) {
					$("#n" + id).remove();
				}
			});
		}
	}
	
	function follow(id) {
		var name = $("#f" + id).html();
		if(name == "关注"){
			$.get("qa_qaFollowAdd?id=" + id, function() {
				$("#f" + id).html("已关注");
			});
		}else if(name == "已关注"){
			$.get("qa_qaFollowCanel?id=" + id, function() {
				$("#f" + id).html("关注");
			});
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>问答列表
						<s:if test="authorize==1">
							<a href="<%=CnLang.BASEPATH%>qa/add.html" style="float: right;">+&nbsp;添加问答</a>
						</s:if>
					</div>
				</div>
				<div class="items">
				<s:if test="null==list||0==list.size()">
					<div class="norecord">
						暂时没有发表任何问答！
					</div>
				</s:if>
				<s:else>
					<ul>
						<s:iterator value="list" id="qa">
							<li class="c-list-item">
								<div class="item" id="n<s:property value="#qa.id" />">
									<table style="width: 100%; border-style: none;">
										<tr>
											<td style="width: 90px;" valign="top">
												<img alt="" src='<s:property value="#qa.usrAvatarUrl" />' style="width: 80px; height: 80px;"/>
											</td>
											
											<td>
												<div style="padding-left: 5px; padding-top: 3px;">
													<div style="margin-bottom: 3px;">
														<a href='<%=CnLang.BASEPATH%>user/<s:property value="#qa.usrId" />.html'><s:property value="#qa.usrName" /></a>：&nbsp;<s:property value="#qa.universityString" />&nbsp;<s:property value="#qa.universityString2" />
													</div>
													<div class="topics-post-feed-item-reason hiddenline" style="width: 550px;">
														<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html" title="<s:property value="#qa.title" />"><s:property value="#qa.title" /></a>
													</div>
													<div class="qa-topics-post-feed-item-title newline" style="text-indent: 2em; max-height: 70px;">
														<s:property value="#qa.content" />
													</div>
													<div class="item-title">
														<div class="left">回答：<span style="font-family: Georgia,Times New Roman,Times,serif;"> <s:property value="#qa.answerCnt" /></span></div>
														<div class="right">
															<a class="js-view-question btn btn-plain primary" id="f<s:property value='#qa.id' />"  href="javascript:follow('<s:property value="#qa.id" />')"><s:if test="null==#qa.followId || 0==#qa.followId">关注</s:if><s:else>已关注</s:else></a>
															<a class="js-view-question btn btn-plain primary" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html">回复</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</li>
						</s:iterator>
					</ul>
					<%
						Object p = request.getAttribute("page");
						Object r = request.getAttribute("record");
						Object s = request.getAttribute("size");
						Object id = request.getParameter("id");
						String u = CnLang.BASEPATH + "qa/";
						if (null == id || id.equals("")) {
							u = u + "list";
						} else {
							u = u + id;
						}
						u = u + "/p@pageIndex.html";
						String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
								+ "&u=" + u;
					%>
					<jsp:include page="<%=url%>"></jsp:include>
					</s:else>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>