<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#ucode").watermark("填写电子邮箱.");
		$("#upwd").watermark("设置登录密码.");
		$("#upwd2").watermark("确认密码.");
		$("#uname").watermark("你的真实姓名.");
		$("#vcode").watermark("输入验证码");

		validate();
		
		$.get("pub_degree",function(data) {
			if (data.state == 1) {
				$("#selDegree option").remove();
				$("#selDegree").append("<option value='0'>未填</option>");
				$(data.list).each(function(i) {
					$("#selDegree").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selDegree").val('<s:property value="userInfo.degree" />');
			}
		});
		
		$.get("pub_title",function(data) {
			if (data.state == 1) {
				$("#selTitle option").remove();
				$("#selTitle").append("<option value='0'>未填</option>");
				$(data.list).each(function(i) {
					$("#selTitle").append(" <option value='" + data.list[i].categoryId + "'>"
											+ data.list[i].categoryName
											+ "</option>");
				});
				$("#selTitle").val('<s:property value="userInfo.title" />');
			}
		});

		$("#gvcode").click(function() {
			var lname = $("#lname").val();
			if (lname != "") {
				$.get("sms_send?uname=" + lname+"&tttt="+Math.random(), function(data) {
				});
				$("#gvcode").attr("disabled", "disabled");
					$("#gvcode").attr("class", "btn-sms-none");
					countDown(99);
			}
		});
		
		$(".regrb").find("li").click(function(){
			$(".regrb").find("li").attr("class","");
			$(this).attr("class","current");
			var v = $(this).attr("lang");
			if(v == 1){
				$("#nameHtml").html("组长姓名：");
				$("#degree").html("组长学历：");
				$("#title").html("组长职称：");
			}else if(v == 0){
				$("#nameHtml").html("真实姓名：");
				$("#degree").html("学&nbsp;&nbsp;历：");
				$("#title").html("职&nbsp;&nbsp;称：");
			}
			$("#urole").val(v);
		});
		
	});

	function validate() {
		$(".registerform1").Validform({
			tiptype : 2,
			callback : function(form) {
				//form[0].submit();
			}
		});
	}

	function countDown(secs) {
		$("#gvcode").val(secs + " 秒后重新获取");
		if (--secs > -1) {
			setTimeout("countDown(" + secs + ")", 1000);
		} else {
			$("#gvcode").removeAttr("disabled");
			$("#gvcode").attr("class", "btn-sms-ok");
			$("#gvcode").val("点击获取验证码");
		}
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w930">
				<div class="mlogin-title clearfix">
					<span class="mlogin">用户注册</span>
				</div>
				<s:if test="message!=null&&message!=''">
					<div class="message">
						<s:property value="message" />
					</div>
				</s:if>
				<div class="reg">
					<form method="post" action="reg/next.html" class="registerform1">
					<table border="0" cellpadding="0" cellspacing="0"
							class="tb-50 mt20">
						<tr style="display:none;">
							<td width="100px"><span id="regname">我是</span>
							</td>
							<td width="205px" style="font-size:13px;"><input type="text" id="urole" name="urole"
								value="<s:property value="urole" />" /></td>
						</tr>
						<tr class="reg_tr">
							<td colspan="2">
							<ul class="regrb clearfix">
							<li lang="1" ><a >课题组注册</a></li>
							<li lang="0" class="current"><a >个人用户注册</a></li>
							</ul>
							</td>
						</tr>
						<tr class="reg_tr">
							<td width="100px"><span id="regname">邮箱注册</span>
							</td>
							<td width="220px"><input type="text" id="ucode" name="ucode"
								class="txt w200" value="<s:property value="ucode" />"
								maxlength="32" datatype="e" nullmsg="请输入邮箱"
								errormsg="请输入邮箱" ajaxurl="user_ckName" /></td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>
						<tr class="reg_tr">
							<td width="100px">密&nbsp;&nbsp;码：</td>
							<td><input type="password" id="upwd" name="upwd" value=""
								class="txt w200" maxlength="16" datatype="*6-16"
								nullmsg="请设置密码！" errormsg="密码为6~16位字符！" />
							</td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>

						<tr class="reg_tr">
							<td width="100px">确认密码：</td>
							<td><input type="password" id="upwd2" name="upwd2" value=""
								class="txt w200" maxlength="16" datatype="*" recheck="upwd"
								nullmsg="请再输入一次密码！" errormsg="您两次输入的账号密码不一致！" /></td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>
						<tr class="reg_tr">
							<td width="100px" id="nameHtml">真实姓名：</td>
							<td><input type="text" id="uname" name="uname"
								value="<s:property value="uname" />" class="txt w200"
								maxlength="8" datatype="mz" nullmsg="请输入姓名！" errormsg="请输入真实姓名。" />
							</td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>
						<tr id="education" class="reg_tr">
							<td style="width: 100px;" id="degree">学&nbsp;&nbsp;历：</td>
							<td>
								<select class="combo-30 w110" name="degree" id="selDegree">		
								</select>
							</td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>
						<tr id="title_tr" class="reg_tr">
							<td width="100px" id="title">职&nbsp;&nbsp;称：</td>
							<td>
								<select class="combo-30 w110" name="title" id="selTitle">		
								</select>
							</td>
							<td>
								<div class="Validform_checktip"></div>
							</td>
						</tr>
						<tr class="reg_tr">
							<td width="100px"><s:hidden id="utype" name="utype" value="1"></s:hidden>
							</td>
							<td colspan="2"><input type="submit" id="login" 
								class="btn-90 mt6 w100" value="注册" /><input type="hidden" id="ts"
												name="ts" value="<%=System.currentTimeMillis() %>"/></td>
						</tr>
					</table>
				</form>
				</div>
			</div>

		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>