<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="mainnav-2 clearfix">
	<div class="breadcrumb">
		<a href="<%=CnLang.BASEPATH%>resume/add/<s:if test="null==resumeEntity">0</s:if><s:else><s:property value="resumeEntity.id" /></s:else>.html" style="float: right;">
			<s:if test="authorize==1">
				编辑<em class="gtgt">&gt;&gt;</em>
			</s:if>
		</a>
	</div>
</div>
<s:if test="null==resumeEntity||null==resumeEntity.content||''==resumeEntity.content">
	<div class="norecord">
		暂时没有个人简历！
	</div>
</s:if>
<s:else>
	<div class="items">
		<div class="item">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" colspan="2">
						<div class="short2 newline" style="max-width: 690px; overflow: auto;">
							<s:property value="resumeEntity.content" escapeHtml="false" />
						</div>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</s:else>