<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css" type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/kindeditor/themes/default/default.css"
	type="text/css"></link>
	<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>

<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/kindeditor-min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/kindeditor/lang/zh_CN.js"></script>

<script type="text/javascript" src="<%=CnLang.BASEPATH%>js/base.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		init_editor();
		init_editor3();
		$("#txtTitle").watermark("文章的标题.");
		$("#txtUrl").watermark("课题组的网络链接地址，如http://www.123.com/111.html.");
		
		$(".myform").Validform({
			tiptype : 2,
			callback : function(form) {
				form[0].submit();
			}
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="left w700">
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb">
						<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>课题组
					</div>
				</div>
				<s:if test="message!=null&&message!=''">
					<s:if test="message==1">
						<div class="message-s">保存成功.</div>
					</s:if>
					<s:else>
						<div class="message-i">
							<s:property value="message" />
						</div>
					</s:else>
				</s:if>
				<div class="items">
					<form id="myform" method="post" action="<%=CnLang.BASEPATH%>rg_save"
						enctype="application/x-www-form-urlencoded" class="myform">
						<table border="0" cellpadding="0" cellspacing="0"
							class="item-edit">
							<tr>
								<td class="w90">标题：</td>
								<td class="w320"><input id="txtTitle" name="title"
									value="<s:property value="info.title" />" class="txt-28 w300"
									maxlength="64" datatype="*" nullmsg="标题不能为空。"
									errormsg="标题格式输入错误。" />
								</td>
								<td align="left"><div class="Validform_checktip"></div></td>
							</tr>
							<tr>
								<td valign="top">内容：</td>
								<td style="padding:8px 0px;" colspan="2"><textarea id="content" name="content"
										style="visibility: hidden;"><s:property value="info.content" /></textarea></td>
							</tr>
							<tr>
								<td valign="top">研究领域：</td>
								<td style="padding:8px 0px;" colspan="2"><textarea id="area" name="area"
										style="visibility: hidden;"><s:property value="info.researchArea" /></textarea></td>
							</tr>
							<tr>
								<td>详细地址：</td>
								<td colspan="2"><input id="txtUrl" name="url"
									value="<s:property value="info.detailUrl" />" class="txt-28 w400"
									maxlength="128" /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" class="btn-90 mt6 mb6"
									value="保&nbsp;&nbsp;存" /><input type="hidden" name="id"
									value="<s:property value="info.id" />" /></td>
									<td align="left"><div class="Validform_checktip"></div></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div class="right w210">
				<jsp:include page="../view/bomenu.jsp" flush="false" />
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>