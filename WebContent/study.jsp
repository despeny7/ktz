<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#txtskill").watermark("关键词中间用中文逗号（，）分隔");		
		$(".registerform1").Validform({
			tiptype : 2,
			callback : function(form) {
				//form[0].submit();
			}
		});
	});
	
	function keyChange(event){ 
		var skillVal = $('input[name="skill"]').val();
		if(skillVal == null || skillVal == ''){
			$("#skillShow").html("");
			return;
		}
		var reg=new RegExp(",","g"); //创建正则RegExp对象  
		skillVal = skillVal.replace(reg,"，");  //替换所有英文逗号,改成中文盗号，
		$('input[name="skill"]').val(skillVal);
		if((event.keyCode ==188 || event.keyCode ==8) && skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		} 
	} 
	
	function skillChange(str){
		var arr = str.split("，");
		var htmlStr= "";
		$(arr).each(function(i){
			htmlStr = htmlStr + "<a class='post_tag_del' href='javascript:void(0)'>" + arr[i] + "</a>"
		});
		$("#skillShow").html(htmlStr);
		
		$("#skillShow").find("a").click(function() {
			$(this).remove();
			var skillVal = "";
			$("#skillShow").find("a").each(function(i) {
				if(i == 0){
					skillVal = skillVal + $(this).html();
				}else{
					skillVal = skillVal + "，" + $(this).html();
				}
			});
			$('input[name="skill"]').val(skillVal);
		});
	}
	
	function mouseChange(){
		var skillVal = $('input[name="skill"]').val();
		if(skillVal == null || skillVal == ''){
			$("#skillShow").html("");
			return;
		}
		var reg=new RegExp(",","g"); //创建正则RegExp对象  
		skillVal = skillVal.replace(reg,"，");  //替换所有英文逗号,改成中文盗号，
		$('input[name="skill"]').val(skillVal);
		if(skillVal != null && skillVal != ''){ 
			if(skillVal.substr(skillVal.length-1,1) == '，'){
				skillVal = skillVal.substr(0,skillVal.length-1);
			}
			skillChange(skillVal);
		} 
	}
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="reg">
			<form method="post" action="user_study" class="registerform1">
				<div class="subject">
					<h3 class="title" style="margin-bottom: 0px;">
						<b>研究内容的关键词</b>
					</h3>
					<div class="sub clearfix">
						<input rows="7" class="tt w890 mt10 mb10" id="txtskill" name="skill" onkeyup="keyChange(event)" onmouseout="mouseChange()"/><s:property value="skill"/>
					</div>
					<div style="width: 890px;" id="skillShow">
					</div>
					<table>
					<tr>
					<td><input type="submit" id="login" 
								class="btn-90 mt6 w100" value="保存设定" /></td>
								<td width="20px"></td>
								<td valign="bottom" style="display: none;">跳过设定&nbsp;&nbsp;<a href="<%=CnLang.BASEPATH%>home.html" style="font-weight: bold;">进入网站</a></td>
					</tr>
					</table>
					
			</div>
			</form>
		</div>
	</div>
	<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>