<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>/js/kindeditor/themes/default/default.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">

</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
				<div class="mainnav-2 clearfix">
					<div class="breadcrumb" style="padding-left: 6px; height: 40px; line-height: 30px; font-family: 微软雅黑;">
						<form method="post"
							action="<%=CnLang.BASEPATH%>subjectGroup/userList.html"
							enctype="application/x-www-form-urlencoded">
							<a class="home" href="<%=CnLang.BASEPATH%>home.html">个人首页</a><em class="gtgt">&gt;&gt;</em>导师和学生
							<input type="text" id="uname" name="uname" class="txt-24 w150" value="<s:property value="uname" />"/>
							<input type="submit" class="btn-65 mt8 mb8" value="搜索" />
						</form>
					</div>
				</div>
				<div class="items" style="width: 100%; margin-top: 10px; margin-bottom: 10px; min-height: 500px;">
					<s:if test="null==hierophantList||0==hierophantList.size()">
						<div class="norecord">
							暂时没有导师和学生
						</div>
					</s:if>
					<s:else>
						<table style="width: 100%">
							<tr>
								<td valign="top">
									<s:iterator value="hierophantList" id="user">
										<div class="usr-profile-l subject-div">
											<table style="width: 95%; height: 110px;">
												<tr>
													<td style="width: 100%; ">
														<div style="width: 118px; height: 118px; float: left">
															<!-- <s:property value="#user.usrHdStringL" /> -->
															<img alt="" src="<s:property value="#user.usrHdStringM" />" style="width: 118px; height: 118px;"/>
														</div>
														<div class="profile" style="margin-left: 10px;">
															<p style="line-height:20px; height:25px;">
																<span class="usr-name">
																	<a href="<%=CnLang.BASEPATH%>user/<s:property value="#user.id" />.html"><s:property value="#user.usrName" /></a>
																</span>&nbsp;
																<span ><s:property value="user.degreeString" />&nbsp;&nbsp;</span>
															</p>
															<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString" /></span></p>
															<p style="line-height:20px; height:25px;"><span  class="usr-us"><s:property value="#user.universityString2" /></span></p>
															<p style="line-height:20px; height:25px;"><span  class="usr-degree"><s:property value="#user.degreeString" />&nbsp;&nbsp;<s:property value="#user.titleString" /></span></p>
														</div>
													</td>
												</tr>
												<tr>
													<td class="userDirectTd" valign="top">
														<div style="width:100%;" class="newline">
															<s:property value="#user.researchDirect" />
														</div>
													</td>
												</tr>
											</table>
										</div>
									</s:iterator>
								</td>
							</tr>
							<tr>
								<td style="text-align: right">
									<%
										Object p = request.getAttribute("page");
										Object r = request.getAttribute("record");
										Object s = request.getAttribute("size");
										Object id = request.getParameter("id");
										Object uname = request.getParameter("uname");
										String u = CnLang.BASEPATH + "subjectGroup/userList";
										if (null != uname && !uname.equals("")) {
											u = u + "/" + uname;
										}
										u = u + "/p@pageIndex.html";
										String url = "../view/page.jsp?p=" + p + "&r=" + r + "&s=" + s
												+ "&u=" + u;
									%>
									<jsp:include page="<%=url%>"></jsp:include>
								</td>
							</tr>
						</table>
					</s:else>
				</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>