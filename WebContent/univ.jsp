<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet"
	href="<%=CnLang.BASEPATH%>js/jquery.validform/validform.css"
	type="text/css"></link>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.validform/Validform_v5.3.2.js"></script>

<script type="text/javascript">
	var max = 0;
	$(document).ready(function() {
		var fObj = $(".items").find("li").eq(0);
		fObj.attr("class", "current");
		univ("110000");
		
		$(".items").find("li").click(function() {
			var id = $(this).find("a").attr("name");
			$(".items").find("li").removeAttr("class");
			$(this).attr("class", "current");
			univ(id);
		})

		function univ(name) {
			$("#univs").html("数据加载中...");
			$("#depts").html("请选择大学");
			$.get("pub_univ?id=" +  name, function(data) {
				if (data.state == 1) {
					$("#univs").html("");
					var uname = $("#univId").val();
					$(data.list).each(function(i) {
						if(uname == data.list[i].categoryId){
							$("#univs").append("<li class=\"current\" id ='"+data.list[i].categoryId+"'><a href=\"javascript:void(0)\" >"+data.list[i].categoryName+"</a></li>");
							dept(data.list[i].categoryId);
						}
						else{
						$("#univs").append("<li id ='"+data.list[i].categoryId+"'><a href=\"javascript:void(0)\" >"+data.list[i].categoryName+"</a></li>");
						}
					});
					$(".item").find("li").bind("click",function() {
						var id = $(this).attr("id");
						$(".item").find("li").removeAttr("class");
						$(this).attr("class", "current");
						dept(id);
					})
				}
			});
		}

		function dept(name) {
			$("#depts").html("数据加载中...");
			$.get("pub_dept?id=" + encodeURI(name), function(data) {
				$("#depts").html("");
				var dname = $("#deptId").val();
				if (data.state == 1) {
					$(data.list).each(function(i) {
						if(dname == data.list[i].categoryId){
							$("#depts").append("<li class=\"current\" id ='"+data.list[i].categoryId+"'><a href=\"javascript:void(0)\" >"+data.list[i].categoryName+"</a></li>");
						}
						else{
						$("#depts").append("<li id ='"+data.list[i].categoryId+"'><a href=\"javascript:void(0)\" >"+data.list[i].categoryName+"</a></li>");
						}
						});
					
					$(".item-sub").find("li").bind("click",function() {
						var id = $(this).attr("id");
						$(".item-sub").find("li").removeAttr("class");
						$(this).attr("class", "current");
						$("#univId").val(name);
						$("#deptId").val(id);
					})
				}
			});
		}
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<!-- 正文内容 -->
			<div class="reg">
				<form method="post" action="user_univ" class="registerform1">
					<div class="subject">
						<h3 class="title" style="margin:0px;">
							<b>选择城市</b>
						</h3>
						<div class="items clearfix">
							<ul>
								<li><a href='javascript:void(0)' name='110000' >北京</a></li>
								<li><a href='javascript:void(0)' name='310000' >上海</a></li>
								<li><a href='javascript:void(0)' name='230000' >黑龙江</a></li>
								<li><a href='javascript:void(0)' name='220000' >吉林</a></li>
								<li><a href='javascript:void(0)' name='210000' >辽宁</a></li>
								<li><a href='javascript:void(0)' name='120000' >天津</a></li>
								<li><a href='javascript:void(0)' name='340000' >安徽</a></li>
								<li><a href='javascript:void(0)' name='320000' >江苏</a></li>
								<li><a href='javascript:void(0)' name='330000' >浙江</a></li>
								<li><a href='javascript:void(0)' name='610000' >陕西</a></li>
								<li><a href='javascript:void(0)' name='420000' >湖北</a></li>
								<li><a href='javascript:void(0)' name='440000' >广东</a></li>
								<li><a href='javascript:void(0)' name='430000' >湖南</a></li>
								<li><a href='javascript:void(0)' name='620000' >甘肃</a></li>
								<li><a href='javascript:void(0)' name='510000' >四川</a></li>
								<li><a href='javascript:void(0)' name='370000' >山东</a></li>
								<li><a href='javascript:void(0)' name='350000' >福建</a></li>
								<li><a href='javascript:void(0)' name='410000' >河南</a></li>
								<li><a href='javascript:void(0)' name='500000' >重庆</a></li>
								<li><a href='javascript:void(0)' name='530000' >云南</a></li>
								<li><a href='javascript:void(0)' name='130000' >河北</a></li>
								<li><a href='javascript:void(0)' name='360000' >江西</a></li>
								<li><a href='javascript:void(0)' name='140000' >山西</a></li>
								<li><a href='javascript:void(0)' name='520000' >贵州</a></li>
								<li><a href='javascript:void(0)' name='450000' >广西</a></li>
								<li><a href='javascript:void(0)' name='150000' >内蒙古</a></li>
								<li><a href='javascript:void(0)' name='640000' >宁夏</a></li>
								<li><a href='javascript:void(0)' name='630000' >青海</a></li>
								<li><a href='javascript:void(0)' name='650000' >新疆</a></li>
								<li><a href='javascript:void(0)' name='460000' >海南</a></li>
								<li><a href='javascript:void(0)' name='540000' >西藏</a></li>
								<li><a href='javascript:void(0)' name='900000' >香港</a></li>
								<li><a href='javascript:void(0)' name='910000' >澳门</a></li>
								<li><a href='javascript:void(0)' name='920000' >台湾</a></li>
								<li><a href='javascript:void(0)' name='930000' ><b>中科院</b></a></li>								
							</ul>
						</div>
						<h3 class="title" style="margin:0px;">
							<b>选择大学</b>
						</h3>
						<div class="item clearfix" >
							<ul id ="univs">
							</ul>
						</div>
						
						<h3 class="title" style="margin:0px;">
							<b>选择学院</b>
						</h3>
						<div class="item-sub clearfix" >
							<ul id ="depts">
							</ul>
						</div>
						<input type="hidden" name="univId" value="0" id="univId" />
						<input type="hidden" name="deptId" value="0" id="deptId" />
						<table>
							<tr>
								<td><input type="submit" id="login" class="btn-90 mt6 w100"
									value="下一步" /></td>
								<td width="20px"></td>
								<td valign="bottom" style="display: none;">跳过设定&nbsp;&nbsp;<a
									href="<%=CnLang.BASEPATH%>home.html" style="font-weight: bold;">进入网站</a></td>
							</tr>
						</table>
						<p></p>
					</div>
				</form>
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>