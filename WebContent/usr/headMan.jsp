<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="usr-bonav clearfix" style="width: 99.5%">
	<h3>组长</h3>
	<s:if test="headMan != null">
		<ul>
			<li class="clearfix">
				<div class="img-s">
					<img src="<s:property value="headMan.usrHdStringM" />">
				</div>
				<p>
					<a href="<%=CnLang.BASEPATH%>user/<s:property value="headMan.id" />.html"><s:property value="headMan.usrName"/></a>
					
				</p>
				<p><s:property value="headMan.degreeString"/>&nbsp;&nbsp;<s:property value="headMan.titleString"/></p>
			</li>
		</ul>
	</s:if>
</div>