<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".usr-home-tablist").find("li").each(function(i) {
			this.onclick = function() {
				$(".usr-home-tablist").find("li").removeClass("current");
				$(this).addClass("current");
				$(".usr-home-tab").hide();
				$(".usr-home-tab").eq(i).show();
			}
		});

		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});
</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<s:action name="user_profilel" executeResult="true" flush="true">
				<s:param name="id">
					<s:property value="id" />
				</s:param>
			</s:action>
			<!-- 正文内容 -->
			<div class="left w700" style="min-height: 500px;">
				<div class="usr-home-tablist">
					<ul>
						<li <s:if test="0==userInfo.usrRole">style="display: none;"</s:if><s:else>class="current"</s:else>><span>组内新闻</span></li>
						<li <s:if test="0==userInfo.usrRole">class="current"</s:if>><span>论文专著</span></li>
						<li><span>研究方向</span></li>
						<li>
							<span>
								<s:if test="1==userInfo.usrRole">课题组介绍</s:if>
								<s:else>个人简历</s:else>
							</span>
						</li>
						<li <s:if test="1==userInfo.usrRole">style="display: none;"</s:if>><span>个人博客</span></li>
					</ul>
				</div>
				<div <s:if test="1==userInfo.usrRole">style="display: inline;"</s:if><s:else>style="display: none;"</s:else> class="usr-home-tab">
					<s:action name="news_top" executeResult="true" flush="true">
						<s:param name="id">
							<s:property value="id" />
						</s:param>
					</s:action>
				</div>
				<div <s:if test="0==userInfo.usrRole">style="display: inline"</s:if><s:else>style="display: none;"</s:else> class="usr-home-tab">
					<s:action name="note_top" executeResult="true" flush="true">
						<s:param name="id">
							<s:property value="id" />
						</s:param>
					</s:action>
				</div>
				<div style="display: none;" class="usr-home-tab">
					<s:action name="research_top" executeResult="true" flush="true">
						<s:param name="id">
							<s:property value="id" />
						</s:param>
					</s:action></div>
				<div style="display: none;" class="usr-home-tab">
					<s:action name="resume_top" executeResult="true" flush="true">
						<s:param name="id">
							<s:property value="id" />
						</s:param>
					</s:action></div>
				<div style="display: none;" class="usr-home-tab">
					<s:action name="blog_top" executeResult="true" flush="true">
						<s:param name="id">
							<s:property value="id" />
						</s:param>
					</s:action>
				</div>
			</div>
			<jsp:include page="/view/rightFrame.jsp"></jsp:include>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>