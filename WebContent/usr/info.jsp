<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="items">
	<div class="info">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="w100">姓名：</td>
				<td class="cb3"><s:property value="userInfo.usrName" /></span></td>
			</tr>
			<tr>
				<td>性别：</td>
				<td class="cb3"><s:property value="userInfo.SexString" /></td>
			</tr>
			<tr>
				<td>学历：</td>
				<td class="cb3"><s:property value="userInfo.degreeString" /></td>
			</tr>
			<tr>
				<td>所在地：</td>
				<td class="cb3"><s:property value="userInfo.locationString" /></td>
			</tr>
			<tr>
				<td>院系：</td>
				<td class="cb3"><s:property value="userInfo.universityString" /></td>
			</tr>
			<tr>
				<td valign="top">技能特长：</td>
				<td class="cb3" style="line-height: 20px">
					<div style="width: 500px; margin-top: 10px; margin-bottom: 10px;">
						<s:iterator value="skillList" id="skill">
							<a class="post_tag"><s:property value="#skill" escapeHtml="false" /></a>
						</s:iterator>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>