<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="mainnav-2 clearfix" style="height: 600px; width: 100%; ">
	<table class="main-top-table">
		<tr>
			<td rowspan="2" valign="top">
				<div>
					<h6 class="main-top-model">+ 你可能会回答的问题</h6>
				</div>
				<div style="padding-left: 20px; padding-right: 20px;">
					<ul>
						<s:iterator value="qalist" id="qa" status="item">
							<li class="c-list-item">
								<div class="topics-post-feed-item-reason"><s:property value="#qa.usrName" />：&nbsp;
									<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html" title="<s:property value="#qa.title" />"><s:property value="#qa.title" /></a>
								</div>
								<div style="padding-left: 50px;">
									<div class="topics-post-feed-item-title">
										<s:property value="#qa.shortContent" />
									</div>
									<div>回答： <s:property value="#qa.answerCnt" /></div>
								</div>
							</li>
						</s:iterator>
					</ul>
				</div>
				
			</td>
			<td valign="top">
				<div>
					<h6 class="main-top-model">+ 课题组新闻</h6>
				</div>
				<s:iterator value="newlist" id="news" status="item">
					<div class="title" style="padding: 3px; line-height: 18px; " id="n<s:property value="#news.id" />">
						<div style="width: 70%; float: left; font-size: 12px; font-weight: bold;" class="newline">
							<s:property value="#item.count" />.&nbsp;
							<a href="<%=CnLang.BASEPATH%>news/info/<s:property value="#news.id" />.html" title="<s:property value="#news.title" />"><s:property value="#news.title" /></a>
						</div>
						<div style="float: right; padding-right: 10px;">
							<s:property value="#news.at" />
						</div>
					</div>
				</s:iterator>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<div>
					<h6 class="main-top-model">+ 招聘公告</h6>
				</div>
			</td>
		</tr>
	</table>
</div>