<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><%=CnLang.TITLE%></title>
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="<%=CnLang.KEYWORDS%>" />
<meta http-equiv="description" content="<%=CnLang.DESC%>" />
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>css/com.css"
	type="text/css"></link>
<link rel="stylesheet" href="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox.css" type="text/css"></link>

<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.thickbox/thickbox-compressed.js"></script>
<script type="text/javascript"
	src="<%=CnLang.BASEPATH%>js/jquery.watermark/jquery.watermark.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#question").watermark("Q&A：提问你研究上的困惑获得你想要的答案.");
		$("#note").watermark("论文：输入发表的论文标题并点击添加论文.");
		tb_init("#qaModel");
		tb_init("#noteModel");
	
		$(".usr-home-tablist").find("li").each(function(i) {
			this.onclick = function() {
				$(".usr-home-tablist").find("li").removeClass("current");
				$(this).addClass("current");
				$(".usr-home-tab").hide();
				$(".usr-home-tab").eq(i).show();
			}
		});

		$(".item").hover(function() {
			$(this).find(".link").toggle();
		}, function() {
			$(this).find(".link").toggle();
		});
	});
	
	function follow(id) {
		var name = $("#f" + id).html();
		if(name == "关注"){
			$.get("qa_qaFollowAdd?id=" + id, function() {
				$("#f" + id).html("已关注");
			});
		}else if(name == "已关注"){
			$.get("qa_qaFollowCanel?id=" + id, function() {
				$("#f" + id).html("关注");
			});
		}
	}

</script>
</head>
<body>
	<jsp:include page="/view/head.jsp"></jsp:include>
	<div class="main">
		<div class="submain clearfix mt20 round-rect-2">
			<div class="left" style="padding-left: 6px; height: 50px; line-height: 30px; font-family: 微软雅黑;">
				<input class="txt-28 w350" type="text" id="question"/>
				<a href="<%=CnLang.BASEPATH%>qa/addQaModel.jsp?height=450&width=600&modal=true" id="qaModel">
				<input type="button" class="btn-90 mt8 mb8" value="提问"/></a>
			</div>
			<div class="right" style="padding-left: 6px; height: 50px; line-height: 30px; font-family: 微软雅黑;">
				<input class="txt-28 w320" type="text" id="note"/>
				<a href="<%=CnLang.BASEPATH%>note/addNoteModel.jsp?height=400&width=600&modal=true" id="noteModel">
				<input type="button" class="btn-90 mt8 mb8" value="添加论文"/></a>
			</div>
			<!-- 正文内容 -->
			<div class="left w930">
					
					<div class="mainnav-2 clearfix" style="width: 100%;">
						<table class="main-top-table">
							<tr>
								<td class="main-top-table-td" rowspan="2" valign="top" style="width: 60%;">
									<div class="overflow-y" style="height: 940px;">
										<div>
											<div class="left"><h6 class="main-top-model">+ 你可能会回答的问题</h6></div>
											<div class="right" style="padding-right: 3px;">
												<a href="<%=CnLang.BASEPATH%>qa/list.html" style="float: right;">
													<em class="gtgt">查看更多</em></a>
											</div>
										</div>
										<div style="padding-left: 10px; padding-right: 10px; padding-top: 33px;">
											<ul>
												<s:iterator value="qalist" id="qa" status="item">
													<li class="c-list-item">
														<table style="width: 100%; border-style: none;">
															<tr>
																<td style="width: 90px;" valign="top">
																	<img alt="" src='<s:property value="#qa.usrAvatarUrl" />' style="width: 80px; height: 80px;"/>
																</td>
																
																<td>
																	<div style="padding-left: 5px; padding-top: 3px;">
																		<div style="margin-bottom: 3px;">
																			<a href='<%=CnLang.BASEPATH%>user/<s:property value="#qa.usrId" />.html'><s:property value="#qa.usrName" /></a>：&nbsp;<s:property value="#qa.universityString" />&nbsp;<s:property value="#qa.universityString2" />
																		</div>
																		<div class="topics-post-feed-item-reason hiddenline" style="width: 400px;">
																			<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html" target="blank" title="<s:property value="#qa.title" />"><s:property value="#qa.title" /></a>
																		</div>
																		<div class="topics-post-feed-item-title newline" style="text-indent: 2em;">
																			<s:property value="#qa.shortContent" />
																		</div>
																		<div>
																			<div class="left">回答：<span style="font-family: Georgia,Times New Roman,Times,serif;"> <s:property value="#qa.answerCnt" /></span></div>
																			<div class="right">
																				<a class="js-view-question btn btn-plain primary" id="f<s:property value='#qa.id' />"  href="javascript:follow('<s:property value="#qa.id" />')"><s:if test="null==#qa.followId || 0==#qa.followId">关注</s:if><s:else>已关注</s:else></a>
																				<a class="js-view-question btn btn-plain primary" href="<%=CnLang.BASEPATH%>qa/info/<s:property value="#qa.id" />.html">回复</a>
																			</div>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
													</li>
												</s:iterator>
											</ul>
										</div>
									</div>
								</td>
								<td class="main-top-table-td" valign="top" style="height: 270px; width: 40%;">
									<div>
										<table style="width: 100%">
											<tr>
												<td style="text-align: left"><h6 class="main-top-model">+ 课题组新闻</h6></td>
												<td style="text-align: right; padding-right: 3px;">
													<a href="<%=CnLang.BASEPATH%>news/allList.html" style="float: right;"><em class="gtgt">查看更多</em></a>
												</td>
											</tr>
										</table>
									</div>
									<div>
										<table style="width: 100%">
											<s:iterator value="newlist" id="news" status="item">
												<tr class="title" style="padding: 3px; line-height: 18px; " id="n<s:property value="#news.id" />">
													<td style="width: 250px;">
														<div style="width: 250px; padding-left: 5px; padding-top: 5px; font-size: 12px; font-weight: bold;" class="hiddenline">
															<a href="<%=CnLang.BASEPATH%>news/info/<s:property value="#news.id" />.html" target="blank" title="<s:property value="#news.title" />"><s:property value="#news.title" /></a>
														</div>
													</td>
													<td>
														<div style="padding-right: 4px; text-align: right;">
															<a href="<%=CnLang.BASEPATH%>user/<s:property value="#news.usrId" />.html" target="blank"><s:property value="#news.usrName" /></a><span style="font-size: 10px; font-weight: bold; color: red;">课题组</span>
														</div>
													</td>
												</tr> 
											</s:iterator>
											<tr>
												<td></td>
												<td></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td class="main-top-table-td" valign="top" style="height: 50%;">
									<div>
										<table style="width: 100%">
											<tr>
												<td style="text-align: left"><h6 class="main-top-model">+ 招聘公告</h6></td>
												<td style="text-align: right; padding-right: 3px;">
													<a href="<%=CnLang.BASEPATH%>job/list.html" style="float: right;"><em class="gtgt">查看更多</em></a>
												</td>
											</tr>
										</table>
									</div>
									<div style="padding-left: 10px; padding-right: 10px;">
										<ul>
											<s:iterator value="jobList" id="job" status="item">
												<li class="job-list-item">
													<div class="topics-post-feed-item-reason hiddenline" style="width: 350px;">
														<a style="font-weight: bold;" href="<%=CnLang.BASEPATH%>job/info/<s:property value="#job.id" />.html" title="<s:property value="#job.title" />"><s:property value="#job.title" /></a>
													</div>
													<div style="padding-left: 20px;">
														<div class="newline">
															<s:property value="#job.shortContent" />
														</div>
													</div>
												</li>
											</s:iterator>
										</ul>
									</div>
								</td>
							</tr>
						</table>
					</div>
			</div>
		</div>
		<jsp:include page="/view/foot.jsp"></jsp:include>
	</div>
</body>
</html>