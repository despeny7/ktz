<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="usr-profile-l clearfix">
	<div class="img-l">
		<s:if test="userInfo.usrHd != '' && userInfo.usrHd != 0">
			<img alt="" src="<s:property value="userInfo.usrHdStringL" />"/>
		</s:if>
		<s:else>
			<div style="background: url('http://www.scholargroup.net/imgs/l/7000000.png') no-repeat; height: 128px; width: 128px;">
				<s:if test="#session.user_info.id == userInfo.id">
					<div style="padding-top: 90px; text-align: center;">
						<a href="<%=CnLang.BASEPATH%>hp.html"><font color="black" size="3">编辑头像</font></a>
					</div>
				</s:if>
			</div>
		</s:else>
	</div>
	<div class="profile">
		<p>
			<span  class="usr-name"><s:property value="userInfo.usrName" /></span>&nbsp;
			<s:if test="0==userInfo.usrRole"><span class="usr-sex">&nbsp;&nbsp;</span></s:if>
			<s:if test="1==userInfo.usrRole">&nbsp;&nbsp;<span style="font-size: 24px; font-weight: bold; color: red;">课题组</span></s:if>
		</p>
		<p><span class="usr-degree"><s:property value="userInfo.degreeString" /></span>&nbsp;&nbsp;<span style="font-size: 14px; font-weight: bold;"><s:property value="userInfo.titleString" /></span></p>
		<p><span class="usr-us"><s:property value="userInfo.universityString" />&nbsp;&nbsp;<s:property value="userInfo.universityString2" /></span></p>
		<p>
			<span class="usr-us">
				<s:if test="userInfo.ktzName != '' && userInfo.ktzName != null">
					<a href="<%=CnLang.BASEPATH%>user/<s:property value="userInfo.ktzId" />.html"><s:property value="userInfo.ktzName" /></a>
					<span style="font-weight: bold;">课题组</span>&nbsp;&nbsp;
				</s:if>
			</span>
		</p>
	</div>
	<div class="rp">
		<p>发表论文：<a href="<%=CnLang.BASEPATH%>note/<s:property value="userInfo.id" />.html" ><s:property value="userInfo.noteNum" /></a>个</p>
		<p style="display: none;">浏览人数：
			<s:if test="authorize==1"><a href="<%=CnLang.BASEPATH%>friend.html">500</a></s:if>
			<s:else>500</s:else>人
		</p>
		<div class="radius" style="text-align: center">
			<div style="padding-top: 5px;">招聘招收</div>
			<div style="width: 70px; padding: 10px;">
				<a href="<%=CnLang.BASEPATH%>job/<s:property value="userInfo.id" />.html">招聘博士后招收硕士生</a>
			</div>
		</div>
	</div>
</div>