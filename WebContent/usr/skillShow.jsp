<%@page import="comm.CnLang"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="usr-bonav clearfix" style="width: 99.5%; min-height: 150px;">
	<h3>研究内容及专长</h3>
	<div style="margin:5px;">
		<s:iterator value="skillList" id="skill">
			<p class="post_tag"><s:property value="#skill" escapeHtml="false" /></p>
		</s:iterator>
	</div>
</div>