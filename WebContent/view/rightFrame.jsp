<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="entity.UserEntity"%>
<%@ page import="comm.CnLang"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>

<div class="right w210">
	<s:action name="user_skillShow" executeResult="true" flush="true">
		<s:param name="id">
			<s:property value="id" />
		</s:param>
	</s:action>
	
	<s:action name="user_headMan" executeResult="true" flush="true">
		<s:param name="id">
			<s:property value="id" />
		</s:param>
	</s:action>
	
	<s:action name="user_teacher" executeResult="true" flush="true">
		<s:param name="id">
			<s:property value="id" />
		</s:param>
	</s:action>
	
	<s:action name="user_student" executeResult="true" flush="true">
		<s:param name="id">
			<s:property value="id" />
		</s:param>
	</s:action>
</div>
