>>>>>>>>>>整体的<<<<<<<<<<<
1.框架	struts2.0+mybatis+spring
2.伪URL地址	urlrewrite
3.图片裁剪		im4java

>>>>>>>>>>配置文件的说明<<<<<<<<<<<
1.WEB-INF\urlrewrite.xml 负责伪URL到真实的路径的配置，如访问：user/111.html 真实的地址可能是：user_info?userid=111 这样子的
2。src\applicationContext.xml spring的配置
3.struts.xml struts配置
4.log4j.properties 日志配置
5.config.properties 网站一些的配置，目前主要是图片服务器的配置，如图片保存的位置，图片服务器的地址等（因为图片服务器和网站分开了，不是一起的）

>>>>>>>>>>包的说明<<<<<<<<<<<
1.action	请求的实现相关的。所有的struts2相关的请求都会到这里
2.comm		常用的一些方法、类
3.config	本来准备做中英文的配置切换
4.dao		mybatis访问接口（只是定义，作用呢，为mybatis的反射提供方法名和参数）
5.entity	实体包
6.filter	防止中文乱码
7.image		图片裁剪、缩放等一些操作类
8.impl		实现类（实现dao定义的方法，所以和dao是对应的）
9.mybatis	数据操作的配置文件（sql）
10.test		测试类

一般流程是 action --> impl --> dao --> mybatis

>>>>>>>>>>功能的说明<<<<<<<<<<<
1.每个action类里都有对应impl类的get/set方法
2.每个impl类都有dao类对应的get/set方法
3.每个xxxxMapper.xml文件的命名头都是以<mapper namespace="dao.IxxxxDao">对应的
4.而这些关系的定义都在applicationContext.xml中定义
如
	<!-- NOTE -->
	<bean id="noteDao" class="org.mybatis.spring.mapper.MapperFactoryBean">
		<property name="mapperInterface" value="dao.INoteDao"></property>
		<property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
	</bean>
	<bean id="noteService" class="impl.NoteImpl">
		<property name="noteDao" ref="noteDao"></property>
	</bean>
	<bean id="note" class="action.NoteAction">    
        <property name="noteService" ref="noteService" />    
    </bean> 
****** ref 表示引用（或者叫初始化）
****** 从后面往前看：
id = note class = action.NoteAction 表示  action.NoteAction note = new action.NoteAction();
在action.NoteAction文件中有个 impl.NoteImpl noteService 的get/set方法
name 中的表示自己的变量的名称（即action中 impl.NoteImpl noteService = null ）  ref 表示要引用的变量名（即<bean id="noteService" ）
以上同理
