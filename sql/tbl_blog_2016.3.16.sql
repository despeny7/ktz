SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_news`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_blog`;
CREATE TABLE `tbl_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID(主键)',
  `usr_id` int(10) DEFAULT '0' COMMENT '用户Id',
  `title` varchar(256) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `img_id` int(11) DEFAULT '0' COMMENT '图片ID',
  `tag` varchar(512) DEFAULT NULL COMMENT '标签',
  `visit_cnt` int(10) DEFAULT '0' COMMENT '访问人数',
  `status` int(10) DEFAULT '1' COMMENT '状态 1：有效 0：删除',
  `create_by` int(10) DEFAULT '0' COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT '0',
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78088976 DEFAULT CHARSET=utf8 COMMENT='博客';
