/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50173
Source Host           : 127.0.0.1:3306
Source Database       : ktz

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2016-01-05 10:27:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `m_job_title`
-- ----------------------------
DROP TABLE IF EXISTS `m_job_title`;
CREATE TABLE `m_job_title` (
  `id` int(11) NOT NULL COMMENT 'ID(主键)',
  `name` varchar(256) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='招聘职称';

-- ----------------------------
-- Records of m_job_title
-- ----------------------------
INSERT INTO `m_job_title` VALUES ('20100', '硕士生');
INSERT INTO `m_job_title` VALUES ('20200', '博士生');
INSERT INTO `m_job_title` VALUES ('20300', '博士后');
INSERT INTO `m_job_title` VALUES ('20400', '助理研究员');
INSERT INTO `m_job_title` VALUES ('20500', '讲师');
INSERT INTO `m_job_title` VALUES ('20600', '副教授');
