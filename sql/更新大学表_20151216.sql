ALTER TABLE ktz.m_university
 ADD prov_id INT(10) AFTER id;
 
update m_university as u,m_area as m 
set u.prov_id = m.area_id 
where u.prov_name = m.area_name;

update m_university set prov_id = 900000 where prov_name = '香港';
update m_university set prov_id = 910000 where prov_name = '澳门';
update m_university set prov_id = 920000 where prov_name = '台湾';

