package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;

import comm.Constant;
import comm.Helper;
import entity.BlogEntity;
import entity.JsonEntity;
import entity.QaEntity;
import entity.ReplyEntity;
import impl.BlogImpl;

/**
 * 
 */
public class BlogAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(BlogAction.class);

	private BlogImpl blogService = null;

	private String title = "";
	private String content = "";
	private List<BlogEntity> list = null;
	private BlogEntity info = null;
	
	private List<ReplyEntity> replyList = null;

	/**
	 * 个人列表新闻，没有分页
	 * 
	 * @return
	 */
	public String list() {
		int loginId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.blogService.cnt(loginId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.blogService.list(loginId, page, size, host);
		}
		return "list";
	}
	
	/**
	 * 所有用户新闻，没有分页
	 * 
	 * @return
	 */
	public String allList() {
		int loginId = 0; //登陆ID为0，则代表查询所有
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.blogService.cnt(loginId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.blogService.list(loginId, page, size, host);
		}
		return "allList";
	}
	
	/**
	 * 所有用户的blog
	 * 
	 * @return
	 */
	public String allBlog() {
		int loginId = 0; //登陆ID为0，则代表查询所有
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.blogService.cnt(loginId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.blogService.list(loginId, page, size, host);
		}
		for(BlogEntity blog : list){
			blog.setAnswerCnt(this.blogService.replyListCnt(blog.getId()));
		}
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "allBlog");
		return "allBlog";
	}
	
	public String addReply() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			ReplyEntity entity = new ReplyEntity();
			entity.setQaId(id);
			entity.setContent(getContent());
			entity.setUsrId(super.getLoginId());
			entity.setId(super.getAutoId());
			entity.setType(Constant.Reply_Blog);
			int ret = this.blogService.addReply(entity);
			if (ret > 0) {
				json.setStatus(1);
			}
		}
		super.json(json);
		return null;
	}
	
	/**
	 * 列表
	 * 
	 * @return
	 */
	public String listReply() {
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.blogService.replyListCnt(id);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			this.replyList = this.blogService.replyList(id, page, size, host);
		}
		
		return "list-reply";
	}

	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getUserId();
		super.setId(""+usrId);
		list = this.blogService.list(usrId, 1, 10, host);
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);

		BlogEntity note = this.blogService.info(id);
		setInfo(note);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);

		if (title.equals("")) {
			title = "新闻_"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}

		BlogEntity note = new BlogEntity();
		note.setId(id);
		note.setCategoryId(0);
		note.setTitle(getTitle());
		note.setContent(getContent());
		note.setUsrId(super.getLoginId());

		// 更新
		if (id > 0) {
			int ret = this.blogService.upt(note);
			if (ret > 0) {
				super.setMessage("1");
			} else {
				super.setMessage("保存失败 !");
			}
			return "edit";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				note.setId(super.getAutoId());
				int ret = this.blogService.add(note);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.blogService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<BlogEntity> getList() {
		return list;
	}

	public void setList(List<BlogEntity> list) {
		this.list = list;
	}

	public BlogImpl getBlogService() {
		return blogService;
	}

	public void setBlogService(BlogImpl blogService) {
		this.blogService = blogService;
	}

	public BlogEntity getInfo() {
		return info;
	}

	public void setInfo(BlogEntity info) {
		this.info = info;
	}

	public List<ReplyEntity> getReplyList() {
		return replyList;
	}

	public void setReplyList(List<ReplyEntity> replyList) {
		this.replyList = replyList;
	}

}