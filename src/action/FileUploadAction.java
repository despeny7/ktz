package action;

import impl.FileUploadImpl;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import comm.Helper;


import entity.AttachmentEntity;
import entity.JsonListEntity;

/**
 * 
 */
public class FileUploadAction extends Struts2Action {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ImageAction.class);
	
	 // cfile属性用来封装上传的文件  
    private File cfile;  
    
    //关联实体主键，不能使用符合主键
	private String entityId;
	
	//实体名称，使用类全名关联
	private String entityName;
	
	private FileUploadImpl fileUploadService;
	
	private List<AttachmentEntity> listAttachment = new ArrayList<AttachmentEntity>();
	
	private static final long DEFAULT_MAX_FILE_SIZE = 10*1024*1024;
	//附件名字
	private String cfileFileName;

	public String uploadAttachment(){
		try {
			InputStream input = new BufferedInputStream(new FileInputStream(cfile));
			byte[] b = Helper.input2byte(input);
			input.read(b);
			JsonListEntity json = new JsonListEntity();
			if(cfileFileName != null && cfileFileName.indexOf(".exe") >= 0){
				json.setHtml("不能上传后缀为exe的应用程序文件！");
				super.json(json);
			    return null;
			}
			if(b.length > DEFAULT_MAX_FILE_SIZE){
    			json.setHtml("不能上传大10M的附件！");
    			super.json(json);
    		    return null;
    		}
			
			Date date = new Date();
    		//由于文件可能过多，所以按当前月份分文件夹
    		SimpleDateFormat sf = new SimpleDateFormat("yyyyMM");
    		String folderName = filePath + "/" + sf.format(date);
    		File folder = new File(folderName);
    		if(!folder.exists() && !folder.isDirectory()){
    			folder.mkdirs();
    		}

    		SimpleDateFormat sf2 = new SimpleDateFormat("yyyyMMddKKmmss");
    		Random r = new Random();
    		//文件新名称按日期：年月日时分秒+三位随机数生成
    		String fileAutoName = "";
			File file = new File(folderName + "/" + fileAutoName);
			do {
				fileAutoName = sf2.format(date) + "_" + r.nextInt(1000);  
				file = new File(folderName + "/" + fileAutoName);
			} while (file.exists());
			logger.info("entityId = " +entityId + ";url=" + file.getPath());
            FileOutputStream o = new FileOutputStream(file);
            o.write(b);
            o.close();
            
            //保存到数据库
            AttachmentEntity entity = new AttachmentEntity();
    		entity.setEntityId(entityId);
    		entity.setEntityName(entityName);
    		entity.setFileName(cfileFileName);
    		entity.setName(sf.format(date) + "/" + fileAutoName); //保存时要把父文件夹也就是年月文件夹保存到数据，否则下载时找不到属于哪个年月文件夹
    		entity.setId(super.getAutoId());
    		fileUploadService.add(entity);
    		
    		if(entity.getFileName().length() > 20){
    			entity.setShortFileName(entity.getFileName().substring(0, 20) + "...");
    		}else{
    			entity.setShortFileName(entity.getFileName());
    		}
    		listAttachment.add(entity);
    		json.setList(listAttachment);
    		json.setState(1);
    		
    		super.json(json);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除附件和数据库记录
	 * @return
	 */
	public String delFile(){
		int fileId = Helper.parseInt(super.getId());
		AttachmentEntity entity = this.fileUploadService.info(fileId);
		if(entity != null){
			File file = new File(filePath + "/" + entity.getName());
			if(file.exists()){
				file.delete();
			}
			fileUploadService.del(fileId);
		}
		return null;
	}
	
	public String downloadFile(){
		try {
			logger.info("开始下载附件；entityId = " +super.getId());
			HttpServletResponse response = ServletActionContext.getResponse();
			HttpServletRequest request = ServletActionContext.getRequest();
			int fileId = Helper.parseInt(super.getId());
			AttachmentEntity entity = this.fileUploadService.info(fileId);
			File file = new File(filePath + "/" + entity.getName());
			if(file.exists()){
				logger.info("entityId = " +fileId + ";url=" + file.getPath());
				String agent = request.getHeader("User-Agent");
				boolean isMSIE = (agent != null && agent.indexOf("MSIE") != -1);
				String fileName = java.net.URLEncoder.encode(entity.getFileName(), "utf-8");
				if(!isMSIE){
					//如果浏览器是火狐，用下面编码格式
					fileName = new String(entity.getFileName().getBytes("UTF-8"), "ISO-8859-1");
				}
				fileName=fileName.replace("+","%20");
				InputStream inputStream = new FileInputStream(file);
				byte[] b = new byte[inputStream.available()];
				
				response.setCharacterEncoding("utf-8");
		        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		        response.setContentType("application/x-msdownload;charset=utf-8");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				int i = 0;
				while ((i = inputStream.read(b)) > 0) {
	                servletOutputStream.write(b, 0, i);
	            }
				inputStream.close();
				servletOutputStream.flush();
		        servletOutputStream.close();
			}else{
				logger.info("附件不存在;entityId = " +fileId + ";filePath=" + filePath + "/" + entity.getName());
			}
		} catch (Exception e) {
            e.printStackTrace();
        }
        return null;
	}
	
	/**
	 * 根据实体单号和实体entity查询对应的附件
	 * @return
	 */
	public String fileList(){
		this.listAttachment = fileUploadService.listAttachment(Long.parseLong(entityId), entityName);
		JsonListEntity json = new JsonListEntity();
		json.setList(listAttachment);
		json.setState(1);
		super.json(json);
		return null;
	}


	public File getCfile() {
		return cfile;
	}

	public void setCfile(File cfile) {
		this.cfile = cfile;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public FileUploadImpl getFileUploadService() {
		return fileUploadService;
	}

	public void setFileUploadService(FileUploadImpl fileUploadService) {
		this.fileUploadService = fileUploadService;
	}

	public List<AttachmentEntity> getListAttachment() {
		return listAttachment;
	}

	public void setListAttachment(List<AttachmentEntity> listAttachment) {
		this.listAttachment = listAttachment;
	}

	public String getCfileFileName() {
		return cfileFileName;
	}

	public void setCfileFileName(String cfileFileName) {
		this.cfileFileName = cfileFileName;
	}
	
}