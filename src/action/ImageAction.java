package action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import comm.Encrypt;
import comm.Helper;
import entity.ImageEntity;
import image.ImageSize;
import image.Imager;
import image.ImagerEntity;
import impl.ImageImpl;

/**
 * 
 */
public class ImageAction extends Struts2Action {

	public ImageImpl getImageService() {
		return imageService;
	}

	public void setImageService(ImageImpl imageService) {
		this.imageService = imageService;
	}

	public File getCfile() {
		return cfile;
	}

	public void setCfile(File cfile) {
		this.cfile = cfile;
	}

	public String getCfileFileName() {
		return cfileFileName;
	}

	public void setCfileFileName(String cfileFileName) {
		this.cfileFileName = cfileFileName;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ImageAction.class);
	private ImageImpl imageService = null;

	private String cfileFileName;
	private File cfile;
	private List<ImageEntity> list = null;

	/**
	 * 头像
	 * @return
	 */
	public String hp() {
		upload(2, true);
		return null;
	}

	/**
	 * 普通
	 * @return
	 */
	public String im() {
		upload(1, false);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String list() {
		// TODO
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.imageService.cnt();
		super.setAttr("record", cnt);
		if (cnt > 0) {
			List<ImageEntity> list = this.imageService.list(page, size, host);
			setList(list);
		}
		return "list";
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private int upload(int type, boolean isAvatar) {
		int imgId = 0;
		int loginId = super.getLoginId();
		try {

			InputStream input = new BufferedInputStream(new FileInputStream(cfile));
			byte[] bytes = Helper.input2byte(input);
			String name = cfileFileName;
			String ext = "jpg";

			boolean zoom = false;
			int size = 1;
			if (type == 1) {
				zoom = true;
			}

			ImagerEntity entity = new Imager().create(bytes, ext, path, zoom, isAvatar);

			if (entity.getStatus() == 1) {
				if (zoom) {
					// 1:o 2:s 4:m 8:l
					size = 1 + 2 + 4 + 8;
				}
				int length = (int) cfile.length();
				String data = Encrypt.encryptBASE64(bytes);
				ImageEntity img = new ImageEntity();
				img.setData(data);
				img.setOrgExt(ext);
				img.setOrgName(name);
				img.setOrgLength(length);
				img.setCategoryId(type);
				img.setFolder(entity.getFolder());
				img.setImgId(entity.getId());
				img.setImgName(entity.getName());
				img.setUsrId(loginId);
				img.setSize(size);
				imageService.add(img);
			}

			ServletActionContext.getResponse().setCharacterEncoding("utf-8");
			ServletActionContext.getResponse().setContentType("application/json");
			imgId = entity.getId();
			String url = new Imager().get(imgId, ImageSize.O, host);
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append("\"id\":\"" + entity.getId() + "\",");
			sb.append("\"width\":\"" + entity.getWidth() + "\",");
			sb.append("\"height\":\"" + entity.getHeight() + "\",");
			sb.append("\"url\":\"" + url + "\"");
			sb.append("}");

			// System.out.println(sb.toString());
			ServletActionContext.getResponse().getWriter().write(sb.toString());
			ServletActionContext.getResponse().getWriter().flush();
			ServletActionContext.getResponse().getWriter().close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return imgId;
	}

	// /**
	// *
	// * @return
	// */
	// public String add(int itype) {
	// JsonEntity json = new JsonEntity();
	// json.setStatus(0);
	// String data = this.d;
	// data = trim(data);
	// String v1 = this.v;
	// v1 = trim(v1);
	// String name = this.n;
	// name = trim(name);
	// String ext = this.e;
	// ext = trim(ext);
	// String size = this.s;
	// size = trim(size);
	// String type = this.t;
	// type = trim(type);
	//
	// if (data.length() > 16) {
	// String v2 = Encrypt.md5(data + "@nmit.office");
	// v2 = v2.substring(0, 4);
	// if (v1.equals(v2)) {
	// try {
	// byte[] bytes = Encrypt.decryptBASE64(data);
	// ImagerEntity entity = new Imager().create(itype, bytes,
	// ext, path);
	//
	// if (entity.getStatus() == 1) {
	// ImageEntity img = new ImageEntity();
	// img.setData(data);
	// img.setOrgExt(ext);
	// img.setOrgName(name);
	// img.setOrgSize(Helper.parseInt(size));
	// img.setCategoryId(itype);
	// img.setFolder(entity.getFolder());
	// img.setImgId(entity.getId());
	// img.setImgName(entity.getName());
	// imageService.add(img);
	//
	// json.setStatus(img.getImgId());
	// json.setInfo("成功");
	// }
	// } catch (Exception e1) {
	// logger.error(e1);
	// json.setStatus(-1);
	// json.setInfo(e.toString());
	// }
	// } else {
	// json.setStatus(-3);// 验证错误
	// json.setInfo("验证错误");
	// }
	// } else {
	// json.setStatus(-2);// 参数错误
	// json.setInfo("参数错误");
	// }
	//
	// super.json(json);
	// return null;
	// }

	/**
	 * 
	 * @return
	 */
	public String get() {
		// JsonEntity json = new JsonEntity();
		// json.setStatus(0);
		// String v1 = this.v;
		// v1 = trim(v1);
		// String sid = super.getId();
		// int imgId = Helper.parseInt(sid);
		//
		// if (imgId > 0) {
		// String v2 = Encrypt.md5(imgId + "@nmit.office");
		// v2 = v2.substring(0, 4);
		// if (v1.equals(v2)) {
		// try {
		// String size = this.s;
		// size = trim(size);
		// int isize = Helper.parseInt(size);// 图片大小 1:s 2:m 3:l
		//
		// String url = new Imager().get(imgId, isize, host);
		// json.setStatus(1);
		// json.setInfo(url);
		//
		// } catch (Exception e1) {
		// logger.error(e1);
		// json.setStatus(-1);
		// json.setInfo(e.toString());
		// }
		// } else {
		// json.setStatus(-3);// 验证错误
		// json.setInfo("验证错误");
		// }
		// } else {
		// json.setStatus(-2);// 参数错误
		// json.setInfo("参数错误");
		// }

		// super.json(json);
		return null;
	}

	public List<ImageEntity> getList() {
		return list;
	}

	public void setList(List<ImageEntity> list) {
		this.list = list;
	}

}