package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;

import comm.Helper;
import entity.JobEntity;
import entity.JsonEntity;
import impl.JobImpl;

/**
 * 
 */
public class JobAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(JobAction.class);

	private JobImpl jobService = null;

	private String title = "";
	private String content = "";
	private List<JobEntity> list = null;
	private JobEntity info = new JobEntity();

	/**
	 * 列表，没有分页
	 * 
	 * @return
	 */
	public String list() {
		//查询指定用户或者所有用户的招聘信息
		int userId = Helper.parseInt(getId());
		if(userId < 0){
			userId = 0;
			super.setAuthorize("1");
		}
		
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.jobService.cnt(userId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.jobService.list(userId, page, size, 100);
		}
		System.out.println(super.getAuthorize() + "--------------------");
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "jobs");
		return "list";
	}
	
	/**
	 * 列表，没有分页
	 * 
	 * @return
	 */
	public String myJobList() {
		int loginId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.jobService.cnt(loginId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.jobService.list(loginId, page, size, 100);
		}
		return "myJobList";
	}

	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getUserId();
		super.setId(""+usrId);
		list = this.jobService.list(0, 1, 10, 100);
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);

		this.info = this.jobService.info(id);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "jobs");
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		if(null != this.getId() && !"".equals(this.getId())){
			this.info = this.jobService.info(Integer.parseInt(this.getId()));
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		info.setCategoryId(0);
		info.setUsrId(super.getLoginId());

		// 更新
		if (info.getId() > 0) {
			int ret = this.jobService.upt(info);
			if (ret > 0) {
				super.setMessage("1");
			} else {
				super.setMessage("保存失败 !");
			}
			return this.list();
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				info.setId(super.getAutoId());
				int ret = this.jobService.add(info);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.jobService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public JobImpl getJobService() {
		return jobService;
	}

	public void setJobService(JobImpl jobService) {
		this.jobService = jobService;
	}

	public List<JobEntity> getList() {
		return list;
	}

	public void setList(List<JobEntity> list) {
		this.list = list;
	}

	public JobEntity getInfo() {
		return info;
	}

	public void setInfo(JobEntity info) {
		this.info = info;
	}

}