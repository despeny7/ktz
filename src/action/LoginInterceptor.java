package action;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

import impl.AppContext;
import impl.LogImpl;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import comm.Helper;

import entity.LogEntity;
import entity.UserEntity;

public class LoginInterceptor extends AbstractInterceptor {
	private static final long serialVersionUID = 1L;

	LogImpl logImpl = (LogImpl) AppContext.getInstance().getBean("logService");

	@Override
	/* * 验证是否已登录 LOGIN_TYPE = 1 :已登录 LOGIN_TYPE = 0 :未登录 */
	public String intercept(ActionInvocation invocation) throws Exception {
		try {
			Map<String, Object> session = ActionContext.getContext()
					.getSession();
			UserEntity user = (UserEntity) session.get("user_info");

			HttpServletRequest request = (HttpServletRequest) ActionContext
					.getContext().get(ServletActionContext.HTTP_REQUEST);
			String uri = request.getRequestURI();
			String ip = Helper.getIP(ServletActionContext.getRequest());
			int usrId = -1;
			if (null != user) {
				usrId = user.getId();
			}

			// 仅仅是GET数据
			String qs = request.getQueryString();
			String paras = qs;

			String usrAgent = request.getHeader("User-Agent");
			String referer = request.getHeader("Referer");

			LogEntity log = new LogEntity();
			log.setIp(ip);
			log.setParas(paras);
			log.setReferer(referer);
			log.setUrl(uri);
			log.setUser_agent(usrAgent);
			log.setUsr_id(usrId);

			logImpl.add(log);

			// System.out.println("user:" + user + ">>>>> uri:" + uri);
			// 设置哪些页面需要验证
			// 如果没验证
			if (null == user) {
				// 那些URL可以例外
				// System.out.println(uri + "<<<<<<" +
				// uri.indexOf("user_index"));
				// if (uri.equals("") || uri.equals("/")
				// || uri.equals(request.getContextPath())
				// || uri.equals(request.getContextPath() + "/")
				// || uri.equals("/"+request.getContextPath() + "/")
				// || uri.indexOf("user_login") > -1
				// || uri.indexOf("user_index") > -1
				// || uri.indexOf("user_logout") > -1
				// || uri.indexOf("user_ckVcode") > -1) {
				// // goto;
				// } else {
				// return "logout";
				// }
				// 
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		String result = invocation.invoke();
		return result;
	}
}
