package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import comm.CnLang;
import comm.Constant;
import comm.Helper;
import entity.JsonEntity;
import entity.MessageEntity;
import entity.NoteEntity;
import entity.UserEntity;
import impl.MessageImpl;
import impl.NoteImpl;

/**
 * 
 */
public class MessageAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(MessageAction.class);

	private MessageImpl messageService = null;
	private List<MessageEntity> list = null;
	private String content = "";

	/**
	 * 列表
	 * 
	 * @return
	 */
	public String list() {
		int userId = super.getLoginId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.messageService.cnt(userId);
		super.setAttr("record", cnt);
		List<MessageEntity> mList = null;
		if (0 < cnt) {
			mList = this.messageService.list(userId, page, size);
			setList(mList);
		}
		return "list";
	}
	
	/**
	 * 未读消息列表
	 * 
	 * @return
	 */
	public String newMsgList() {
		int userId = super.getLoginId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.messageService.unCnt(userId);
		super.setAttr("record", cnt);
		List<MessageEntity> mList = null;
		if (0 < cnt) {
			mList = this.messageService.newMsgList(userId, page, size);
			setList(mList);
		}
		return "newMsgList";
	}

	/**
	 * 添加好友请求
	 * 
	 * @return
	 */
	public String addfriendMsg() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		String id = super.getId();
		int receiverId = Helper.parseInt(id);
		int loginId = super.getLoginId();

		int ret = this.messageService.add(super.getAutoId(), loginId,
				receiverId);
		if (0 < ret) {
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	/**
	 * 删除记录
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		String id = super.getId();
		int msgId = Helper.parseInt(id);
		int loginId = super.getLoginId();
		int ret = this.messageService.del(msgId, loginId);
		if (ret > 0) {
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	/**
	 * 更新记录成已读
	 * 
	 * @return
	 */
	public String upt() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		String id = super.getId();
		int msgId = Helper.parseInt(id);
		int loginId = super.getLoginId();
		int ret = this.messageService.upt(msgId, loginId);
		if (ret > 0) {
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	/**
	 * 最新未读的消息数
	 * 
	 * @return
	 */
	public String unCnt() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		String id = super.getId();
		int loginId = super.getLoginId();
		int ret = this.messageService.unCnt(loginId);
		if (ret > 0) {
			json.setStatus(ret);
		}
		super.json(json);
		return null;
	}
	
	/**
	 * 课题组添加成员请求
	 * 
	 * @return
	 */
	public String ktzAddMemberMsg() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		int receiverId = Helper.parseInt(super.getId());
		int loginId = super.getLoginId();
		String content = "课题组邀请您加入该课题组！";
		int ret = this.messageService.add(super.getAutoId(), loginId, receiverId, Constant.MESSAGE_KTZREQ, content);
		if (0 < ret) {
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}
	
	/**
	 * 拒绝加入课题组
	 * @return
	 */
	public String refuse(){
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		int msgId = Helper.parseInt(super.getId());
		int loginId = super.getLoginId();
		int ret = this.messageService.uptmMsgCategory(msgId, loginId, Constant.MESSAGE_REFUSE);
		if (0 < ret) {
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public MessageImpl getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageImpl messageService) {
		this.messageService = messageService;
	}

	public List<MessageEntity> getList() {
		return list;
	}

	public void setList(List<MessageEntity> list) {
		this.list = list;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}