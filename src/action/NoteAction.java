package action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.AttachmentEntity;
import entity.JsonEntity;
import entity.NoteEntity;
import impl.FileUploadImpl;
import impl.NoteImpl;

/**
 * 
 */
public class NoteAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(NoteAction.class);

	private NoteImpl noteService = null;

	private String title = "";
	private String content = "";
	private List<NoteEntity> list = null;
	private NoteEntity noteInfo = new NoteEntity();
	private FileUploadImpl fileUploadService;
	private String entityId; //附件外键id，新增附件时生成个随机数当临时id，当实体保存后，用实体id替换临时id
	private List<AttachmentEntity> listAttachment = new ArrayList<AttachmentEntity>();

	/**
	 * 列表
	 * 
	 * @return
	 */
	public String list() {
		int userId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.noteService.cnt(userId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.noteService.list(userId, page, size);
			for(NoteEntity entity : list){
				entity.setListAttachment(fileUploadService.listAttachment(Long.parseLong(entity.getId() + ""), "NoteEntity"));
			}
		}
		return "list";
	}
	
	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getUserId();
		super.setId(""+usrId);
		list = this.noteService.list(usrId, 1, 10);
		for(NoteEntity entity : list){
			entity.setListAttachment(fileUploadService.listAttachment(Long.parseLong(entity.getId() + ""), "NoteEntity"));
		}
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);
		noteInfo = this.noteService.info(id);
		listAttachment = fileUploadService.listAttachment(Long.parseLong(sId), "NoteEntity");
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		if(null != this.getId() && !"".equals(this.getId())){
			this.noteInfo = this.noteService.info(Integer.parseInt(this.getId()));
			this.listAttachment = fileUploadService.listAttachment(Long.parseLong(this.getId()), "NoteEntity");
			//编辑的时候需要把外键id传到上传附件页面
			this.entityId = this.noteInfo.getId()+"";
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		noteInfo.setCategoryId(0);
		noteInfo.setUsrId(super.getLoginId());

		// 更新
		if (noteInfo.getId() > 0) {
			int ret = this.noteService.upt(noteInfo);
			if (ret > 0) {
				super.setMessage("1");
				return "save-success";
			} else {
				super.setMessage("修改失败 !");
			}
			return "add";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				noteInfo.setId(super.getAutoId());
				int ret = this.noteService.add(noteInfo);
				if (ret > 0) {
					//保存成功后，把实体id替换保存附件的外键id
					this.fileUploadService.uptEntityId(noteInfo.getId()+"", entityId);
					
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}
	
	/**
	 * 模态窗口添加论文
	 * @return
	 */
	public String modelSave() {
		noteInfo.setCategoryId(0);
		noteInfo.setUsrId(super.getLoginId());

		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		} else {
			// 自动生成Id
			noteInfo.setId(super.getAutoId());
			this.noteService.add(noteInfo);
			
			//保存成功后，把实体id替换保存附件的外键id
			this.fileUploadService.uptEntityId(noteInfo.getId()+"", entityId);
		}
		return "to_main";
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.noteService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<NoteEntity> getList() {
		return list;
	}

	public void setList(List<NoteEntity> list) {
		this.list = list;
	}

	public NoteEntity getNoteInfo() {
		return noteInfo;
	}

	public void setNoteInfo(NoteEntity noteInfo) {
		this.noteInfo = noteInfo;
	}

	public NoteImpl getNoteService() {
		return noteService;
	}

	public void setNoteService(NoteImpl noteService) {
		this.noteService = noteService;
	}

	public FileUploadImpl getFileUploadService() {
		return fileUploadService;
	}

	public void setFileUploadService(FileUploadImpl fileUploadService) {
		this.fileUploadService = fileUploadService;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public List<AttachmentEntity> getListAttachment() {
		return listAttachment;
	}

	public void setListAttachment(List<AttachmentEntity> listAttachment) {
		this.listAttachment = listAttachment;
	}

}