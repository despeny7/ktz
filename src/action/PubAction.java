package action;

import impl.PubImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xerces.internal.util.URI;

import comm.Constant;
import comm.Helper;

import entity.CategoryEntity;
import entity.JsonListEntity;

/**
 * User类
 */
public class PubAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PubImpl pubService;
	private List<CategoryEntity> list = null;
	private String id = "";
	
	public static List<CategoryEntity> memberLevelList = new ArrayList<CategoryEntity>();
	static{
		memberLevelList.add(new CategoryEntity(Constant.HEADMAN, "组长"));
		memberLevelList.add(new CategoryEntity(Constant.TEACHERMAN, "合作老师"));
		memberLevelList.add(new CategoryEntity(Constant.STUDENTMAN, "合作学生"));
	}
	
	public static List<CategoryEntity> noteTypeList = new ArrayList<CategoryEntity>();
	static{
		noteTypeList.add(new CategoryEntity(1, "论文"));
		noteTypeList.add(new CategoryEntity(2, "专利"));
		noteTypeList.add(new CategoryEntity(3, "专著"));
	}

	public PubImpl getPubService() {
		return pubService;
	}

	public void setPubService(PubImpl pubService) {
		this.pubService = pubService;
	}

	/**
	 * 验证
	 * 
	 * @return
	 */
	public String city() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			String q = getQ();
			// System.out.println(q);
			List<CategoryEntity> list = this.pubService.cityList(q);
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}

	/**
	 * 验证
	 * 
	 * @return
	 */
	public String deptx() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			String q = getQ();
			int uid = Helper.parseInt(this.getId());
			// System.out.println(q);
			List<CategoryEntity> list = this.pubService.deptList(uid);
			List<CategoryEntity> result = new ArrayList<CategoryEntity>();
			int count = 0;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getCategoryName().indexOf(q) > -1) {
					result.add(list.get(i));
					count++;
					if (count > 10) {
						break;
					}
				}
			}
			if (result.size() < 1) {
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getCategoryName().indexOf("其他") > -1) {
						result.add(list.get(i));
						break;
					}
				}
			}
			if (result.size() < 1) {
				CategoryEntity entity = new CategoryEntity();
				entity.setCategoryName("没有合适的院系");
				entity.setCategoryId(0);
				result.add(entity);
			}
			if (result != null) {
				json.setList(result);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}

	/*
	 * 验证
	 * 
	 * @return
	 */
	public String degree() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			List<CategoryEntity> list = this.pubService.degreeList();
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}

	/*
	 * 验证 职称
	 * 
	 * @return
	 */
	public String title() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			List<CategoryEntity> list = this.pubService.titleList();
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}
	
	/*
	 * 招聘职称要求
	 * 
	 * @return
	 */
	public String jobTitle() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			List<CategoryEntity> list = this.pubService.jobTitleList();
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}

	/*
	 * 验证
	 * 
	 * @return
	 */
	public String subject() {
		List<CategoryEntity> cList = this.pubService.subjectList(0);
		setList(cList);
		return "subject";
	}

	public String editSubject() {
		list = this.pubService.subjectList(0);
		return "editSubject";
	}

	/*
	 * 验证
	 * 
	 * @return
	 */
	public String univ() {
		String pname = new String(id);
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			List<CategoryEntity> list = this.pubService.univList(pname);
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}

	/*
	 * 验证
	 * 
	 * @return
	 */
	public String dept() {
		String pname = this.getId();
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		try {
			int univId = Helper.parseInt(pname);
			List<CategoryEntity> list = this.pubService.deptList(univId);
			if (list != null) {
				json.setList(list);
				json.setState(1);
			}
		} catch (Exception e) {
			logger.error(e.fillInStackTrace());
		}
		super.json(json);
		return null;
	}
	
	public String memberLevel(){
		JsonListEntity json = new JsonListEntity();
		json.setList(memberLevelList);
		json.setState(1);
		super.json(json);
		return null;
	}
	
	/**
	 * 论文专著类型
	 * @return
	 */
	public String noteType(){
		JsonListEntity json = new JsonListEntity();
		json.setList(noteTypeList);
		json.setState(1);
		super.json(json);
		return null;
	}

	public List<CategoryEntity> getList() {
		return list;
	}

	public void setList(List<CategoryEntity> list) {
		this.list = list;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}