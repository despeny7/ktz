package action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;

import comm.Constant;
import comm.Helper;
import entity.FollowEntity;
import entity.JsonEntity;
import entity.QaEntity;
import entity.QaEntity;
import entity.ReplyEntity;
import impl.FollowImpl;
import impl.NoteImpl;
import impl.QAImpl;

/**
 * 
 */
public class QAAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(QAAction.class);

	private QAImpl qaService = null;

	private String title = "";
	private String content = "";
	private List<QaEntity> list = null;
	private List<ReplyEntity> replyList = null;
	private QaEntity qaInfo = new QaEntity();
	private FollowImpl followService = null;

	/**
	 * 查询所有提问列表
	 * 
	 * @return
	 */
	public String list() {
		int userId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.qaService.listCnt(0);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.qaService.list(0, page, size, userId,host);
			for(QaEntity qa : list){
				qa.setAnswerCnt(this.qaService.replyListCnt(qa.getId()));
			}
		}
		return "list";
	}
	
	/**
	 * 我的提问列表
	 * 
	 * @return
	 */
	public String myList() {
		int userId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.qaService.listCnt(userId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.qaService.list(userId, page, size, 0,host);
		}
		return "myList";
	}
	
	/**
	 * 我的回答列表
	 * 
	 * @return
	 */
	public String myAnswerList() {
		int userId = super.getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.qaService.myAnswerListCnt(userId);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			list = this.qaService.myAnswerList(userId, page, size);
		}
		return "myAnswerList";
	}

	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int usrId = getUserId();
		super.setId("" + usrId);
		this.list = this.qaService.top(0, 10, usrId, host);
		for(QaEntity qa : list){
			qa.setAnswerCnt(this.qaService.replyListCnt(qa.getId()));
		}
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "qa");
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);
		qaInfo = this.qaService.info(id);
		System.out.println(qaInfo.getQaLabelList());
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);

		if (title.equals("")) {
			title = "文章_"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}

		qaInfo.setId(id);
		qaInfo.setCategoryId(0);
		qaInfo.setUsrId(super.getLoginId());

		// 更新
		if (id > 0) {
			int ret = this.qaService.upt(qaInfo);
			if (ret > 0) {
				super.setMessage("1");
			} else {
				super.setMessage("保存失败 !");
			}
			return "edit";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				qaInfo.setId(super.getAutoId());
				int ret = this.qaService.add(qaInfo);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}
	
	/**
	 * 模态窗口添加问答
	 * @return
	 */
	public String modelSave() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);

		if (title.equals("")) {
			title = "文章_"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}
		
		

		qaInfo.setId(id);
		qaInfo.setCategoryId(0);
		qaInfo.setUsrId(super.getLoginId());

		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		} else {
			// 自动生成Id
			qaInfo.setId(super.getAutoId());
			this.qaService.add(qaInfo);
		}
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		if("main".equals(session.get("showMenu"))){
			//如果添加窗口是在主页打开的，就返回主页
			return "to_main";
		}else if("qa".equals(session.get("showMenu"))){
			//如果添加窗口是在问答页面打开的，就返回到问答页面
			return this.top();
		}else{
			return "to_main";
		}
	}

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.qaService.del(id, loginId);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public String addReply() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			ReplyEntity entity = new ReplyEntity();
			entity.setQaId(id);
			entity.setContent(getContent());
			entity.setUsrId(super.getLoginId());
			entity.setId(super.getAutoId());
			entity.setType(Constant.Reply_Qa);
			int ret = this.qaService.addReply(entity);
			if (ret > 0) {
				json.setStatus(1);
			}
		}
		super.json(json);
		return null;
	}
	
	/**
	 * 列表
	 * 
	 * @return
	 */
	public String listReply() {
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.qaService.replyListCnt(id);
		super.setAttr("record", cnt);
		if (0 < cnt) {
			this.replyList = this.qaService.replyList(id, page, size, host);
		}
		
		return "list-reply";
	}
	
	//关注问题
	public String qaFollowAdd(){
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}else{
			FollowEntity follow = new FollowEntity();
			follow.setId(super.getAutoId());
			int qaId = Helper.parseInt(super.getId());
			follow.setObjId(qaId);
			follow.setType(Constant.QA);
			follow.setUserId(super.getLoginId());
			this.followService.add(follow);
		}
		return null;
	}
	
	//取消问题关注
	public String qaFollowCanel(){
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}else{
			FollowEntity follow = new FollowEntity();
			int qaId = Helper.parseInt(super.getId());
			follow.setObjId(qaId);
			follow.setUserId(super.getLoginId());
			follow.setType(Constant.QA);
			this.followService.del(follow);
		}
		return null;
	}

//	/**
//	 * 
//	 * @return
//	 */
//	public String delReply() {
//		JsonEntity json = new JsonEntity();
//		json.setStatus(0);
//		String sId = super.getId();
//		int id = Helper.parseInt(sId);
//		int loginId = super.getLoginId();
//		// 成功
//		if (id > 0 && loginId > 0) {
//			this.qaService.deReply(id, loginId);
//			json.setStatus(1);
//		}
//		super.json(json);
//		return null;
//	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<QaEntity> getList() {
		return list;
	}

	public void setList(List<QaEntity> list) {
		this.list = list;
	}

	public QaEntity getQaInfo() {
		return qaInfo;
	}

	public void setQaInfo(QaEntity qaInfo) {
		this.qaInfo = qaInfo;
	}

	public QAImpl getQaService() {
		return qaService;
	}

	public void setQaService(QAImpl qaService) {
		this.qaService = qaService;
	}

	public List<ReplyEntity> getReplyList() {
		return replyList;
	}

	public void setReplyList(List<ReplyEntity> replyList) {
		this.replyList = replyList;
	}

	public FollowImpl getFollowService() {
		return followService;
	}

	public void setFollowService(FollowImpl followService) {
		this.followService = followService;
	}
}