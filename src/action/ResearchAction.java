package action;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.ResearchEntity;
import impl.ResearchImpl;

/**
 * 
 */
public class ResearchAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ResearchAction.class);

	
	private ResearchEntity researchEntity = new ResearchEntity();
	
	private ResearchImpl researchService = null;
	
	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int userId = getUserId();
		researchEntity = this.researchService.searchByUserId(userId);
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);
		researchEntity = this.researchService.info(id);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		if(null != this.getId() && !"".equals(this.getId())){
			this.researchEntity = this.researchService.info(Integer.parseInt(this.getId()));
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {

		researchEntity.setCategoryId(0);
		researchEntity.setUsrId(super.getLoginId());

		// 更新
		if (researchEntity.getId() > 0) {
			int ret = this.researchService.upt(researchEntity);
			if (ret > 0) {
				super.setMessage("1");
				return "save-success";
			} else {
				super.setMessage("修改失败 !");
			}
			return "add";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				researchEntity.setId(super.getAutoId());
				int ret = this.researchService.add(researchEntity);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}

	public ResearchEntity getResearchEntity() {
		return researchEntity;
	}

	public void setResearchEntity(ResearchEntity researchEntity) {
		this.researchEntity = researchEntity;
	}

	public ResearchImpl getResearchService() {
		return researchService;
	}

	public void setResearchService(ResearchImpl researchService) {
		this.researchService = researchService;
	}
	
}