package action;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.ResumeEntity;
import impl.ResumeImpl;

/**
 * 
 */
public class ResumeAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ResumeAction.class);

	
	private ResumeEntity resumeEntity = new ResumeEntity();
	
	private ResumeImpl resumeService = null;
	
	/**
	 * top
	 * 
	 * @return
	 */
	public String top() {
		int userId = getUserId();
		resumeEntity = this.resumeService.resumeUserId(userId);
		return "top";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);
		resumeEntity = this.resumeService.info(id);
		return "info";
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		if (super.getLoginId() == 0) {
			super.setMessage("无权访问!");
		}
		if(null != this.getId() && !"".equals(this.getId())){
			this.resumeEntity = this.resumeService.info(Integer.parseInt(this.getId()));
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {

		resumeEntity.setUsrId(super.getLoginId());

		// 更新
		if (resumeEntity.getId() > 0) {
			int ret = this.resumeService.upt(resumeEntity);
			if (ret > 0) {
				super.setMessage("1");
				return "save-success";
			} else {
				super.setMessage("修改失败 !");
			}
			return "add";
		} else {
			if (super.getLoginId() == 0) {
				super.setMessage("无权访问!");
			} else {
				// 自动生成Id
				resumeEntity.setId(super.getAutoId());
				int ret = this.resumeService.add(resumeEntity);
				if (ret > 0) {
					super.setMessage("1");
					return "save-success";
				}
				super.setMessage("保存失败 !");
			}
			return "add";
		}
	}

	public ResumeEntity getResumeEntity() {
		return resumeEntity;
	}

	public void setResumeEntity(ResumeEntity resumeEntity) {
		this.resumeEntity = resumeEntity;
	}

	public ResumeImpl getResumeService() {
		return resumeService;
	}

	public void setResumeService(ResumeImpl resumeService) {
		this.resumeService = resumeService;
	}

}