package action;

import java.util.List;

import org.apache.log4j.Logger;

import comm.Helper;
import entity.CategoryEntity;
import entity.JsonEntity;
import entity.JsonListEntity;
import entity.TypoEntity;
import impl.TypoImpl;

/**
 * 
 */
public class TypoAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(TypoAction.class);

	private TypoImpl typoService = null;

	// input/output
	private String cid = "";
	private String title = "";
	private String content = "";
	private String author = "";
	private String origin = "";
	private String url = "";
	private List<TypoEntity> list = null;
	private TypoEntity noteInfo = null;
	private List<CategoryEntity> categoryList = null;

	/**
	 * 
	 * @return
	 */
	public String del() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		String sId = super.getId();
		int id = Helper.parseInt(sId);
		int loginId = super.getLoginId();
		// 成功
		if (id > 0 && loginId > 0) {
			this.typoService.del(loginId, id);
			json.setStatus(1);
		}
		super.json(json);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String category() {
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		List<CategoryEntity> list = this.typoService.categoryList();
		json.setList(list);
		json.setState(1);
		super.json(json);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String add() {
		super.setId("" + 0);
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String edit() {
		super.setMessage(getMessage());
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);
		if (id > 0 && super.getLoginId() > 0) {
			TypoEntity note = this.typoService.info(id);
			if (note != null) {
				this.title = note.getTitle();
				this.content = note.getContent();
				this.cid = "" + note.getCategoryId();
				this.author = note.getAuthor();
				this.url = note.getOriginUrl();
				this.origin = note.getOrigin();
			}
		} else {
			super.setMessage("参数错误或无权访问!");
		}
		return "add";
	}

	/**
	 * 
	 * @return
	 */
	public String save() {
		String sId = this.getId();
		int id = Helper.parseInt(sId);
		setId("" + id);

		boolean bl = true;
		String title = getTitle();
		title = title.trim();
		String content = getContent();
		String csid = getCid();
		int cid = Helper.parseInt(csid);
		if (cid < 1) {
			bl = false;
			super.setMessage("分类参数不对。");
		}
		if (title.equals("")) {
			bl = false;
			super.setMessage("文章标题不能为空。");
		}
		String author = getAuthor();
		String url = getUrl();
		String origin = getOrigin();

		if (bl) {
			TypoEntity note = new TypoEntity();
			note.setId(id);
			note.setCategoryId(cid);
			note.setTitle(title);
			note.setContent(content);
			note.setAuthor(author);
			note.setOrigin(origin);
			note.setOriginUrl(url);
			note.setUsrId(super.getLoginId());

			// 更新
			if (id > 0) {
				int ret = this.typoService.upt(note);
				if (ret > 0) {
					super.setMessage("1");
					return "typo-save-success";
				} else {
					super.setMessage("保存失败 !");
				}
			} else {
				if (super.getLoginId() == 0) {
					super.setMessage("无权访问!");
				} else {
					// 自动生成Id
					note.setId(super.getAutoId());
					super.setId("" + note.getId());
					int ret = this.typoService.add(note);
					if (ret > 0) {
						super.setMessage("1");
						return "save-success";
					}
					super.setMessage("保存失败 !");
				}
			}
		}
		return "add";
	}

	/**
	 * 文章的列表，分页显示
	 * 
	 * page:当前页
	 * 
	 * size:页数
	 * 
	 * @return
	 */
	public String search() {
		String sId = this.getId();
		int cid = Helper.parseInt(sId);
		setId("" + cid);

		List<CategoryEntity> categoryList = this.typoService.categoryList();
		setCategoryList(categoryList);

		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.typoService.searchCnt(cid);
		super.setAttr("record", cnt);
		if (cnt > 0) {
			List<TypoEntity> noteList = this.typoService
					.search(cid, page, size);
			setList(noteList);
		}
		return "search";
	}

	/**
	 * 详情
	 * 
	 * @return
	 */
	public String info() {
		String sId = this.getId();
		int cid = Helper.parseInt(sId);
		setId("" + cid);

		TypoEntity note = this.typoService.info(cid);
		setNoteInfo(note);
		return "info";
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getTitle() {
		if (title == null) {
			title = "";
		}
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		if (content == null) {
			content = "";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		if (author == null) {
			author = "";
		}
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getOrigin() {
		if (origin == null) {
			origin = "";
		}
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getUrl() {
		if (url == null) {
			url = "";
		}
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<TypoEntity> getList() {
		return list;
	}

	public void setList(List<TypoEntity> list) {
		this.list = list;
	}

	public List<CategoryEntity> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CategoryEntity> categoryList) {
		this.categoryList = categoryList;
	}

	public TypoEntity getNoteInfo() {
		return noteInfo;
	}

	public void setNoteInfo(TypoEntity noteInfo) {
		this.noteInfo = noteInfo;
	}

	public TypoImpl getTypoService() {
		return typoService;
	}

	public void setTypoService(TypoImpl typoService) {
		this.typoService = typoService;
	}
}