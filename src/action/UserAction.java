package action;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import image.Imager;
import impl.JobImpl;
import impl.MessageImpl;
import impl.NewsImpl;
import impl.PubImpl;
import impl.QAImpl;
import impl.UserImpl;
import comm.Constant;
import comm.Encrypt;
import comm.Helper;
import comm.RegexHelper;
import comm.Regexr;
import config.Cn;
import entity.JobEntity;
import entity.JsonEntity;
import entity.JsonListEntity;
import entity.MessageEntity;
import entity.NewsEntity;
import entity.QaEntity;
import entity.UserEntity;
import entity.ValidateEntity;

/**
 * User类
 */
public class UserAction extends Struts2Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserImpl userService;

	private String ucode = "";
	private String uname = "";
	private String uname2 = "";
	private String upwd = "";
	private String upwd2 = "";
	private String npwd = "";
	private String vcode = "";
	private String usex = "";
	private String ud = "";// degree
	private String us = "";// signature
	private String ul = "";// location
	private String imgid = "";
	private String x = "";
	private String y = "";
	private String x2 = "";
	private String y2 = "";
	private String skill = ""; // < 专业技能
	private String subtId = ""; // /< 学科分类ID
	private String urole = ""; // /< 用户角色  0：个人注册；  1：课题组注册
	private String degree = "0"; // /< 学历
	private String title = "0"; // /< 职称
	private String deptId = "0"; // /< 学员ID


	private List<UserEntity> list = null;
	private UserEntity userInfo = null;
	private List<String> skillList = new ArrayList<String>(); //技能特长标签集合

	private NewsImpl newsService;
	private List<NewsEntity> newlist = null;
	private List<QaEntity> qalist = null;
	private QAImpl qaService;
	
	private List<UserEntity> subjectGroupList = null; //课题组
	private List<UserEntity> hierophantList = null;
	private List<JobEntity> jobList = null;
	private PubImpl pubService = null;
	private JobImpl jobService = null;
	
	private UserEntity headMan = null; //课题组组长
	private List<UserEntity> teacherList = null; //合作老师
	private List<UserEntity> studentList = null; //合作学生
	
	private String univName = ""; //大学名称
	
	private MessageImpl messageService;
	
	private int memberLevel = 0; //课题组身份
	/**
	 * 首页
	 * 
	 * @return
	 */
	public String index() {
		UserEntity entity = super.getSession();
		if (entity == null) {
			return this.goLogin();
		} else {
			return "login-success";
		}
	}
	
	public String goLogin(){
		//优先在登陆页面查处课题组= 78346482，如果没有，则随便查出一个课题组，显示在登陆页面
		List<UserEntity> showList = this.userService.search(0, "", 1, 12, host, 1, "雷霆");
		if(showList != null && showList.size() > 0){
			this.userInfo = showList.get(0);
		}else{
			showList = this.userService.search(0, "", 1, 1, host, 1, null);
			if(showList != null && showList.size() > 0){
				this.userInfo = showList.get(0);
			}
		}
		
		this.subjectGroupList = this.userService.search(0, "", 1, 6, host, 1, "");
		return "login";
	}

	/**
	 * 验证 验证验证码
	 * 
	 * @return
	 */
	public String ckVcode() {
		String pCode = ServletActionContext.getRequest().getParameter("param");
		String vcode = (String) ActionContext.getContext().getSession()
				.get("vCode");
		ValidateEntity json = new ValidateEntity();
		json.setStatus("n");
		json.setInfo(" ");
		if (pCode.equals(vcode)) {
			json.setStatus("y");
			json.setInfo("");
		}
		super.json(json);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String reg() {
		return "reg";
	}

	/**
	 * 
	 * @return
	 */
	public String ckName() {
		String usrCode = ServletActionContext.getRequest()
				.getParameter("param");
		boolean state = this.userService.exist(usrCode);
		ValidateEntity json = new ValidateEntity();
		json.setStatus("n");
		json.setInfo("已注册!");
		if (state) {
			json.setStatus("y");
			json.setInfo("");
		}
		super.json(json);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String next() {
		try {
			String ret = checkReg();

			if (ret.length() > 0) {
				return ret;
			}

			String pwd = super.md5(getUpwd());
			int roleId = Helper.parseInt(urole);
			roleId = roleId > 0 ? roleId : 0;

			int sex = -1;
			int degreeId = Helper.parseInt(degree);
			int titleId = Helper.parseInt(title);

			int loginId = super.getAutoId();

			UserEntity entity = new UserEntity();
			entity.setLastIp(super.getIP());
			entity.setSex(sex);
			entity.setUsrCode(getUcode());
			entity.setId(loginId);
			entity.setUsrName(getUname());
			entity.setUsrPwd(pwd);
			entity.setUsrRole(roleId);
			entity.setDegree(degreeId);
			entity.setTitle(titleId);			

			int state = userService.add(entity);

			if (state == -1) {
				super.setMessage("你注册的用户名已存在，请重新换个注册名称。");
				return "reg";

			} else if (state == 1) {
				try {
					uptSession(loginId);
					// super.sendMail(getUcode());
					// String lname22 = "";
					// int i = getUcode().indexOf("@");
					// if (i > 0) {
					// lname22 = getUcode().substring(i + 1);
					// // 隐藏一部分字符
					// String n = getUcode().substring(0, i);
					// n = Helper.goneString(n);
					// setUcode(n + "@" + lname22);
					// }
					//
					// this.setUname2("http://mail." + lname22);
					return "subt-setting";
				} catch (Exception e) {
					logger.error(e);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		// System.out.println(">>>>>>>>>【99】");
		super.setMessage("注册失败，请重新输入。");
		return "reg";
	}

	/**
	 * 
	 * @return
	 */
	public String regsuccess() {
		return "reg-success2";
	}

	/**
	 * 
	 * @return
	 */
	public String subject() {
		String iSubtId = Helper.trim(this.getSubtId());
		int loginId = super.getLoginId();
		this.userService.uptSubt(iSubtId, loginId);
		UserEntity entity = this.userService.detailInfo(super.getLoginId());
		return "univ-setting";
	}

	/**
	 * 
	 * @return
	 */
	public String univ() {
		int universityId = Helper.parseInt(this.getDeptId());
				int loginId = super.getLoginId();
		this.userService.uptDept(universityId, loginId);
		UserEntity entity = this.userService.detailInfo(super.getLoginId());
		return "study-setting";
	}

	
	/**
	 * 
	 * @return
	 */
	public String study() {
		int loginId = super.getLoginId();
		UserEntity entity = new UserEntity();
		entity.setId(loginId);
		entity.setLoginId(loginId);
		if(this.getSkill() != null && !"".equals(this.getSkill())){
			entity.setSkill(this.getSkill().replaceAll(",", "，"));
			if(entity.getSkill().lastIndexOf("，") == entity.getSkill().length() -1){
				entity.setSkill(entity.getSkill().substring(0, entity.getSkill().length() -1));
			}
		}
		this.userService.uptDetailInfo(entity);
		

		if(this.getSkill() != null){
			String[] arr = this.getSkill().split("，");
			skillList.clear();
			Collections.addAll(skillList, arr);
			
			Map<String, Object> session = ActionContext.getContext().getSession();
			if(session == null){
				return null;
			}
			session.put("skillList", skillList);
		}
		return "login-success";
	}

	/**
	 * 登录
	 * 
	 */
	public String login() {
		try {
			// 模拟登陆
			if ("111@qq.com".equals(getUcode())) {
				Map<String, Object> session = ActionContext.getContext()
						.getSession();
				if (!session.containsKey("user_info")) {
					uptSession(14609746);
					return "login-success";
				}
			}

			String ret = checkLogin();
			if (ret.length() > 0) {
				return ret;
			}

			String uPwd = getUpwd();
			String uCode = getUcode();

			ret = login2(uCode, uPwd);

			if (ret.length() > 0) {
				return ret;
			}

		} catch (Exception e) {
			logger.error(e);
		}
		return this.goLogin();
	}

	/**
	 * 注销用户
	 * 
	 * @return
	 */
	public String logout() {
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> session = actionContext.getSession();
		if (session.containsKey("user_info")) {
			session.remove("user_info");
		}
		return this.goLogin();
	}

	/**
	 * 
	 * @return
	 */
	public String me() {
//		if(getSession() == null){
			this.userInfo = this.userService.info(super.getLoginId(), host);
//		}else{
//			this.userInfo = getSession();
//		}
		if(this.userInfo != null && this.userInfo.getSkill() != null){
			skillList.clear();
			String[] arr = this.userInfo.getSkill().split("，");
			Collections.addAll(skillList, arr);
		}
		//把学科拼凑成字符串
		if(userInfo.getSubtId() != null && !"".equals(userInfo.getSubtId())){
			String subjName = pubService.subjecNameList(userInfo.getSubtId());
			userInfo.setSubString(subjName);
		}
		setUserInfo(this.userInfo);
		return "me";
	}

	/**
	 * 更新个人资料
	 * 
	 * @return
	 */
	public String mesave() {
		int loginId = super.getLoginId();
		userInfo.setId(loginId);
		userInfo.setLoginId(loginId);
		userInfo.setDeptId(Helper.parseInt(deptId));
		int ret = this.userService.upt(userInfo);
		if(userInfo.getSkill() != null && !"".equals(userInfo.getSkill())){
			userInfo.setSkill(userInfo.getSkill().replaceAll(",", "，"));
			if(userInfo.getSkill().lastIndexOf("，") == userInfo.getSkill().length() -1){
				userInfo.setSkill(userInfo.getSkill().substring(0, userInfo.getSkill().length() -1));
			}
		}
		this.userService.uptDetailInfo(userInfo);
		if (ret > 0) {
			super.setMessage("1");
			//修改信息后更新session
			Map<String, Object> session = ActionContext.getContext().getSession();
			session.put("user_info", userInfo);
			
			//更新session中用户专长
			skillList.clear();
			String[] arr = this.userInfo.getSkill().split("，");
			Collections.addAll(skillList, arr);
			session.put("skillList", skillList);
			return "me-save-success";
		} else {
			super.setMessage("更新失败。");
		}
		return "me";
	}

	/**
	 * 
	 * @return
	 */
	public String more() {
		UserEntity entity = this.userService.detailInfo(super.getLoginId());
		this.userInfo = entity;
		return "more";
	}

	/**
	 * 更新个人资料
	 * 
	 * @return
	 */
	public String moresave() {
		// String name = trim(this.uname);
		// if (Regexr.usrName(name)) {
		int loginId = super.getLoginId();
		UserEntity entity = new UserEntity();
		entity.setId(loginId);
		entity.setLoginId(loginId);
		String s = this.getSkill();
		entity.setSkill(s);
		int ret = this.userService.uptDetailInfo(entity);
		if (ret > 0) {
			super.setMessage("1");
			return "more-save-success";
		} else {
			super.setMessage("更新失败。");
		}
		return "more";
	}

	/**
	 * 更改密码
	 * 
	 * @return
	 */
	public String pwd() {
		return "pwd";
	}

	/**
	 * 更改密码-保存
	 * 
	 * @return
	 */
	public String pwdsave() {
		//
		// String name = trim(this.uname);
		// if (Regexr.usrName(name)) {
		//
		int loginId = super.getLoginId();
		String pwd = trim(this.upwd);
		String npwd2 = trim(this.npwd);
		if (Regexr.pwd(npwd2)) {
			pwd = super.md5(pwd);
			npwd2 = super.md5(npwd2);
			int ret = this.userService.chgPwd(loginId, pwd, npwd2);
			if (ret > 0) {
				super.setMessage("1");
				return "pwd-save-success";
			} else if (ret == -1) {
				super.setMessage("旧密码输入错误.");
			} else {
				super.setMessage("修改密码失败.");
			}
		} else {
			super.setMessage("登录密码只能是6-16数字或字母组合。");
		}
		return "pwd";
	}

	/**
	 * 头像
	 * 
	 * @return
	 */
	public String hp() {
		return "hd";
	}

	/**
	 * 更新个人资料
	 * 
	 * @return
	 */
	public String hpsave() {
		int loginId = super.getLoginId();
		int imgId = Helper.parseInt(imgid);
		int fx = Helper.parseFloat2(x);
		int fy = Helper.parseFloat2(y);
		int fx2 = Helper.parseFloat2(x2);
		int fy2 = Helper.parseFloat2(y2);
		if (imgId > 0 && fx > -1 && fy > -1 && fx2 > -1 && fy2 > -1) {
			int ret = new Imager().cut(imgId, "", path, fx, fy, fx2);
			if (ret > 0) {
				this.userService.uptHd(loginId, imgId);
				uptSession();
				return "hd-save-success";
			}
		}
		super.setMessage("保存失败。");
		return "hd";
	}

	/**
	 * 指定用户主页
	 * 
	 * @return
	 */
	public String home() {
		// /TODO
		int usrId = getUserId();
		this.userInfo = this.userService.info(usrId, host);

		return "home";
	}
	
	/**
	 * 主页
	 * 
	 * @returns
	 */
	public String main() {
		// /TODO
		int usrId = getUserId();
		if(getSession() == null){
			this.userInfo = this.userService.info(usrId, host);
		}else{
			this.userInfo = getSession();
		}
		if(this.userInfo == null){
			return this.goLogin();
		}
		
		newlist = this.newsService.list(0, 1, 10);
		qalist = this.qaService.list(0, 1, 10, usrId,host);
		for(QaEntity qa : qalist){
			qa.setAnswerCnt(this.qaService.replyListCnt(qa.getId()));
		}
		
		jobList = this.jobService.list(0, 1, 10, 50);
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "main");
		return "main";
	}

	/**
	 * 指定用户的基本+详细信息
	 * 
	 * @return
	 */
	public String info() {
		int usrId = getUserId();
		this.userInfo = this.userService.info(usrId, host);

		return "info";
	}
	
	/**
	 * 指定用户的课题组组长
	 * 
	 * @return
	 */
	public String headMan() {
		int usrId = getUserId();
		UserEntity userEntity = this.userService.info(usrId, host);
		int ktzId = 0;
		//如果用户是课题组，则根据用户id查询课题组内成员信息，如果不是课题组，则根据所属课题组信息查询
		if(userEntity != null){
			if(userEntity.getUsrRole() == 1){
				ktzId = userEntity.getId();
			}else{
				ktzId = userEntity.getKtzId();
			}
			List<UserEntity> headManList = this.userService.searchKtzMember(ktzId, Constant.HEADMAN, host);
			headMan = (headManList != null && headManList.size() >0)? headManList.get(0) : null;
		}
		return "headMan";
	}

	/**
	 * 指定用户的合作导师
	 * 
	 * @return
	 */
	public String teacher() {
		int usrId = getUserId();
		UserEntity userEntity = this.userService.info(usrId, host);
		int ktzId = 0;
		if(userEntity != null){
			if(userEntity.getUsrRole() == 1){
				ktzId = userEntity.getId();
			}else{
				ktzId = userEntity.getKtzId();
			}
			teacherList = this.userService.searchKtzMember(ktzId, Constant.TEACHERMAN, host);
		}
		return "teacher";
	}

	/**
	 * 指定用户的合作学生
	 * 
	 * @return
	 */
	public String student() {
		int usrId = getUserId();
		UserEntity userEntity = this.userService.info(usrId, host);
		int ktzId = 0;
		if(userEntity != null){
			if(userEntity.getUsrRole() == 1){
				ktzId = userEntity.getId();
			}else{
				ktzId = userEntity.getKtzId();
			}
			studentList = this.userService.searchKtzMember(ktzId, Constant.STUDENTMAN, host);
		}
		return "student";
	}

	/**
	 * 指定用户的基本信息-竖版显示
	 * 
	 * @return
	 */
	public String profile() {
		int usrId = getUserId();
		this.userInfo = this.userService.info(usrId, host);
		return "profile";
	}

	/**
	 * 指定用户的基本信息-横版显示
	 * 
	 * @return
	 */
	public String profilel() {
		int usrId = getUserId();
		this.userInfo = this.userService.info(usrId, host);
		return "profile-l";
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public String search() {
		int userId = super.getLoginId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.userService.searchCnt(userId, "", null, null);
		super.setAttr("record", cnt);
		List<UserEntity> usrList = null;
		if (0 < cnt) {
			//查询所有个人注册
			usrList = this.userService.search(userId, "", page, size, host, null, null);
			setList(usrList);
		}
		return "search";
	}

	public String getImgid() {
		return imgid;
	}

	public void setImgid(String imgid) {
		this.imgid = imgid;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public String getX2() {
		return x2;
	}

	public void setX2(String x2) {
		this.x2 = x2;
	}

	public String getY2() {
		return y2;
	}

	public void setY2(String y2) {
		this.y2 = y2;
	}

	/**
	 * LOGIN验证
	 * 
	 * @return
	 */
	private String checkLogin() {
		if (getUcode().equals("")) {
			super.setMessage(Cn.USRLOGINNAMEISEMPTY);
			return this.goLogin();
		}
		if (getUpwd().equals("")) {
			super.setMessage(Cn.USRLOGINPWDISEMPTY);
			return this.goLogin();
		}
//		if (getVcode().equals("")) {
//			super.setMessage(Cn.USRLOGINVCODEISEMPTY);
//			return "login";
//		}
//		String vcode = (String) ActionContext.getContext().getSession()
//				.get("vCode");
//		if (!getVcode().equals(vcode)) {
//			super.setMessage(Cn.USRLOGINVCODEISERROR);
//			return "login";
//		}
		return "";
	}

	/**
	 * 登录
	 * 
	 * @param uName
	 * @param uPwd
	 * @param auto
	 * @return
	 */
	/**
	 * @param uName
	 * @param uPwd
	 * @return
	 */
	private String login2(String uName, String uPwd) {
		String pwd = md5(uPwd);
		int loginId = userService.login(uName, pwd);
		if (loginId > 0) {
			super.setMessage(Cn.USERLOGINSUCCESS);
			// UserEntity userEntity = userService.info(state, host);
			// ActionContext actionContext = ActionContext.getContext();
			// Map<String, Object> session = actionContext.getSession();
			// session.put("user_info", userEntity);
			// setSession(arg0);
			UserEntity userEntity = uptSession(loginId);
			userService.uptLastDate(loginId, super.getIP());
			if (null != userEntity) {
				//保存未读消息到session中
				Map<String, Object> session = ActionContext.getContext().getSession();
				UserEntity u = (UserEntity) session.get("user_info");
				int newMsgNum = this.messageService.unCnt(loginId);
				u.setNewMsgNum(newMsgNum);
				session.put("user_info", u);
				
				if (Helper.isEmpty(userEntity.getSubtId())) {
					return "subt-setting";
				}
				return "login-success";
			} else {
				return this.goLogin();
			}

		} else {
			// >1 登录成功; -1 用户不存在; -2 用户无效或冻结; -3 用户密码错误; 0 登录失败;
			if (loginId == -1) {
				super.setMessage(Cn.USERLOGINCODENOTEXITS);
			} else if (loginId == -2) {
				super.setMessage(Cn.USERLOGINCODEFREEZE);
				// return "login";
			} else if (loginId == -3) {
				super.setMessage(Cn.USERLOGINCODECHECKFAIL);
				// return "login";
			} else {
				super.setMessage(Cn.USERLOGINFAILED);
			}
			return this.goLogin();
		}
	}

	/**
	 * 
	 * @return
	 */
	private String checkReg() {
		String forName = "reg";
		if (this.getUcode().equals("")) {
			super.setMessage("请输入注册邮箱。");
			return forName;
		}
		if (!RegexHelper.email(this.getUcode())) {
			// TODO 邮箱、手机号码验证
			super.setMessage("请输入正确格式邮箱。");
			return forName;
		}
		if (this.getUpwd().equals("")) {
			super.setMessage("请输入密码。");
			return forName;
		}
		if (this.getUpwd2().equals("")) {
			super.setMessage("请输入确认密码。");
			return forName;
		}
		if (!RegexHelper.pwd(this.getUpwd())) {
			// TODO 密码格式的验证
			super.setMessage("输入密码格式有误。");
			return forName;
		}
		if (!this.getUpwd2().equals(this.getUpwd())) {
			super.setMessage("两次输入的密码不一致。请重新确认。");
			return forName;
		}
		if (this.getUname().equals("")) {
			super.setMessage("请输入真实姓名。");
			return forName;
		}
		if (!RegexHelper.usrName(this.getUname())) {
			// TODO 真实姓名验证
			super.setMessage("请输入真实姓名。");
			return forName;
		}
//		if (this.getVcode().equals("")) {
//			super.setMessage("请输入验证码。");
//			return forName;
//		}
//		String vcode = (String) ActionContext.getContext().getSession()
//				.get("vCode");
//		if (!this.getVcode().equals(vcode)) {
//			super.setMessage("验证码输入错误，请重新输入。");
//			return forName;
//		}
		return "";
	}
	
	public String subjectGroupTop(){
		int usrId = getUserId();
		//查询所有课题组
		this.subjectGroupList = this.userService.search(usrId, "", 1, 12, host, 1, null);
		//查询所有导师
		this.hierophantList = this.userService.search(usrId, "", 1, 6, host, 0, null);
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "subjectGroup");
		return "subjectGroupTop";
	}
	
	//根据条件查询课题组
	public String subjectList(){
		int usrId = getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		size = 15;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.userService.searchCnt(usrId, "", 1, uname);
		super.setAttr("record", cnt);
		super.setAttr("uname", uname);
		//查询所有课题组
		this.subjectGroupList = this.userService.search(usrId, "", page, size, host, 1, uname);
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "subjectGroup");
		return "subjectList";
	}
	
	//根据条件查询用户
	public String userList(){
		int usrId = getUserId();
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		size = 15;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.userService.searchCnt(usrId, "", 0, uname);
		super.setAttr("record", cnt);
		super.setAttr("uname", uname);
		//查询所有用户
		this.hierophantList = this.userService.search(usrId, "", page, size, host, 0, uname);
		
		//设置当前导航显示哪个页面
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("showMenu", "subjectGroup");
		return "userList";
	}
	
	/**
	 * 课题组管理页面
	 * @return
	 */
	public String ktzManage() {
		if(getSession() == null){
			this.userInfo = this.userService.info(super.getLoginId(), host);
		}else{
			this.userInfo = getSession();
		}
		
		//如果角色不是课题组，则返回到编辑页面
		if(this.userInfo.getUsrRole() != 1){
			return "me";
		}
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		size = 10;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.userService.ktzListCnt(this.userInfo.getId(), uname, univName);
		super.setAttr("record", cnt);
		this.list = this.userService.ktzList(this.userInfo.getId(), page, size, uname, univName);
		if(this.list != null && this.list.size() > 0){
			//在查询结果里查询出课题组长
			for(UserEntity user : list){
				if(Constant.HEADMAN == user.getHeadManFlag()){
					this.headMan = user;
					break;
				}
			}
		}
		return "ktzManage";
	}
	
	/**
	 * 课题组成员展示页面
	 * @return
	 */
	public String ktzList() {
		//课题组ID
		String sId = this.getId();
		int ktzId = Helper.parseInt(sId);
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		size = 10;
		super.setAttr("page", page);
		super.setAttr("size", size);
		int cnt = this.userService.ktzListCnt(ktzId, uname, univName);
		super.setAttr("record", cnt);
		super.setAttr("id", ktzId);
		this.list = this.userService.ktzList(ktzId, page, size, uname, univName);
		return "ktzList";
	}
	
	/**
	 * 查询可以添加的课题组成员
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String member() throws UnsupportedEncodingException {
		int page = Helper.parseInt(super.getP());
		page = page < 1 ? 1 : page;
		List<UserEntity> menberList = this.userService.memberList(page, size, uname, univName);
		JsonListEntity json = new JsonListEntity();
		json.setState(0);
		if (menberList != null) {
			int record = this.userService.memberListCnt(uname, univName);
			String html = this.getHtml(page, size, record);
			json.setList(menberList);
			json.setState(1);
			json.setHtml(html);
		}
		super.json(json);
		return null;
	}
	
	//课题组成员分页通过ajax来查找数据
	public String getHtml(int pageIndex, int pageSize, int record) {
		String html = "";
	    int maxPage = 50;
		int showNum = 7;
		int diff = showNum / 2 + 1;

		pageIndex = pageIndex > 1 ? pageIndex : 1;
		pageIndex = pageIndex > maxPage ? maxPage : pageIndex;
		if (pageSize > 0 && record > 0) {
			int page = record / pageSize;
			if (record % pageSize > 0) {
				page = page + 1;
			}
			page = page > maxPage ? maxPage : page;

			if (page > 1) {
				html = html + "<div class='page'><ul>";
				// prePAGE
				if (pageIndex > 1) {
					html = html + link(pageIndex - 1, "上一页");
				}
				if (pageIndex == 1) {
					html = html + current(pageIndex);
				} else {
					html = html + link(1, "1");
				}
				if (pageIndex > diff) {
					html = html + more();
				}
				int min = pageIndex - (diff - 2);
				while (min < 2) {
					min++;
				}
				while ((page - min) < diff + 1) {
					min--;
				}
				int max = min + (showNum - 2);
				for (int i = min; i < max; i++) {
					if (i > 1) {
						if (i == pageIndex) {
							html = html + current(i);
						} else {
							html = html + link(i, "" + i);
						}
					}
				}

				if ((page + 1 - pageIndex) > diff) {
					html = html + "<li>...</li>";
				}
				//if (page > showNum) {
					if (page == pageIndex) {
						html = html + current(page);
					} else {
						html = html + link(page, "" + page);
					}
				//}
				if (pageIndex < page) {
					html = html + link(pageIndex + 1, "下一页");
				}
				html = html + "</ul></div>";
			}
		}
		return html;
	}
	
	private String link(int page, String linkName) {
		return "<li><a href='javascript:memberList(" + page + ");'>" + linkName
				+ "</a></li>";
	}

	/**
	 * 
	 * @param page
	 * @return
	 */
	private String current(int page) {
		return "<li class='current'><a href='javascript:void(0);'>" + page
				+ "</a></li>";
	}
	
	/**
	 * 加入课题组
	 * @return
	 */
	public String agreeAddKtz() {
		JsonEntity json = new JsonEntity();
		json.setStatus(0);

		int msgId = Helper.parseInt(super.getId());
		int loginId = super.getLoginId();
		
		//查询消息的具体信息
		MessageEntity msgInfo = messageService.info(msgId);
		
		//加入新课题组之前查询下该用户的课题组id是否有值，有则不能加入新课题组
		UserEntity user = this.userService.info(loginId, host);
		if(user.getKtzId() == null || user.getKtzId() == 0){
			//设置该用户的ktzId,默认课题组身份为学生
			int ret = this.userService.uptUserKtz(loginId, msgInfo.getSenderId(), Constant.STUDENTMAN);
			if (ret > 0) {
				json.setStatus(ret);
				messageService.uptmMsgCategory(msgInfo.getId(), loginId, Constant.MESSAGE_AGREE);
			}
		}else if(user.getKtzId() == msgInfo.getSenderId()){
			json.setStatus(99);
		}else{
			json.setStatus(0);
			json.setInfo("你已加入【" + user.getKtzName() + "】课题组，如想更换课题组，请先退出课题组【" + user.getKtzName() + "】;");
		}
		super.json(json);
		return null;
	}
	
	public String delKtzMember(){
		JsonEntity json = new JsonEntity();
		
		int memberId = Helper.parseInt(super.getId());
		
		//把该用户的ktzId清空
		this.userService.uptUserKtz(memberId, null, null);
		
		//清空该用户的课题组组长标识
		this.userService.uptUserKtzHeadman(memberId, null);
		
		json.setStatus(1);
		super.json(json);
		return null;
	}
	
	/**
	 * 设置课题组人员身份
	 * @return
	 */
	public String setKtzMemberLevel(){
		JsonEntity json = new JsonEntity();
		json.setStatus(0);
		//要操作的课题组成员id
		int memberId = Helper.parseInt(super.getId());
		
		//课题组id，实际上就是当前课题组登陆人id
		int ktzId = super.getLoginId();
		//查询课题组组长
		if(memberLevel == Constant.HEADMAN){
			List<UserEntity> headManList = this.userService.searchKtzMember(ktzId, Constant.HEADMAN, host);
			UserEntity user = (headManList != null && headManList.size() > 0)? headManList.get(0) : null;
			if(user != null){
				json.setInfo("课题组组长是【" + user.getUsrName() + "】,如想更换组长，请先去除【" + user.getUsrName() + "】的组长职务");
			}else{
				this.userService.uptUserKtzHeadman(memberId, memberLevel);
				json.setStatus(1);
			}
		}else{
			this.userService.uptUserKtzHeadman(memberId, memberLevel);
			json.setStatus(1);
		}
		
		super.json(json);
		return null;
	}
	
	public String skillShow(){
		int usrId = getUserId();
		skillList = this.userService.skillList(usrId);
		return "skillShow";
	}

	public UserImpl getUserService() {
		return userService;
	}

	public void setUserService(UserImpl userService) {
		this.userService = userService;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpwd() {
		return upwd;
	}

	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}

	public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode;
	}

	public String getUsex() {
		return usex;
	}

	public void setUsex(String usex) {
		this.usex = usex;
	}

	public String getNpwd() {
		return npwd;
	}

	public void setNpwd(String npwd) {
		this.npwd = npwd;
	}

	public String getUd() {
		return ud;
	}

	public void setUd(String ud) {
		this.ud = ud;
	}

	public String getUs() {
		return us;
	}

	public void setUs(String us) {
		this.us = us;
	}

	public String getUl() {
		return ul;
	}

	public void setUl(String ul) {
		this.ul = ul;
	}

	public String getUpwd2() {
		return upwd2;
	}

	public void setUpwd2(String upwd2) {
		this.upwd2 = upwd2;
	}

	public String getUcode() {
		return ucode;
	}

	public void setUcode(String ucode) {
		this.ucode = ucode;
	}

	public String getUname2() {
		return uname2;
	}

	public void setUname2(String uname2) {
		this.uname2 = uname2;
	}

	public UserEntity getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserEntity userInfo) {
		this.userInfo = userInfo;
	}

	public List<UserEntity> getList() {
		return list;
	}

	public void setList(List<UserEntity> list) {
		this.list = list;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getSubtId() {
		return subtId;
	}

	public void setSubtId(String subtId) {
		this.subtId = subtId;
	}

	public String getUrole() {
		return urole;
	}

	public void setUrole(String urole) {
		this.urole = urole;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public List<String> getSkillList() {
		return skillList;
	}

	public void setSkillList(List<String> skillList) {
		this.skillList = skillList;
	}

	public NewsImpl getNewsService() {
		return newsService;
	}

	public void setNewsService(NewsImpl newsService) {
		this.newsService = newsService;
	}

	public List<NewsEntity> getNewlist() {
		return newlist;
	}

	public void setNewlist(List<NewsEntity> newlist) {
		this.newlist = newlist;
	}

	public List<QaEntity> getQalist() {
		return qalist;
	}

	public void setQalist(List<QaEntity> qalist) {
		this.qalist = qalist;
	}

	public QAImpl getQaService() {
		return qaService;
	}

	public void setQaService(QAImpl qaService) {
		this.qaService = qaService;
	}

	public List<UserEntity> getSubjectGroupList() {
		return subjectGroupList;
	}

	public void setSubjectGroupList(List<UserEntity> subjectGroupList) {
		this.subjectGroupList = subjectGroupList;
	}

	public List<JobEntity> getJobList() {
		return jobList;
	}

	public void setJobList(List<JobEntity> jobList) {
		this.jobList = jobList;
	}

	public JobImpl getJobService() {
		return jobService;
	}

	public void setJobService(JobImpl jobService) {
		this.jobService = jobService;
	}

	public List<UserEntity> getHierophantList() {
		return hierophantList;
	}

	public void setHierophantList(List<UserEntity> hierophantList) {
		this.hierophantList = hierophantList;
	}

	public PubImpl getPubService() {
		return pubService;
	}

	public void setPubService(PubImpl pubService) {
		this.pubService = pubService;
	}

	public UserEntity getHeadMan() {
		return headMan;
	}

	public void setHeadMan(UserEntity headMan) {
		this.headMan = headMan;
	}

	public String getUnivName() {
		return univName;
	}

	public void setUnivName(String univName) {
		this.univName = univName;
	}

	public MessageImpl getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageImpl messageService) {
		this.messageService = messageService;
	}

	public int getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(int memberLevel) {
		this.memberLevel = memberLevel;
	}

	public List<UserEntity> getTeacherList() {
		return teacherList;
	}

	public void setTeacherList(List<UserEntity> teacherList) {
		this.teacherList = teacherList;
	}

	public List<UserEntity> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<UserEntity> studentList) {
		this.studentList = studentList;
	}

}