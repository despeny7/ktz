package comm;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

public class CnLang {

	private static HttpServletRequest request = ServletActionContext.getRequest();
	public static String TITLE = "课题组网,展示科研工作者的风采与成就";
	public static String KEYWORDS = "课题组网,课题组,scholargroup,科研工作者,研究成果,分享科研";
	public static String DESC = "课题组网，scholargroup.net，展示你和你的团队科研成就，发布你的团队最新活动，发表你及你团队最新成果，了解同研究方向其它人的最新研究成果，分享科研工作者最新论文，交流讨论帮助回答科研中遇到的问题，寻找最适合的科研合作伙伴。";
	public static String BASEPATH = getHost();
	private static String getHost() {
		String host = ConfigHelper.getValue("web_host");
		if (Helper.isEmpty(host) || "localhost".equals(host)) {
			host = request.getScheme() + "://" + request.getServerName()
					+ (request.getServerPort() == 80 ? "" : ":" + request.getServerPort()) + request.getContextPath()
					+ "/";
		}
		return host;
	}
}
