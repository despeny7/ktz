package comm;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigHelper {

	static Logger logger = Logger.getLogger(ConfigHelper.class);

	private static Map<String, String> configMap = null;
	static {
		getConfig();
	}

	/**
	 * get value
	 * 
	 * @param key
	 * @return
	 */
	public static String getValue(String key) {
		try {
			if (configMap != null) {
				return configMap.get(key);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return "";
	}

	/**
	 * 
	 */
	private static void getConfig() {
		String path = ConfigHelper.class.getClassLoader().getResource("")
				.getPath().replace("%20", " ")
				+ "/config.properties";
		if (configMap == null) {
			configMap = new HashMap<String, String>();
		}
		try {
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fis);
			Enumeration<?> enumeration = properties.propertyNames();
			while (enumeration.hasMoreElements()) {
				String key = (String) enumeration.nextElement();
				String value = properties.getProperty(key);
				//System.out.println(key+">>>"+value);
				configMap.put(key, value);
			}

		} catch (Exception e) {
			logger.error(e);
		}
	}
}
