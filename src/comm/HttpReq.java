package comm;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class HttpReq {

	static Logger logger = Logger.getLogger(HttpReq.class);
	
	static Charset defaultCharset=Charset.forName("utf-8");

	/*
	 * Get
	 */
	public static String Get(String url) {
		return Request(EMethod.GET, url, "", defaultCharset);
	}

	/*
	 * Get
	 */
	public static String Get(String url, Charset charset) {
		return Request(EMethod.GET, url, "", charset);
	}

	/*
	 * Post
	 */
	public static String Post(String url, String data) {
		return Request(EMethod.POST, url, data, defaultCharset);
	}

	/*
	 * Post
	 */
	public static String Post(String url, String data, Charset charset) {
		return Request(EMethod.POST, url, data, charset);
	}

	/*
	 * Request
	 */
	public static String Request(EMethod method, String urlString, String data,
			Charset charset) {

		Map<String, String> propertys = new HashMap<String, String>();
		propertys
				.put("User-Agent",
						"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; InfoPath.2)");
		//propertys.put("Accept-Encoding", "gzip");
		propertys.put("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,*/*");
		propertys.put("Connection", "keep-alive");
		propertys.put("Content-Type", "text/html;charset=utf-8");
		propertys.put("Accept-Language", "zh-CN,zh");
		//propertys.put("Accept-Charset", "GBK,utf-8;q=0.7,*;q=0.3");

		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(urlString);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod(method.toString());
			if (propertys != null) {
				for (String key : propertys.keySet()) {
					urlConnection.addRequestProperty(key, propertys.get(key));
				}
			}
			if (EMethod.POST.equals(method)) {
				if (!"".equals(data)) {
					byte[] bytes = data.getBytes();
					urlConnection.setFixedLengthStreamingMode(bytes.length);
					urlConnection.getOutputStream().write(bytes);
					urlConnection.getOutputStream().flush();
					urlConnection.getOutputStream().close();
				}
			}
			String htmlString = "";
			
			InputStream inputStream = urlConnection.getInputStream();
			InputStreamReader iReader = new InputStreamReader(inputStream,charset);
			BufferedReader bReader = new BufferedReader(iReader);
			
			String s = null;

			while ((s = bReader.readLine()) != null) {
				//logger.info(s);
				htmlString+=s;
				//System.out.println(s);
			}
			bReader.mark(0);
			bReader.reset();
			bReader.close();

			//logger.info(htmlString);
			return htmlString;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}

		return "";
	}

	// private static HttpRespons makeContent(String urlString,
	// HttpURLConnection urlConnection) throws IOException {
	//
	// }
}
