package comm;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.List;

/**
 * 生成json格式字符串类
 * 
 * @author desmond 2013-11-15
 */
public class Jsoner {

	/**
	 * 根据List生成JSON格式字符串
	 * @param cls
	 * @param list
	 * @return
	 * @throws Exception
	 */
	private static String getJsonStringByList(Object cls, List<?> list) throws Exception {

		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (null != list && list.size() > 0) {
			for (Object object : list) {
				sb.append(getJsonString(object) + ",");
			}

			sb.setLength(sb.length() - 1);
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 根据Entity生成JSON格式字符串
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	public static String getJsonString(Object cls)
			throws Exception {
		Field[] fields = cls.getClass().getDeclaredFields();
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		AccessibleObject.setAccessible(fields, true);

		boolean bl = false;

		for (Field field2 : fields) {
			String key = field2.getName();
				if (!bl) {
					bl = true;
				}
				sb.append("\"" + key + "\":");// key.toLowerCase()
				if (field2.getType() == List.class) {
					List<?> list = (List<?>) field2.get(cls);
					String value = getJsonStringByList(cls, list);
					sb.append("" + value + ",");
				} else {
					Object value = field2.get(cls);
						sb.append("\"" + value + "\",");
				}
		}
		if (bl) {
			sb.setLength(sb.length() - 1);
		}
		sb.append("}");
		return sb.toString();
	}
}
