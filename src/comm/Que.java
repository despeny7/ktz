package comm;

import java.util.LinkedList;
import java.util.Queue;

public class Que<T> {

	private Queue<T> queue = null;

	/*
	 * 
	 */
	public synchronized void Add(T item) {
		if (null == queue) {
			queue = new LinkedList<T>();
		}
		queue.offer(item);
	}

	/*
	 * 
	 */
	public synchronized T Get() {
		if (null != queue) {
			T item = this.queue.poll();
			return item;
		}
		return null;
	}

	/*
	 * 
	 */
	public synchronized int getCount() {
		if (null != queue) {
			if (!this.queue.isEmpty()) {
				return this.queue.size();
			}
		}
		return 0;
	}

}
