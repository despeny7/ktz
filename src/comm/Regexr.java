package comm;

import java.util.regex.Pattern;

/**
 * 正则类
 * 
 * @author desmond 2013-11-15
 */
public class Regexr {

	/**
	 * 邮箱
	 * @param input
	 * @return
	 */
	public static boolean email(String input){
		String regex="^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		return matcher(regex,input);
	}
	
	/**
	 * 真实姓名
	 * @param input
	 * @return
	 */
	public static boolean usrName(String input){
		String regex="^[\u4e00-\u9fa5]{2,10}|[a-zA-Z]{1}[a-z.A-Z0-9]{1,9}$";
		return matcher(regex,input);
	} 
	
	/**
	 * 手机号码
	 * @param input
	 * @return
	 */
	public static boolean mobile(String input){
		String regex="^13[0-9]{9}$|14[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$";
		return matcher(regex,input);
	}
	
	/**
	 * 电话
	 * @param input
	 * @return
	 */
	public static boolean tel(String input){
		String regex = "^[0-9]{3,4}[-/]{0,1}[0-9]{7,8}$|[0-9]{7,8}$|13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$";
		return matcher(regex,input);
	}
	
	/**
	 * qq
	 * @param input
	 * @return
	 */
	public static boolean qq(String input){
		String regex = "^[1-9]{1}[0-9]{3,10}$";
		return matcher(regex,input);
	}
	
	/**
	 * 密码
	 * @param input
	 * @return
	 */
	public static boolean pwd(String input){
		String regex = "^[\\w\\W]{6,16}$";
		return matcher(regex,input);
	}
	
	/**
	 * 去掉HTML标签
	 * @param html
	 * @return
	 */
	public static String rmHtml(String html)
	{
		String regex = "</?[^>]+>";
		html = html .replaceAll(regex, "");
		return html;
	}
	
	/**
	 * 去掉空格
	 * @param html
	 * @return
	 */
//	public static String rmSpace(String html){
//		return html ;
//	}
	
	/**
	 * 将\r\n 转换成空格
	 * @param html
	 * @return
	 */
	public static String chgBr(String html){
		return html.replace("\r\n", " ").replace("\n", " ");
		
	}
	
	/**
	 * 
	 * @param regex
	 * @param input
	 * @return
	 */
	private static boolean matcher(String regex,String input){
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(input).matches();
		
	}
}
