package comm;

/**
 * 邮件实体类
 * 
 * @author desmond 2013-11-15
 */
public class SimpleMailEntity {

	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		if(formName==null||formName.equals("")){
			formName = formAddr;
		}
		this.formName = formName;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public boolean isSmtpValidate() {
		return smtpValidate;
	}
	public void setSmtpValidate(boolean smtpValidate) {
		this.smtpValidate = smtpValidate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getFormAddr() {
		return formAddr;
	}
	public void setFormAddr(String formAddr) {
		this.formAddr = formAddr;
	}
	/**
	 * smtp host 如：smtp stmp.163.com
	 */
	private String smtpHost = "";
	
	/**
	 * 端口 21
	 */
	private String smtpPort = "";
	private boolean smtpValidate = false;

	/**
	 * 发件者用户名
	 */
	private String userName = "";
	
	/**
	 * 发件者密码
	 */
	private String userPwd = "";
	
	/**
	 * 收件人地址
	 */
	private String formAddr = "";
	
	/**
	 * 收件人的显示名称
	 */
	private String formName = "";
}
