package config;

public class Cn {
public final static  String USRLOGINNAMEISEMPTY="请输入登录名称。";
public final static String USRLOGINPWDISEMPTY="请输入登录密码。";
public final static String USRLOGINVCODEISEMPTY="请输入验证码。";
public final static String USRLOGINVCODEISERROR="验证码错误 !";

public final static String USERLOGINSUCCESS="登录成功!";

public final static String USERLOGINCODENOTEXITS="用户不存在，请重新输入。";
public final static String USERLOGINCODEFREEZE="用户无效或冻结，请重新输入。";
public final static String USERLOGINCODECHECKFAIL="用户密码错误，请重新输入。";
public final static String USERLOGINFAILED="登录失败，请重新输入。";
}
