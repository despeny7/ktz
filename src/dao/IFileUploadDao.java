package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IFileUploadDao {
	
	public int add(AttachmentEntity entity);
	
	public int uptEntityId(Map<String, String> map);
	
	public int del(int id);

	public List<AttachmentEntity> listAttachment(Map<String, String> map);
	
	public AttachmentEntity info(int id);
}
