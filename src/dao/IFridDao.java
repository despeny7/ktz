package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IFridDao {
	
	public int add(FriendEntity entity);

	public int del(FriendEntity entity);

	public List<FriendEntity> list(Map<String, Integer> map);
	
	public int cnt(Map<String, Integer> map);
	
	public List<FriendEntity> top(Map<String, Integer> map);
	
	public int upt(FriendEntity entity);

	public int exist(FriendEntity entity);
}
