package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IMessageDao {
	
	public int add(MessageEntity entity);

	public int del(MessageEntity entity);
	
	public int upt(MessageEntity entity);

	public List<MessageEntity> list(Map<String, Integer> map);
	
	public List<MessageEntity> newMsgList(Map<String, Integer> map);
	
	public MessageEntity info(int id);
	
	public int cnt(Map<String, Integer> map);
	
	public int unCnt(Map<String, Integer> map);
	
	public int uptmMsgCategory(MessageEntity entity);
}
