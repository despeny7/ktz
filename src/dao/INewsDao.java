package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface INewsDao {
	
	public List<NewsEntity> list(Map<String,Integer> map);
	
	public int listCnt(Map<String,Integer> map);
	
	public NewsEntity info(int id);
	
	public int add(NewsEntity note);

	public int upt(NewsEntity note);
	
	public int del(Map<String,Integer> map);
	
}
