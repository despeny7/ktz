package dao;

import java.util.List;

import entity.CategoryEntity;
import entity.DeptEntity;

public interface IPubDao {
	
	public List<CategoryEntity> cityList();

	public List<CategoryEntity> degreeList();
	
	public List<CategoryEntity> titleList();
	
	public List<CategoryEntity> subjectList();
	
	public List<DeptEntity> deptList();
	
	public int uptDept(CategoryEntity entity);
	
	public List<CategoryEntity> subjecNameList(String[] ids);
	
	public List<CategoryEntity> jobTitleList();
}
