package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface IRGDao {
	
	public List<ResearchGroupEntity> list(Map<String,Integer> map);
	
	public int listCnt(Map<String,Integer> map);
	
	public ResearchGroupEntity info(int id);
	
	public int add(ResearchGroupEntity note);

	public int upt(ResearchGroupEntity note);
	
	public int del(Map<String,Integer> map);
	
	public String getId(Map<String,Integer> map);
	
}
