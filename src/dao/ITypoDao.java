package dao;

import java.util.List;
import java.util.Map;

import entity.*;

public interface ITypoDao {
	
	public TypoEntity info(int id);
	
	public int add(TypoEntity note);

	public int upt(TypoEntity note);
	
	public int del(Map<String,Integer> map);
	
	public List<CategoryEntity> categoryList(); 
	
	public List<TypoEntity> search(SearchEntity entity);
	
	public int searchCnt(SearchEntity entity);
}
