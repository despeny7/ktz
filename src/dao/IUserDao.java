package dao;

import java.util.List;
import java.util.Map;

import entity.CategoryEntity;
import entity.SearchEntity;
import entity.UserEntity;

public interface IUserDao {

	public UserEntity login(String usrCode);

	public UserEntity info(int id);

	public int uptLastDate(UserEntity user);

	public List<UserEntity> list(SearchEntity entity);

	public int cnt();

	public int reset(UserEntity entity);

	public int close(UserEntity entity);

	public String getPwd(int id);

	public int add(UserEntity entity);

	public int addDetail(UserEntity entity);

	public int exist(String usrCode);

	public List<CategoryEntity> cityList();

	public List<CategoryEntity> degreeList();

	public int upt(UserEntity entity);

	public int uptHd(UserEntity entity);

	public int uptImg(UserEntity entity);

	public int uptSubt(UserEntity entity);
	
	public int uptDept(UserEntity entity);

	public UserEntity detailInfo(int id);

	public int uptDetailInfo(UserEntity entity);

	public List<UserEntity> search(Map<String, Object> map);

	public int searchCnt(Map<String, Object> map);

	public List<UserEntity> ktzList(Map<String, Object> map);
	
	public int ktzListCnt(Map<String, Object> map);
	
	public List<UserEntity> memberList(Map<String, Object> map);
	
	public int memberListCnt(Map<String, Object> map);
	
	public int uptUserKtz(Map<String, Object> map);
	
	public int uptUserKtzHeadman(Map<String, Object> map);
	
	public List<UserEntity> searchKtzMember(Map<String, Object> map);
}
