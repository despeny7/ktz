package entity;

import java.sql.Blob;



/**
 * 附件，使用实体全名和实体ID关联
 * 
 * @author Jinni 2007-12-26
 *
 */

public class AttachmentEntity
{

	private int id;
	
	//重新命名的附件临时名称
	private String name;
	
	//文件原始名称
	private String fileName;
	
	private String shortFileName;
	
	//关联实体主键，不能使用符合主键
	private String entityId;
	
	//实体名称，使用类全名关联 如：NoteEntity
	private String entityName;

	//创建时间
	private String at = "";
	
	public int getId()
    {
    	return id;
    }
	
	public void setId(int id)
    {
    	this.id = id;
    }
	
	public String getName()
    {
    	return name;
    }
	
	public void setName(String name)
    {
    	this.name = name;
    }

	public String getEntityId()
    {
    	return entityId;
    }

	public void setEntityId(String entityId)
    {
    	this.entityId = entityId;
    }

	public String getEntityName()
    {
    	return entityName;
    }

	public void setEntityName(String entityName)
    {
    	this.entityName = entityName;
    }

	public String getFileName()
    {
    	return fileName;
    }

	public void setFileName(String fileName)
    {
    	this.fileName = fileName;
    }

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getShortFileName() {
		return shortFileName;
	}

	public void setShortFileName(String shortFileName) {
		this.shortFileName = shortFileName;
	}

}
