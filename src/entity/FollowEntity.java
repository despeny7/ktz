package entity;
/**
 * 关注表
 * @author quchao
 *
 */
public class FollowEntity {

	private int id = 0;
	private int userId = 0;  //用户id
	private int objId; //被关注的对象id
	/**
	 * 关注类型：多个
	 * 问题关注：QA；   用户关注；USER
	 */
	private String type;
	private String time; //关注时间
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getObjId() {
		return objId;
	}
	public void setObjId(int objId) {
		this.objId = objId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	} 
	
	
}
