package entity;

public class ImageEntity {
	public int getImgId() {
		return imgId;
	}

	public void setImgId(int imgId) {
		this.imgId = imgId;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public int getFolder() {
		return folder;
	}

	public void setFolder(int folder) {
		this.folder = folder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgExt() {
		return orgExt;
	}

	public void setOrgExt(String orgExt) {
		this.orgExt = orgExt;
	}

	// public int getOrgSize() {
	// return orgSize;
	// }
	// public void setOrgSize(int orgSize) {
	// this.orgSize = orgSize;
	// }

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getOrgLength() {
		return orgLength;
	}

	public void setOrgLength(int orgLength) {
		this.orgLength = orgLength;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSizeStr() {
		return sizeStr;
	}

	public void setSizeStr(String sizeStr) {
		this.sizeStr = sizeStr;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	private int imgId = 0;
	public String getImgUrlO() {
		return imgUrlO;
	}

	public void setImgUrlO(String imgUrlO) {
		this.imgUrlO = imgUrlO;
	}

	public String getImgUrlM() {
		return imgUrlM;
	}

	public void setImgUrlM(String imgUrlM) {
		this.imgUrlM = imgUrlM;
	}

	private int categoryId = 0;
	private int usrId = 0;
	private String usrName = "";
	private String imgName = "";
	private int folder = 0;
	private String remark = "";
	private String data = "";
	private String orgName = "";
	private String orgExt = "";
	// private int orgSize = 0;
	private int orgLength = 0;
	private int size = 0;
	private String sizeStr = "";
	private String at = "";
	private String imgUrlO = "";
	private String imgUrlM = "";
}
