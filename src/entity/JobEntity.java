package entity;

public class JobEntity {
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getVisitCnt() {
		return visitCnt;
	}

	public void setVisitCnt(int visitCnt) {
		this.visitCnt = visitCnt;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}


	public String getJobRequire() {
		return jobRequire;
	}

	public void setJobRequire(String jobRequire) {
		this.jobRequire = jobRequire;
	}


	public int getProfessTitle() {
		return professTitle;
	}

	public void setProfessTitle(int professTitle) {
		this.professTitle = professTitle;
	}


	public String getProfessTitleName() {
		return professTitleName;
	}

	public void setProfessTitleName(String professTitleName) {
		this.professTitleName = professTitleName;
	}


	public int getUsrRole() {
		return usrRole;
	}

	public void setUsrRole(int usrRole) {
		this.usrRole = usrRole;
	}


	private int id = 0;
	private int usrId = 0;
	private String usrName = "";
	private int categoryId = 0;
	private String categoryName = "";
	private String title = "";
	private String shortContent = "";
	private int visitCnt = 0;
	private String at = "";
	private String jobDesc; //工作描述
	private String jobRequire; //工作要求
	private int professTitle; //招聘的职称
	private String professTitleName;
	
	/**
	 * 发布人类型
	 * 0：个人注册；  1：课题组注册
	 */
	private int usrRole = 0; 
}
