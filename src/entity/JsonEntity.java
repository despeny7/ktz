package entity;

public class JsonEntity {
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	private String info = "";
	private int status = 0;
	
	public JsonEntity(){
		this.status=0;
		this.info="失败。";
	}
}
