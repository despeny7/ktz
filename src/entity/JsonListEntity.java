package entity;

import java.util.List;

public class JsonListEntity {
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public List<?> getList() {
		return list;
	}

	public void setList(List<?> list) {
		this.list = list;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	private int state = 0;
	private List<?> list = null;
	private String html;
}
