package entity;

public class MessageEntity {
	private int id = 0;
	private int loginId =0;
	private int senderId = 0;
	private String senderName = "";
	private int receiverId = 0;
	private int receiverName = 0;
	private int categoryId = 0; //'消息类型 0：普通消息 1：好友请求消息 2：同意请求    3:课题组请求消息    -1：拒绝请求   对照Constant.COMMONMESSAGE及下面几个属性
	private int status = 0;  // '1：未读；0：已读 -1：删除',
	private String content = "";
	private String at = ""; //'创建时间',
	private int ts = 0; //'时间戳，等价于create_at',
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public int getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(int receiverName) {
		this.receiverName = receiverName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAt() {
		return at;
	}
	public void setAt(String at) {
		this.at = at;
	}
	public int getLoginId() {
		return loginId;
	}
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public int getTs() {
		return ts;
	}
	public void setTs(int ts) {
		this.ts = ts;
	}
}
