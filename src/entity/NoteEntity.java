package entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NoteEntity {
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getVisitCnt() {
		return visitCnt;
	}

	public void setVisitCnt(int visitCnt) {
		this.visitCnt = visitCnt;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getFirstAuthor() {
		return firstAuthor;
	}

	public void setFirstAuthor(String firstAuthor) {
		this.firstAuthor = firstAuthor;
	}

	public String getSecondAuthor() {
		return secondAuthor;
	}

	public void setSecondAuthor(String secondAuthor) {
		this.secondAuthor = secondAuthor;
	}

	public String getCommunAuthor() {
		return communAuthor;
	}

	public void setCommunAuthor(String communAuthor) {
		this.communAuthor = communAuthor;
	}

	public String getOtherAuthor() {
		return otherAuthor;
	}

	public void setOtherAuthor(String otherAuthor) {
		this.otherAuthor = otherAuthor;
	}

	public String getPeriodical() {
		return periodical;
	}

	public void setPeriodical(String periodical) {
		this.periodical = periodical;
	}

	public List<AttachmentEntity> getListAttachment() {
		return listAttachment;
	}

	public void setListAttachment(List<AttachmentEntity> listAttachment) {
		this.listAttachment = listAttachment;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getYearPage() {
		return yearPage;
	}

	public void setYearPage(String yearPage) {
		this.yearPage = yearPage;
	}

	private int id = 0;
	private int usrId = 0;
	private String usrName = "";
	private int categoryId = 0;
	private String categoryName = "";
	private String title = "";
	private String content = "";
	private String shortContent = "";
	private int visitCnt = 0;
	private String at = "";
	private Integer type; //论文：1 ； 专利:2
	private String firstAuthor; //全部作者
	private String secondAuthor; //第二作者  已不用
	private String communAuthor; //通讯作者
	private String otherAuthor; //其它作者  已不用
	private String periodical; //期刊名
	private Date publishDate; //出版时间
	private String yearPage; //年卷期及页码
	
	private List<AttachmentEntity> listAttachment = new ArrayList<AttachmentEntity>();//附件
}
