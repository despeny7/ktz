package entity;

public class ReplyEntity {
	
	private int id = 0;
	private int qaId = 0;
	private int usrId = 0;
	private String usrName = "";
	private int usrHd = 0;
	private String usrHdString = "";
	private String content = "";
	private String shortContent = "";
	private String at = "";
	private String usrAvatar = "";
	private String usrAvatarUrl = "";
	private String type; //回复类型； QA：问答，  BLOG：博客
	
	public String getUsrAvatar() {
		return usrAvatar;
	}
	public void setUsrAvatar(String usrAvatar) {
		this.usrAvatar = usrAvatar;
	}
	public String getUsrAvatarUrl() {
		return usrAvatarUrl;
	}
	public void setUsrAvatarUrl(String usrAvatarUrl) {
		this.usrAvatarUrl = usrAvatarUrl;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUsrId() {
		return usrId;
	}
	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}
	public String getUsrName() {
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	public int getUsrHd() {
		return usrHd;
	}
	public void setUsrHd(int usrHd) {
		this.usrHd = usrHd;
	}
	public String getUsrHdString() {
		return usrHdString;
	}
	public void setUsrHdString(String usrHdString) {
		this.usrHdString = usrHdString;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getShortContent() {
		return shortContent;
	}
	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}
	public String getAt() {
		return at;
	}
	public void setAt(String at) {
		this.at = at;
	}
	public int getQaId() {
		return qaId;
	}
	public void setQaId(int qaId) {
		this.qaId = qaId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
