package entity;

public class UserEntity {
	private int id = 0;
	private int loginId = 0;
	private String usrCode = "";
	private String usrPwd = "";
	private String usrName = "";
	/**
	 * 注册类型
	 * 0：个人注册；  1：课题组注册
	 */
	private int usrRole = 0; 
	private int usrHd = 0;
	private String usrHdString = "";
	private String usrHdStringM = "";
	private String usrHdStringL = "";
	private int sex = 0;
	private String sexString = "";
	private String brithday = "";
	private int degree = 0;
	private String degreeString = "";
	private int location = 0;
	private String locationString = "";
	private int university = 0;   //学院ID
	private int univId = 0; //大学ID
	private String provName; // 大学省份
	private String provId;//省份id
	private String universityString = "";
	private String universityString2 = "";
	private int deptId=0;
	private String mobile = "";
	private String email = "";
	private int regType = 0;
	private int usrVerify = 0;
	private String signature = "";
	private int score = 0;
	private int status = 0;
	private String lastIp = "";
	private String at = "";
	private String researchStudy="";
	private String skill ="";
	private String subtId ="";
	private String subString = "";
	private int title =0; ///< 职称
	private String titleString =""; ///< 职称
	private String researchDirect; //研究方向
	private int noteNum;//发表论文数
	private Integer ktzId = 0;//所属课题组Id,为0或者空则代表没有课题组
	private String ktzName;//课题组名称
	private Integer headManFlag;//课题组成员身份标识 ；  1:组长；  2：合作老师；  3：合作学生
	private int newMsgNum = 0; //未读新消息
	
	public int getUnivId() {
		return univId;
	}

	public void setUnivId(int univId) {
		this.univId = univId;
	}

	public String getProvName() {
		return provName;
	}

	public void setProvName(String provName) {
		this.provName = provName;
	}

	public String getUsrHdStringM() {
		return usrHdStringM;
	}

	public void setUsrHdStringM(String usrHdStringM) {
		this.usrHdStringM = usrHdStringM;
	}

	public String getUsrHdStringL() {
		return usrHdStringL;
	}

	public void setUsrHdStringL(String usrHdStringL) {
		this.usrHdStringL = usrHdStringL;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsrCode() {
		return usrCode;
	}

	public void setUsrCode(String usrCode) {
		this.usrCode = usrCode;
	}

	public String getUsrPwd() {
		return usrPwd;
	}

	public void setUsrPwd(String usrPwd) {
		this.usrPwd = usrPwd;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public int getUsrRole() {
		return usrRole;
	}

	public void setUsrRole(int usrRole) {
		this.usrRole = usrRole;
	}

	public int getUsrHd() {
		return usrHd;
	}

	public void setUsrHd(int usrHd) {
		this.usrHd = usrHd;
	}

	public String getUsrHdString() {
		return usrHdString;
	}

	public void setUsrHdString(String usrHdString) {
		this.usrHdString = usrHdString;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getSexString() {
		return sexString;
	}

	public void setSexString(String sexString) {
		this.sexString = sexString;
	}

	public String getBrithday() {
		return brithday;
	}

	public void setBrithday(String brithday) {
		this.brithday = brithday;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public String getDegreeString() {
		return degreeString;
	}

	public void setDegreeString(String degreeString) {
		this.degreeString = degreeString;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public String getLocationString() {
		return locationString;
	}

	public void setLocationString(String locationString) {
		this.locationString = locationString;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	// public int getUsrId() {
	// return usrId;
	// }
	//
	// public void setUsrId(int usrId) {
	// this.usrId = usrId;
	// }

	public int getRegType() {
		return regType;
	}

	public void setRegType(int regType) {
		this.regType = regType;
	}

	public int getUsrVerify() {
		return usrVerify;
	}

	public void setUsrVerify(int usrVerify) {
		this.usrVerify = usrVerify;
	}

	public String getLastIp() {
		return lastIp;
	}

	public void setLastIp(String lastIp) {
		this.lastIp = lastIp;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public int getUniversity() {
		return university;
	}

	public void setUniversity(int university) {
		this.university = university;
	}

	public String getUniversityString() {
		return universityString;
	}

	public void setUniversityString(String universityString) {
		this.universityString = universityString;
	}

	public String getResearchStudy() {
		return researchStudy;
	}

	public void setResearchStudy(String researchStudy) {
		this.researchStudy = researchStudy;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getUniversityString2() {
		return universityString2;
	}

	public void setUniversityString2(String universityString2) {
		this.universityString2 = universityString2;
	}

	public String getSubtId() {
		return subtId;
	}

	public void setSubtId(String subtId) {
		this.subtId = subtId;
	}

	public int getTitle() {
		return title;
	}

	public void setTitle(int title) {
		this.title = title;
	}

	public String getTitleString() {
		return titleString;
	}

	public void setTitleString(String titleString) {
		this.titleString = titleString;
	}

	public String getResearchDirect() {
		return researchDirect;
	}

	public void setResearchDirect(String researchDirect) {
		this.researchDirect = researchDirect;
	}

	public String getSubString() {
		return subString;
	}

	public void setSubString(String subString) {
		this.subString = subString;
	}

	public int getNoteNum() {
		return noteNum;
	}

	public void setNoteNum(int noteNum) {
		this.noteNum = noteNum;
	}


	public Integer getKtzId() {
		return ktzId;
	}

	public void setKtzId(Integer ktzId) {
		this.ktzId = ktzId;
	}

	public String getKtzName() {
		return ktzName;
	}

	public void setKtzName(String ktzName) {
		this.ktzName = ktzName;
	}

	public Integer getHeadManFlag() {
		return headManFlag;
	}

	public void setHeadManFlag(Integer headManFlag) {
		this.headManFlag = headManFlag;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getProvId() {
		return provId;
	}

	public void setProvId(String provId) {
		this.provId = provId;
	}

	public int getNewMsgNum() {
		return newMsgNum;
	}

	public void setNewMsgNum(int newMsgNum) {
		this.newMsgNum = newMsgNum;
	}

}
