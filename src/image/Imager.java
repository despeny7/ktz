package image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.IdentifyCmd;
import org.im4java.process.ArrayListOutputConsumer;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 图片的抽象类
 * 
 * 提供图片的上传、压缩、裁剪
 * 
 * 图片的压缩、裁剪采用用im4java+graphicsmagick实现
 * 
 * 环境安装步骤：
 * 
 * 1。安装graphicsmagick环境
 * 
 * 2.导入im4java.jar
 * 
 * 图片大小:
 * 
 * hp s:96 m:128 l:256
 * 
 * img s:128 m:512 l:768
 * 
 * @author despeny 2013-11-15
 */
public class Imager {

	/**
	 * 上传并生成图片（包括不同的尺寸）
	 * 
	 * @param type
	 *            1:img 2:hp 3 temp
	 * @param ext
	 * @param data
	 * @return
	 */
	public ImagerEntity create(byte[] data, String ext, String path, boolean zoom) {
		return create(data, ext, path, zoom, false);
	}

	/**
	 * 上传并生成图片（包括不同的尺寸）
	 * 
	 * @param type
	 *            1:img 2:hp 3 temp
	 * @param ext
	 * @param data
	 * @return
	 */
	public ImagerEntity create(byte[] data, String ext, String path, boolean zoom, boolean isAvatar) {
		ImagerEntity entity = new ImagerEntity();
		try {
			if (ext.equals("")) {
				ext = "jpg";
			}
			ext = ext.toLowerCase();

			int imgId = getId();
			int folder = getFolder(imgId);
			String name = getImgName(imgId) + "." + ext;
			String imgName = getName(imgId, ext, "o", path);
			int[] wh = original(imgName, ext, data, isAvatar);

			/**
			 * 生成不同尺寸的图片
			 */
			if (zoom) {
				small(imgId, path, imgName);
				middle(imgId, path, imgName);
				large(imgId, path, imgName);
			}

			entity.setFolder(folder);
			entity.setId(imgId);
			entity.setName(name);
			entity.setStatus(1);
			entity.setWidth(wh[0]);
			entity.setHeight(wh[1]);
		} catch (Exception e) {
			e.printStackTrace();
			entity.setStatus(-2);
		}
		return entity;
	}

	/**
	 * 裁剪
	 * 
	 * @param type
	 *            1:img 2:hp 3 temp
	 * @param ext
	 * @param data
	 * @return
	 */
	public int cut(int imgId, String ext, String path, int x1, int y1, int x2) {
		try {
			if (ext.equals("")) {
				ext = "jpg";
			}
			ext = ext.toLowerCase();
			String imgName = getName(imgId, ext, "o", path);

			// SMALL
			String s = getName(imgId, "jpg", "s", path);
			cut(imgName, s, 48, x1, y1, x2);
			// MIDDLE
			String m = getName(imgId, "jpg", "m", path);
			cut(imgName, m, 64, x1, y1, x2);
			// LARGE
			String l = getName(imgId, "jpg", "l", path);
			cut(imgName, l, 128, x1, y1, x2);
			return imgId;
		} catch (Exception e) {
			e.printStackTrace();
			return -2;
		}
	}

	/**
	 * 
	 * @param imgId
	 * @param host
	 * @return
	 */
	public String get(int imgId, String host) {
		return this.get(imgId, ImageSize.S, host);
	}

	/**
	 * 获取头像
	 * 
	 * @param imageId
	 * @param host
	 * @return
	 */
	public String getAvatar(int imageId, String host) {
		if (0 >= imageId) {
			imageId = 700;
		}
		return get(imageId, ImageSize.L, host);
	}

	/**
	 * 获取图片的URL
	 * 
	 * @param imgId
	 * @param size
	 * @return
	 */
	public String get(int imgId, ImageSize size, String host) {
		if (imgId < 0) {
			return "";
		}
		String url = host;
		// 给URL的末尾添加/
		if (url.length() == 0 || !url.endsWith("/")) {
			url = url + "/";
		}

		if (ImageSize.M.equals(size)) {
			url = url + "m";
		} else if (ImageSize.L.equals(size)) {
			url = url + "l";
		} else if (ImageSize.O.equals(size)) {
			url = url + "o";
		} else {
			url = url + "s";
		}

		if (imgId == 100 || imgId == 700) {
			url = url + "/" + (imgId * 10000) + ".png";
		} else {
			url = url + "/" + getFolder(imgId) + "/" + getImgName(imgId) + ".jpg";
		}
		return url;
	}

	/**
	 * 返回图片的绝对路径
	 * 
	 * @param imgId
	 * @param ext
	 * @return
	 */
	private String getName(int imgId, String ext, String type, String path) {
		String folder = path + "/" + type + "/" + getFolder(imgId);
		File file = new File(folder);
		if (!file.exists()) {
			file.mkdirs();
		}

		return folder + "/" + getImgName(imgId) + "." + ext;
	}

	/**
	 * 
	 * @param imgId
	 * @return
	 */
	private int getFolder(int imgId) {
		return (imgId / 5000) + 1;
	}

	/**
	 * 
	 * @param imgId
	 * @return
	 */
	private String getImgName(int imgId) {
		int s = (int) Math.sqrt(imgId);
		String imgName = Integer.toHexString(imgId * s) + Integer.toHexString(s % 16);
		return imgName;
	}

	/**
	 * 获取图片Id
	 * 
	 * @return
	 */
	private synchronized static int getId() {
		if (imgId == 0) {
			imgId = (int) (System.currentTimeMillis() / 1000) - 1378800000;
		} else {
			imgId++;
		}
		return imgId;
	}

	/**
	 * 原图
	 * 
	 * @param path
	 * @param ext
	 * @param bytes
	 * @throws Exception
	 */
	private int[] original(String path, String ext, byte[] bytes, boolean isAvatar) throws Exception {
		return draw(path, ext, bytes, isAvatar);
	}

	/**
	 * 小图
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void small(int imgId, String path, String orgImgName) throws Exception {
		String imgName = getName(imgId, "jpg", "s", path);
		zoom(orgImgName, imgName, 128);
	}

	/**
	 * 中图
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void middle(int imgId, String path, String orgImgName) throws Exception {
		String imgName = getName(imgId, "jpg", "m", path);
		zoom(orgImgName, imgName, 512);
	}

	/**
	 * 大图
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void large(int imgId, String path, String orgImgName) throws Exception {
		String imgName = getName(imgId, "jpg", "l", path);
		zoom(orgImgName, imgName, 768);
	}

	/**
	 * 画图
	 * 
	 * @param path
	 * @param ext
	 * @param bytes
	 * @throws Exception
	 */
	private int[] draw(String path, String ext, byte[] bytes, boolean isAvatar) throws Exception {
		ByteArrayInputStream imgStream = new ByteArrayInputStream(bytes);
		BufferedImage image = ImageIO.read(imgStream);
		// *以下是如果是需要自己裁剪、压缩图片的方法
		int oWidth = image.getWidth();
		int oHeight = image.getHeight();

		int sWidth = oWidth; /// < 目标图片宽
		int sHeight = oHeight; /// < 目标图片高

		int sX = 0;
		int sY = 0;

		if (isAvatar) {
			// 将图片重绘成白色底、原图片居中
			if (sWidth > sHeight) {
				sY = (sWidth - sHeight) / 2;
				sHeight = sWidth;
			} else if (sHeight > sWidth) {
				sX = (sHeight - sWidth) / 2;
				sWidth = sHeight;
			}
		}

		BufferedImage tag = new BufferedImage(sWidth, sHeight, BufferedImage.TYPE_INT_RGB);
		tag.getGraphics().setColor(Color.white);
		tag.getGraphics().fillRect(0, 0, sHeight, sHeight);
		tag.getGraphics().drawImage(image, sX, sY, oWidth, oHeight, null);

		FileOutputStream out = new FileOutputStream(path);
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(tag);
		/* 压缩质量 */
		jep.setQuality(1, true);
		encoder.encode(tag, jep);
		out.close();
		// ImageIO.write(image, ext, new File(path));
		return new int[] { sWidth, sHeight };
	}

	/**
	 * 等宽缩放 如果目标宽小于原宽，则不缩放
	 * 
	 * @param imagePath
	 * @param newPath
	 * @param width
	 * @throws Exception
	 */
	private void zoom(String orgImgName, String newPath, int width) throws Exception {
		int w = getWidth(orgImgName);
		int h = getHeight(orgImgName);
		int zh = h;
		int zw = w;
		if (width > 0) {
			if (w > width) {
				zw = width;
				zh = (width * h / w) + 1;
			}
		}
		IMOperation op = new IMOperation();
		op.addImage(orgImgName);
		op.resize(zw, zh);
		op.addImage(newPath);
		ConvertCmd convert = new ConvertCmd(true);
		convert.run(op);
	}

	/**
	 * 图片压缩+裁剪(正方形)
	 * 
	 * @param imagePath
	 * @param newPath
	 * @param ow
	 * @param sx
	 * @param sy
	 * @param sx2
	 * @throws Exception
	 * 
	 */
	private void cut(String imagePath, String newPath, int ow, int sx, int sy, int sx2) throws Exception {
		int sw = getWidth(imagePath);
		//
		// 先裁剪在缩放
		// 初始化
		sx = sx < 0 ? 0 : sx;
		sx = sx > sw ? sw : sx;

		int w = sx2 - sx;

		IMOperation op = new IMOperation();
		op.addImage(imagePath);
		op.crop(w, w, sx, sy).resize(ow, ow);
		op.addImage(newPath);
		ConvertCmd convert = new ConvertCmd(true);
		convert.run(op);
	}

	// /**
	// * 图片压缩+裁剪
	// *
	// * @param imagePath
	// * 原图路径
	// * @param newPath
	// * 目标图路径
	// * @param oWidth
	// * 目标宽
	// * @param oHeight
	// * 目标高
	// * @param sx
	// * X轴坐标 * @param sy Y轴坐标
	// * @throws Exception
	// */
	// private void cut(String imagePath, String newPath, int ow, int oh, int
	// sx,
	// int sy, int sx2, int sy2) throws Exception {
	// int sw = getWidth(imagePath);
	// int sh = getHeight(imagePath);
	//
	// // 先裁剪在缩放
	// // 初始化
	// sx = sx < 0 ? 0 : sx;
	// sx = sx > sw ? sw : sx;
	//
	// sy = sy < 0 ? 0 : sy;
	// sy = sy > sh ? sh : sy;
	//
	// int w = sx2 - sx;
	// int h = sy2 - sy;
	//
	// IMOperation op = new IMOperation();
	// op.addImage(imagePath);
	// op.crop(w, h, sx, sy).resize(ow, oh);
	// op.addImage(newPath);
	// ConvertCmd convert = new ConvertCmd(true);
	// convert.run(op);
	// }

	/**
	 * 获取原宽
	 * 
	 * @param imagePath
	 * @return
	 */
	private static int getWidth(String imagePath) throws Exception {
		int line = 0;
		IMOperation op = new IMOperation();
		op.format("%w"); //
		op.addImage(1);
		IdentifyCmd identifyCmd = new IdentifyCmd(true);
		ArrayListOutputConsumer output = new ArrayListOutputConsumer();
		identifyCmd.setOutputConsumer(output);
		identifyCmd.run(op, imagePath);
		ArrayList<String> cmdOutput = output.getOutput();
		assert cmdOutput.size() == 1;
		line = Integer.parseInt(cmdOutput.get(0));
		return line;
	}

	/**
	 * 获取原高
	 * 
	 * @param imagePath
	 * @return
	 * @throws IM4JavaException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	private static int getHeight(String imagePath) throws Exception {
		int line = 0;
		IMOperation op = new IMOperation();

		op.format("%h");
		op.addImage(1);
		IdentifyCmd identifyCmd = new IdentifyCmd(true);
		ArrayListOutputConsumer output = new ArrayListOutputConsumer();
		identifyCmd.setOutputConsumer(output);
		identifyCmd.run(op, imagePath);
		ArrayList<String> cmdOutput = output.getOutput();
		assert cmdOutput.size() == 1;
		line = Integer.parseInt(cmdOutput.get(0));
		return line;
	}

	private static int imgId = 0;
}
