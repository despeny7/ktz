package impl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppContext {

	private static AppContext instance;

	private ApplicationContext appContext;

	public synchronized static AppContext getInstance() {
		if (instance == null) {
			instance = new AppContext();
		}
		return instance;
	}
	
	/**
	 * 
	 * @param beanId
	 * @return
	 */
	public Object getBean(String beanId) {
		return appContext.getBean(beanId);
	}
	
	/**
	 * 
	 */
	private AppContext() {
		this.appContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
	}
}
