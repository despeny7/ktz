package impl;

import image.Imager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import dao.*;
import entity.BlogEntity;
import entity.ReplyEntity;

public class BlogImpl {
	
	private IBlogDao blogDao = null;

	public IBlogDao getBlogDao() {
		return blogDao;
	}

	public void setBlogDao(IBlogDao blogDao) {
		this.blogDao = blogDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public int cnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.blogDao.listCnt(map);
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<BlogEntity> list(int usrId, int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<BlogEntity> list = this.blogDao.list(map);
		if (null != list){
			Imager imager = new Imager();
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
				int imageId = comm.Helper.parseInt(list.get(i).getUsrAvatar());
				list.get(i).setUsrAvatarUrl(imager.getAvatar(imageId, host));
				String content = list.get(i).getContent();
				if (content.length() > 0) {
					content = content.replaceAll("<[^>]+>", "").replace("nbsp;", "").replace("&", "");
				}
				System.out.println(content);
				list.get(i).setContent(content);
			}
		}
		return list;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public BlogEntity info(int id) {
		BlogEntity entity = this.blogDao.info(id);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
		String content = entity.getContent();
		content = content.replace("nowrap;", "normal;");
		entity.setContent(content);
		return entity;
	}

	/**
	 * 添加
	 * 
	 * @param news
	 * @return
	 */
	public int add(BlogEntity entity) {
		return this.blogDao.add(entity);
	}

	/**
	 * 编辑
	 * 
	 * @param news
	 * @return
	 */
	public int upt(BlogEntity entity) {
		return this.blogDao.upt(entity);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param newsId
	 * @return
	 * 
	 */
	public int del(int loginId, int id) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", id);
		return this.blogDao.del(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(BlogEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
	
	public int addReply(ReplyEntity entity) {
		return this.blogDao.addReply(entity);
	}
	
	public List<ReplyEntity> replyList(int qaId, int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("qaId", qaId);
		map.put("start", start);
		map.put("size", size);
		List<ReplyEntity> list = this.blogDao.listReply(map);
		if (null != list) {
			Imager imager = new Imager();
			for (int i = 0; i < list.size(); i++) {
				exchgR(list.get(i));
				int imageId = comm.Helper.parseInt(list.get(i).getUsrAvatar());
				list.get(i).setUsrAvatarUrl(imager.getAvatar(imageId, host));
			}
		}
		return list;
	}
	
	/**
	 * 统计数
	 * 
	 * @param usrId
	 * @return
	 */
	public int replyListCnt(int qaId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("qaId", qaId);
		return this.blogDao.listReplyCnt(map);
	}
	
	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchgR(ReplyEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
