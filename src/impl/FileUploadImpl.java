package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dao.IFileUploadDao;
import entity.AttachmentEntity;


public class FileUploadImpl {

	private IFileUploadDao fileUploadDao;
	
	public int add(AttachmentEntity entity){
		return fileUploadDao.add(entity);
	}
	
	/**
	 * 替换生成的外键ID
	 * @param newEntityId
	 * @param oldEntityId
	 * @return
	 */
	public int uptEntityId(String newEntityId, String oldEntityId){
		Map<String, String> map = new HashMap<String, String>();
		map.put("newEntityId", newEntityId);
		map.put("oldEntityId", oldEntityId);
		return fileUploadDao.uptEntityId(map);
	}
	
	public List<AttachmentEntity> listAttachment(Long entityId, String entityName){
		Map<String, String> map = new HashMap<String, String>();
		map.put("entityId", entityId+"");
		map.put("entityName", entityName);
		List<AttachmentEntity> listAttachment = fileUploadDao.listAttachment(map);
		for(AttachmentEntity file : listAttachment){
			if(file.getFileName().length() > 20){
				file.setShortFileName(file.getFileName().substring(0, 20) + "...");
			}else{
				file.setShortFileName(file.getFileName());
			}
		}
		return listAttachment;
	}
	
	public int del(int entityId){
		return fileUploadDao.del(entityId);
	}
	
	public AttachmentEntity info(int id){
		return fileUploadDao.info(id);
	}

	public IFileUploadDao getFileUploadDao() {
		return fileUploadDao;
	}

	public void setFileUploadDao(IFileUploadDao fileUploadDao) {
		this.fileUploadDao = fileUploadDao;
	}
}
