package impl;

import image.ImageSize;
import image.Imager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class FridImpl {

	private IFridDao fridDao = null;
	private IUserDao userDao = null;

	/**
	 * 发送消息
	 * 
	 * @param loginId
	 * @param receverId
	 * @return
	 */
	public int add(int id, int loginId, int usrId, int fridId) {
		FriendEntity entity = new FriendEntity();
		entity.setId(id);
		entity.setUsrId(usrId);
		entity.setLoginId(loginId);
		entity.setFridId(fridId);
		entity.setStatus(3);
		entity.setRemark("");
		int ret = fridDao.exist(entity);
		if (0 < ret) {
			return 99;
		} else {
			return fridDao.add(entity);
		}
	}

	/**
	 * 删除（接收方）
	 * 
	 * @param id
	 * @param loginId
	 * @return
	 */
	public int del(int id, int loginId) {
		FriendEntity entity = new FriendEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		return fridDao.del(entity);
	}

	/**
	 * 列表
	 * 
	 * @param loginId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<FriendEntity> list(int loginId, int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		map.put("start", start);
		map.put("size", size);
		List<FriendEntity> list = this.fridDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i), host);
			}
		return list;
	}

	/**
	 * 列表
	 * 
	 * @param loginId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<FriendEntity> top(int loginId, String host) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		List<FriendEntity> list = this.fridDao.top(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i), host);
			}
		return list;
	}

	/**
	 * 统计数
	 * 
	 * @param loginId
	 * @return
	 */
	public int cnt(int loginId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		return this.fridDao.cnt(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(FriendEntity entity, String host) {
		UserEntity user = userDao.info(entity.getFridId());

		String date = user.getAt();
		date = Helper.getShortDate1(date);
		user.setAt(date);
		int sex = user.getSex();
		if (sex == 0) {
			user.setSexString("女");
		} else if (sex == 1) {
			user.setSexString("男");
		} else {
			user.setSexString("保密");
		}
		String brithday = user.getBrithday();
		brithday = Helper.getShortDate1(date);
		user.setBrithday(brithday);
		int imgId = user.getUsrHd();
		if (imgId <= 0) {
			imgId = 700;
		}
		Imager imager = new Imager();
		user.setUsrHdString(imager.get(imgId, ImageSize.S, host));
		user.setUsrHdStringM(imager.get(imgId, ImageSize.M, host));
		user.setUsrHdStringL(imager.get(imgId, ImageSize.L, host));

		String skill = Helper.chgNewLine(user.getSkill());
		user.setSkill(skill);
		String rs = Helper.chgNewLine(user.getResearchStudy());
		user.setResearchStudy(rs);
		entity.setFriInfo(user);
	}

	public IFridDao getFridDao() {
		return fridDao;
	}

	public void setFridDao(IFridDao fridDao) {
		this.fridDao = fridDao;
	}

	public IUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
}
