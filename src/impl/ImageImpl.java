package impl;

import image.ImageSize;
import image.Imager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

/**
 * 
 * @author Administrator
 * 
 */
public class ImageImpl {

	private IImageDao imageDao = null;

	public IImageDao getImageDao() {
		return imageDao;
	}

	public void setImageDao(IImageDao imageDao) {
		this.imageDao = imageDao;
	}

	/**
	 * 添加
	 * 
	 * @param entity
	 * @return
	 */
	public int add(ImageEntity entity) {
		int ret = this.imageDao.add(entity);
		if (ret > 0) {
			ret = this.imageDao.addDetail(entity);
		}
		return ret;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<ImageEntity> list(int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		start = start > 0 ? start : 0;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("start", start);
		map.put("size", size);
		List<ImageEntity> list = this.imageDao.list(map);
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				String date = list.get(i).getAt();
				date = Helper.getShortDate3(date);
				list.get(i).setAt(date);
				String o = new Imager().get(list.get(i).getImgId(),
						ImageSize.O, host);
				String m = new Imager().get(list.get(i).getImgId(),
						ImageSize.L, host);
				list.get(i).setImgUrlO(o);
				list.get(i).setImgUrlM(m);
			}
		}
		return list;
	}

	/**
	 * CNT
	 * @return
	 */
	public int cnt() {
		return this.imageDao.cnt();
	}
}
