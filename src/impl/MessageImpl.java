package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import comm.Helper;
import entity.*;
import dao.*;

public class MessageImpl {

	private IMessageDao messageDao = null;

	/**
	 * 发送消息
	 * 
	 * @param loginId
	 * @param receverId
	 * @return
	 */
	public int add(int id, int loginId, int receiverId) {
		return add(id, loginId, receiverId, 1, "");
	}

	/**
	 * 发送消息
	 * 
	 * @param loginId
	 * @param receverId
	 * @param categoryId
	 * @param content
	 * @return
	 */
	public int add(int id, int loginId, int receiverId, int categoryId,
			String content) {
		MessageEntity entity = new MessageEntity();
		entity.setCategoryId(categoryId);
		entity.setReceiverId(receiverId);
		entity.setSenderId(loginId);
		entity.setLoginId(loginId);
		entity.setContent(content);
		entity.setTs(Helper.getTs());
		entity.setId(id);
		return messageDao.add(entity);
	}

	/**
	 * 删除（接收方）
	 * 
	 * @param id
	 * @param loginId
	 * @return
	 */
	public int del(int id, int loginId) {
		MessageEntity entity = new MessageEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		return messageDao.del(entity);
	}

	/**
	 * 标示已读
	 * 
	 * @param id
	 * @param loginId
	 * @return
	 */
	public int upt(int id, int loginId) {
		MessageEntity entity = new MessageEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		return messageDao.upt(entity);
	}

	/**
	 * 列表
	 * 
	 * @param loginId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<MessageEntity> list(int loginId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		map.put("start", start);
		map.put("size", size);
		List<MessageEntity> list = this.messageDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		return list;

	}
	
	/**
	 * 未读消息列表
	 * 
	 * @param loginId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<MessageEntity> newMsgList(int loginId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		map.put("start", start);
		map.put("size", size);
		List<MessageEntity> list = this.messageDao.newMsgList(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		return list;

	}

	/**
	 * 统计数
	 * 
	 * @param loginId
	 * @return
	 */
	public int cnt(int loginId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		return this.messageDao.cnt(map);
	}

	/**
	 * 用于判断最近是否有新的消息
	 * 
	 * @param loginId
	 * @param ts
	 * @return
	 */
	public int unCnt(int loginId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("loginId", loginId);
		return this.messageDao.unCnt(map);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(MessageEntity entity) {
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
	
	/**
	 * 标示消息是否同意拒绝
	 * 
	 * @param id
	 * @param loginId
	 * @return
	 */
	public int uptmMsgCategory(int id, int loginId, int categoryId) {
		MessageEntity entity = new MessageEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		entity.setCategoryId(categoryId);
		
		int ret = messageDao.uptmMsgCategory(entity);
		//更新未读消息
		Map<String, Object> session = ActionContext.getContext().getSession();
		UserEntity u = (UserEntity) session.get("user_info");
		int newMsgNum = this.unCnt(loginId);
		u.setNewMsgNum(newMsgNum);
		session.put("user_info", u);
		return ret;
	}
	
	public MessageEntity info(int id){
		return messageDao.info(id);
	}

	public IMessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(IMessageDao messageDao) {
		this.messageDao = messageDao;
	}
}
