package impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashAttributeSet;

import dao.IPubDao;
import entity.CategoryEntity;
import entity.DeptEntity;

public class PubImpl {

	private IPubDao pubDao = null;

	public IPubDao getPubDao() {
		return pubDao;
	}

	public void setPubDao(IPubDao pubDao) {
		this.pubDao = pubDao;
	}

	/**
	 * 城市列表
	 * 
	 * @return
	 */
	public synchronized List<CategoryEntity> cityList(String p) {
		if (cityList == null) {
			cityList = this.pubDao.cityList();
		}
		List<CategoryEntity> result = new ArrayList<CategoryEntity>();
		int count = 0;
		for (int i = 0; i < cityList.size(); i++) {
			if (cityList.get(i).getCategoryName().indexOf(p) > -1) {
				result.add(cityList.get(i));
				count++;
				if (count > 10) {
					break;
				}
			}
		}
		if (result.size() < 1) {
			for (int i = 0; i < cityList.size(); i++) {
				result.add(cityList.get(i));
				if (i > 10) {
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 学历列表
	 * 
	 * @return
	 */
	public synchronized List<CategoryEntity> degreeList() {
		if (degreeList == null) {
			degreeList = this.pubDao.degreeList();
		}
		return degreeList;
	}

	/**
	 * 职称列表
	 * 
	 * @return
	 */
	public synchronized List<CategoryEntity> titleList() {
		if (titleList == null) {
			titleList = this.pubDao.titleList();
		}
		return titleList;
	}

	/**
	 * 招聘职称要求
	 * 
	 * @return
	 */
	public synchronized List<CategoryEntity> jobTitleList() {
		if (jobTitleList == null) {
			jobTitleList = this.pubDao.jobTitleList();
		}
		return jobTitleList;
	}

	/**
	 * 学科列表
	 * 
	 * @return
	 */
	public synchronized List<CategoryEntity> subjectList(int categryId) {
		if (subjectList == null) {
			subjectList = this.pubDao.subjectList();
		}
		return subjectList;
	}

	/**
	 * 获取列表
	 * 
	 * @return
	 */
	private List<DeptEntity> deptList() {
		if (deptList == null) {
			deptList = this.pubDao.deptList();
		}
		return deptList;
	}

	/**
	 * 获取列表
	 * 
	 * @return
	 */
	public List<CategoryEntity> univList(String provId) {
		List<CategoryEntity> univList = null;
		List<DeptEntity> list = this.deptList();
		List<Integer> ids = new ArrayList<Integer>();
		if (null != list) {
			univList = new ArrayList<CategoryEntity>();
			for (int i = 0; i < list.size(); i++) {
				if (provId.equals(list.get(i).getProvId())) {
					CategoryEntity entity = new CategoryEntity();
					entity.setCategoryId(list.get(i).getUnivId());
					entity.setCategoryName(list.get(i).getUnivName());

					if (!ids.contains(list.get(i).getUnivId())) {
						univList.add(entity);
						ids.add(list.get(i).getUnivId());
					}
				}
			}
		}

		return univList;
	}

	/**
	 * 获取列表
	 * 
	 * @return
	 */
	public List<CategoryEntity> deptList(int univId) {
		List<CategoryEntity> univList = null;
		List<DeptEntity> list = this.deptList();
		if (null != list) {
			univList = new ArrayList<CategoryEntity>();
			Map<Integer, String> map = new HashMap<Integer, String>();
			for (int i = 0; i < list.size(); i++) {
				int uId = list.get(i).getUnivId();
				if (uId == univId) {
					CategoryEntity entity = new CategoryEntity();
					entity.setCategoryId(list.get(i).getDeptId());
					entity.setCategoryName(list.get(i).getDeptName());
					if (!map.containsKey(entity.getCategoryId())) {
						map.put(entity.getCategoryId(), entity.getCategoryName());
						univList.add(entity);
					}
				}
			}
		}

		return univList;
	}

	/**
	 * 更新成汉字
	 * 
	 * @param entity
	 * @return
	 */
	public int uptDept(CategoryEntity entity) {
		return this.pubDao.uptDept(entity);
	}

	// 多个学科时，要把学科名字拼凑成字符串
	public String subjecNameList(String subtId) {
		String[] ids = subtId.split(",");
		List<CategoryEntity> list = this.pubDao.subjecNameList(ids);
		StringBuilder subjecName = new StringBuilder("");
		for (int i = 0; i < list.size(); i++) {
			if (i == 0) {
				subjecName.append(list.get(i).getCategoryName());
			} else {
				subjecName.append(",").append(list.get(i).getCategoryName());
			}
		}
		return subjecName.toString();
	}

	private static List<CategoryEntity> subjectList = null;
	private static List<DeptEntity> deptList = null;
	private static List<CategoryEntity> degreeList = null;
	private static List<CategoryEntity> cityList = null;
	private static List<CategoryEntity> titleList = null;
	private static List<CategoryEntity> jobTitleList = null;

}
