package impl;

import image.Imager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import dao.IQADao;
import entity.QaEntity;
import entity.ReplyEntity;

public class QAImpl {

	private IQADao qaDao = null;

	/**
	 * 添加
	 * 
	 * @param entity
	 * @return
	 */
	public int add(QaEntity entity) {
		return this.qaDao.add(entity);
	}

	/**
	 * 更新
	 * 
	 * @param entity
	 * @return
	 */
	public int upt(QaEntity entity) {
		return this.qaDao.upt(entity);
	}
	
	/**
	 * 更新
	 * 
	 * @param entity
	 * @return
	 */
	public int del(int id,int loginId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", id);
		return this.qaDao.del(map);
	}

	/**
	 * 列表
	 * usrId是指文章所有人，usrId为0或者空，则查询所有文章
	 * followUserId是登陆人是否关注该文章，followUserId为0或者空，则不查询当前登陆人是否关注该文章
	 * @param map
	 * @return
	 */
	public List<QaEntity> list(int usrId, int page, int size, int followUserId,String host) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		map.put("followUserId", followUserId);
		List<QaEntity> list = this.qaDao.list(map);
		if (null != list) {
			Imager imager = new Imager();
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
				int imageId = comm.Helper.parseInt(list.get(i).getUsrAvatar());
				list.get(i).setUsrAvatarUrl(imager.getAvatar(imageId,host ));
			}
		}
		return list;
	}

	/**
	 * 统计数
	 * 
	 * @param usrId
	 * @return
	 */
	public int listCnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.qaDao.listCnt(map);
	}

	/**
	 * 置顶5
	 * 
	 * @param usrId
	 * @return
	 */
	public List<QaEntity> top(int usrId, int size,int followUserId, String host) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("size", size);
		map.put("followUserId", followUserId);
		List<QaEntity> list = this.qaDao.top(map);
		if (null != list) {
			Imager imager = new Imager();
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
				int imageId = comm.Helper.parseInt(list.get(i).getUsrAvatar());
				list.get(i).setUsrAvatarUrl(imager.getAvatar(imageId, host));
				String content = list.get(i).getContent();
				if (content.length() > 0) {
					content = content.replaceAll("<[^>]+>", "");
				}
				list.get(i).setContent(content);
			}
		}
		return list;
	}

	/**
	 * 查找
	 * 
	 * @param usrId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<QaEntity> search(int usrId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<QaEntity> list = this.qaDao.list(map);
		if (null != list) {
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		}
		return list;
	}

	/**
	 * 统计数
	 * 
	 * @param usrId
	 * @return
	 */
	public int searchCnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.qaDao.listCnt(map);
	}

	/**
	 * 回复
	 * 
	 * @param entity
	 * @return
	 */
	public int addReply(ReplyEntity entity) {
		return this.qaDao.addReply(entity);
	}

//	/**
//	 * 更新
//	 * 
//	 * @param entity
//	 * @return
//	 */
//	public int delReply(int id,int loginId) {
//		Map<String, Integer> map = new HashMap<String, Integer>();
//		map.put("usrId", loginId);
//		map.put("id", id);
//		return this.qaDao.del(map);
//	}
	
	
	/**
	 * 
	 * @param qaId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<ReplyEntity> replyList(int qaId, int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("qaId", qaId);
		map.put("start", start);
		map.put("size", size);
		List<ReplyEntity> list = this.qaDao.listReply(map);
		if (null != list) {
			Imager imager = new Imager();
			for (int i = 0; i < list.size(); i++) {
				exchgR(list.get(i));
				int imageId = comm.Helper.parseInt(list.get(i).getUsrAvatar());
				list.get(i).setUsrAvatarUrl(imager.getAvatar(imageId, host));
			}
		}
		return list;
	}
	
	/**
	 * 统计数
	 * 
	 * @param usrId
	 * @return
	 */
	public int replyListCnt(int qaId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("qaId", qaId);
		return this.qaDao.listReplyCnt(map);
	}
	
	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public QaEntity info(int id) {
		QaEntity entity = this.qaDao.info(id);
		exchg(entity);
		return entity;
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(QaEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
	
	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchgR(ReplyEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
	
	/**
	 * 回答列表
	 * 
	 * @param map
	 * @return
	 */
	public List<QaEntity> myAnswerList(int usrId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<QaEntity> list = this.qaDao.myAnswerList(map);
		if (null != list) {
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		}
		return list;
	}

	/**
	 * 回答列表统计数
	 * 
	 * @param usrId
	 * @return
	 */
	public int myAnswerListCnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.qaDao.myAnswerListCnt(map);
	}


	public IQADao getQaDao() {
		return qaDao;
	}

	public void setQaDao(IQADao qaDao) {
		this.qaDao = qaDao;
	}
}
