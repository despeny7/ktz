package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class RGImpl {

	private IRGDao rgDao = null;

	public IRGDao getRgDao() {
		return rgDao;
	}

	public void setRgDao(IRGDao rgDao) {
		this.rgDao = rgDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public int cnt(int usrId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		return this.rgDao.listCnt(map);
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public List<ResearchGroupEntity> list(int usrId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", usrId);
		map.put("start", start);
		map.put("size", size);
		List<ResearchGroupEntity> list = this.rgDao.list(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		return list;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public ResearchGroupEntity info(int id) {
		ResearchGroupEntity entity = this.rgDao.info(id);
		if (null != entity) {
			String date = entity.getAt();
			date = Helper.getShortDate3(date);
			entity.setAt(date);
			String content = entity.getContent();
			content = content.replace("nowrap;", "normal;");
			entity.setContent(content);
		}
		return entity;
	}

	/**
	 * 添加
	 * 
	 * @param note
	 * @return
	 */
	public int add(ResearchGroupEntity entity) {
		return this.rgDao.add(entity);
	}

	/**
	 * 编辑
	 * 
	 * @param note
	 * @return
	 */
	public int upt(ResearchGroupEntity entity) {
		return this.rgDao.upt(entity);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param noteId
	 * @return
	 * 
	 */
	public int del(int loginId, int rgId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", rgId);
		return this.rgDao.del(map);
	}
	
	/**
	 * 获取id
	 * @param loginId
	 * @return
	 */
	public int getId(int loginId){
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		String sid= this.rgDao.getId(map);
		if(null == sid ){
			return 0;
		}
		else{
			return Helper.parseInt(sid);
		}
		
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(ResearchGroupEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
