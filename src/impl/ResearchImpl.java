package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class ResearchImpl {

	private IResearchDao researchDao = null;

	public IResearchDao getResearchDao() {
		return researchDao;
	}

	public void setResearchDao(IResearchDao researchDao) {
		this.researchDao = researchDao;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public ResearchEntity searchByUserId(int userid) {
		ResearchEntity info = this.researchDao.searchByUserId(userid);
		if(null != info){
			exchg(info);
		}
		return info;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public ResearchEntity info(int id) {
		ResearchEntity info = this.researchDao.info(id);
		if(null != info){
			String date = info.getAt();
			date = Helper.getShortDate3(date);
			info.setAt(date);
			String content = info.getContent();
			content = content.replace("nowrap;", "normal;");
			info.setContent(content);
		}
		return info;
	}

	/**
	 * 添加
	 * 
	 * @param note
	 * @return
	 */
	public int add(ResearchEntity note) {
		return this.researchDao.add(note);
	}

	/**
	 * 编辑
	 * 
	 * @param note
	 * @return
	 */
	public int upt(ResearchEntity note) {
		return this.researchDao.upt(note);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(ResearchEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 100) {
			content = content.substring(0, 100);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
