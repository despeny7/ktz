package impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comm.Helper;

import entity.*;
import dao.*;

public class TypoImpl {

	private ITypoDao typoDao = null;

	public ITypoDao getTypoDao() {
		return typoDao;
	}

	public void setTypoDao(ITypoDao typoDao) {
		this.typoDao = typoDao;
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	public TypoEntity info(int id) {
		TypoEntity note = this.typoDao.info(id);
		String date = note.getAt();
		date = Helper.getShortDate3(date);
		note.setAt(date);
		String content = note.getContent();
		content = content.replace("nowrap;", "normal;");
		note.setContent(content);
		return note;
	}

	/**
	 * 添加
	 * 
	 * @param note
	 * @return
	 */
	public int add(TypoEntity note) {
		return this.typoDao.add(note);
	}

	/**
	 * 编辑
	 * 
	 * @param note
	 * @return
	 */
	public int upt(TypoEntity note) {
		return this.typoDao.upt(note);
	}

	/**
	 * 删除
	 * 
	 * @param loginId
	 * @param noteId
	 * @return
	 * 
	 */
	public int del(int loginId, int noteId) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("usrId", loginId);
		map.put("id", noteId);
		return this.typoDao.del(map);
	}

	/**
	 * 分类列表（Note)
	 * 
	 * @return
	 */
	public List<CategoryEntity> categoryList() {
		List<CategoryEntity> list = this.typoDao.categoryList();
		return list;
	}

	/**
	 * 
	 * @param categoryId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<TypoEntity> search(int categoryId, int page, int size) {
		int start = 0;
		start = (page - 1) * size;
		start = start > 0 ? start : 0;
		SearchEntity entity = new SearchEntity();
		entity.setCategoryId(categoryId);
		entity.setSize(size);
		entity.setStart(start);
		List<TypoEntity> list = this.typoDao.search(entity);
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				exchg(list.get(i));
			}
		}
		return list;
	}

	/**
	 * 
	 * @param categoryId
	 * @return
	 */
	public int searchCnt(int categoryId) {
		SearchEntity entity = new SearchEntity();
		entity.setCategoryId(categoryId);
		return this.typoDao.searchCnt(entity);
	}

	/**
	 * 转换列表显示的一些内容
	 * 
	 * @param entity
	 */
	private void exchg(TypoEntity entity) {
		String content = entity.getContent();
		if (content.length() > 0) {
			content = content.replaceAll("<[^>]+>", "");
		}
		if (content.length() > 300) {
			content = content.substring(0, 300);
			content = content + "...";
		}
		entity.setShortContent(content);
		String date = entity.getAt();
		date = Helper.getShortDate3(date);
		entity.setAt(date);
	}
}
