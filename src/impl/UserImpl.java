package impl;

import image.ImageSize;
import image.Imager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import comm.Helper;
import dao.IUserDao;
import entity.SearchEntity;
import entity.UserEntity;

public class UserImpl {

	private IUserDao userDao = null;
	
	private NoteImpl noteService = null; 

	/**
	 * 登录
	 * 
	 * @param usrCode
	 * @param usrPwd
	 * @return >0 成功; -1 用户名不存在; -2 用户名被冻结; -3 用户名或密码错误;;
	 */
	public int login(String usrCode, String usrPwd) {
		int state = -3;
		UserEntity userEntity = this.getUserDao().login(usrCode);
		if (userEntity == null) {
			state = -1;
		} else {
			if (userEntity.getStatus() == 1) {
				if (userEntity.getUsrPwd().equals(usrPwd)) {
					// 登录成功
					return userEntity.getId();
				} else {
					return -3;
				}
			} else {
				state = -2;
			}
		}
		return state;
	}

	/**
	 * 获取信息
	 * 
	 * @param usrId
	 * @return
	 */
	public UserEntity info(int id, String host) {
		UserEntity user = this.getUserDao().info(id);
		if(user == null){
			return null;
		}
		String date = user.getAt();
		date = Helper.getShortDate1(date);
		user.setAt(date);
		int sex = user.getSex();
		if (sex == 0) {
			user.setSexString("女");
		} else if (sex == 1) {
			user.setSexString("男");
		} else {
			user.setSexString("保密");
		}
		String brithday = user.getBrithday();
		brithday = Helper.getShortDate1(date);
		user.setBrithday(brithday);
		int imgId = user.getUsrHd();
		if (imgId <= 0) {
			imgId = 700;
		}
		Imager imager = new Imager();
		user.setUsrHdString(imager.get(imgId, ImageSize.S, host));
		user.setUsrHdStringM(imager.get(imgId, ImageSize.M, host));
		user.setUsrHdStringL(imager.get(imgId, ImageSize.L, host));
		
		String skill = Helper.chgNewLine(user.getSkill());
		user.setSkill(skill);
		String rs = Helper.chgNewLine(user.getResearchStudy());
		user.setResearchStudy(rs);
		user.setNoteNum(this.noteService.cnt(id));
		return user;
	}
	
	/**
	 * 获取信息
	 * 
	 * @param usrId
	 * @return
	 */
	public List<String> skillList(int id) {
		UserEntity user = this.getUserDao().info(id);
		if(user == null){
			return null;
		}
		List<String> list = new ArrayList<String>();
		String skill = user.getSkill();
		if(skill != null){
			skill = skill.replaceAll(",", "，");
			String[] arrs = skill.split("，");
			for(int i=0;i<arrs.length;i++){
				String s = arrs[i];
				if(!Helper.isEmpty(s)){
					list.add(s);
				}
			}
		}
		return list;
	}
	
	

	/**
	 * 更新
	 * 
	 * @param usrId
	 * @param ip
	 * @return
	 */
	public int uptLastDate(int loginId, String ip) {
		UserEntity user = new UserEntity();
		user.setId(loginId);
		user.setLastIp(ip);
		return this.getUserDao().uptLastDate(user);
	}

	/**
	 * 列表
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	public List<UserEntity> list(int page, int size, String host) {
		int start = 0;
		start = (page - 1) * size;
		start = start > 0 ? start : 0;

		List<UserEntity> list = null;
		SearchEntity entity = new SearchEntity();
		entity.setStart(start);
		entity.setSize(size);
		list = this.userDao.list(entity);
		for (int i = 0; i < list.size(); i++) {
			// 处理
			String date = list.get(i).getAt();
			date = Helper.getShortDate1(date);
			list.get(i).setAt(date);
			int imgId = list.get(i).getUsrHd();
			if (imgId == 0) {
				imgId = 700;
			}
			String url = new Imager().get(imgId, ImageSize.S, host);
			list.get(i).setUsrHdString(url);
		}

		return list;
	}

	/**
	 * 计数
	 * 
	 * @return
	 */
	public int cnt() {
		return this.userDao.cnt();
	}

	/**
	 * 重置密码
	 * 
	 * @param usrId
	 * @param oPwd
	 * @param nPwd
	 * @param loginId
	 * @return
	 */
	public int reset(int id, String nPwd, int loginId) {
		// String pwd = this.userDao.getPwd(id);
		// if (pwd.equals(oPwd)) {
		UserEntity entity = new UserEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		entity.setUsrPwd(nPwd);
		int ret = this.userDao.reset(entity);
		return ret;
		// } else {
		// return -2;
		// }
	}

	/**
	 * 更改密码
	 * 
	 * @param loginId
	 * @param oPwd
	 * @param nPwd
	 * @return
	 */
	public int chgPwd(int loginId, String oPwd, String nPwd) {
		String pwd = this.userDao.getPwd(loginId);
		if (pwd.equals(oPwd)) {
			return reset(loginId, nPwd, loginId);
		}
		return -1;
	}
	
	/**
	 * 设定学科
	 * 
	 * @param usrId
	 * @param oPwd
	 * @param nPwd
	 * @param loginId
	 * @return
	 */
	public int uptSubt(String subtId, int loginId) {
		UserEntity entity = new UserEntity();
		entity.setSubtId(subtId);
		entity.setLoginId(loginId);
		int ret = this.userDao.uptSubt(entity);
		return ret;
	}
	
	/**
	 * 设定学科
	 * 
	 * @param usrId
	 * @param oPwd
	 * @param nPwd
	 * @param loginId
	 * @return
	 */
	public int uptDept(int universityId, int loginId) {
		UserEntity entity = new UserEntity();
		entity.setUniversity(universityId);
		entity.setLoginId(loginId);
		int ret = this.userDao.uptDept(entity);
		return ret;
	}

	/**
	 * 关闭
	 * 
	 * @param usrId
	 * @param oPwd
	 * @param nPwd
	 * @param loginId
	 * @return
	 */
	public int close(int id, int loginId) {
		UserEntity entity = new UserEntity();
		entity.setId(id);
		entity.setLoginId(loginId);
		int ret = this.userDao.close(entity);
		return ret;
	}

	/**
	 * 添加
	 * 
	 * @param entity
	 * @return
	 */
	public int add(UserEntity entity) {
		int ret = this.userDao.exist(entity.getUsrCode());
		if (ret > 0) {
			return -1;
		} else {
			ret = this.userDao.add(entity);
			this.userDao.addDetail(entity);
			return ret;
		}
	}

	/**
	 * 添加
	 * 
	 * @param entity
	 * @return
	 */
	public boolean exist(String code) {
		int ret = this.userDao.exist(code);
		if (ret > 0) {
			return false;
		}
		return true;
	}

	/**
	 * 更新
	 * 
	 * @param entity
	 * @return
	 */
	public int upt(UserEntity entity) {
		return this.userDao.upt(entity);
	}

	/**
	 * 更新
	 * 
	 * @param entity
	 * @return
	 */
	public int uptHd(int id, int imgId) {
		UserEntity entity = new UserEntity();
		entity.setId(id);
		entity.setLoginId(id);
		entity.setUsrHd(imgId);
		int ret = this.userDao.uptHd(entity);
		this.userDao.uptImg(entity);
		return ret;
	}
	
	/**
	 * 获取详细信息
	 * 
	 * @param usrId
	 * @return
	 */
	public UserEntity detailInfo(int id) {
		UserEntity user = this.getUserDao().detailInfo(id);
		return user;
	}
	
	/**
	 * 更新详细信息
	 * 
	 * @param entity
	 * @return
	 */
	public int uptDetailInfo(UserEntity entity) {
		return this.userDao.uptDetailInfo(entity);
	}

	/**
	 * 搜索好友
	 *usrRole 注册类型  0：个人注册；  1：课题组注册;如果要查询全部，则参数传null;
	 * @param loginId
	 * @param page
	 * @param size
	 * @return
	 */
	public List<UserEntity> search(int loginId,String q, int page, int size,String host, Integer usrRole, String uname) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginId", loginId);
		map.put("start", start);
		map.put("size", size);
		map.put("usrRole", usrRole);
		map.put("uname", uname);
		List<UserEntity> list = this.userDao.search(map);
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				String date = list.get(i).getAt();
				date = Helper.getShortDate1(date);
				list.get(i).setAt(date);
				int imgId = list.get(i).getUsrHd();
				if (imgId == 0) {
					imgId = 700;
				}
				String url = new Imager().get(imgId, ImageSize.L, host);
				list.get(i).setUsrHdStringM(url);
				
				String content = list.get(i).getResearchDirect();
				if (content != null && content.length() > 0) {
					content = content.replaceAll("<[^>]+>", "");
					if (content.length() > 120) {
						content = content.substring(0, 120);
						content = content + "...";
					}
				}
				list.get(i).setResearchDirect(content);
			}
		return list;
	}
	
	/**
	 * 统计数
	 * 
	 * @param loginId
	 * @return
	 */
	public int searchCnt(int loginId,String q, Integer usrRole, String uname) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginId", loginId);
		map.put("usrRole", usrRole);
		map.put("uname", uname);
		return this.userDao.searchCnt(map);
	}
	/**
	 * 根据课题组ID查询所有该课题组成员
	 * @param id
	 * @return
	 */
	public List<UserEntity> ktzList(int id, int page, int size, String uname,String univName) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ktzId", id);
		map.put("start", start);
		map.put("size", size);
		map.put("uname", uname);
		map.put("univName", univName);
		List<UserEntity> list = this.userDao.ktzList(map);
		return list;
	}
	
	public int ktzListCnt(int id, String uname,String univName) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ktzId", id);
		map.put("uname", uname);
		map.put("univName", univName);
		int cnt = this.userDao.ktzListCnt(map);
		return cnt;
	}
	
	/**
	 * k
	 * @param loginId
	 * @param q
	 * @param page
	 * @param size
	 * @param host
	 * @param usrRole
	 * @param uname
	 * @return
	 */
	public List<UserEntity> memberList(int page, int size, String uname, String univname) {
		int start = 0;
		start = (page - 1) * size;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("uname", uname);
		map.put("univname", univname);
		List<UserEntity> list = this.userDao.memberList(map);
		return list;
	}
	
	public int memberListCnt(String uname, String univname) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uname", uname);
		map.put("univname", univname);
		int cnt = this.userDao.memberListCnt(map);
		return cnt;
	}
	
	/**
	 * 添加删除课题组成员
	 * @param loginId
	 * @param ktzId
	 * @return
	 */
	public int uptUserKtz(int loginId, Integer ktzId, Integer headManFlag){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginId", loginId);
		map.put("ktzId", ktzId);
		map.put("headManFlag", headManFlag);
		return this.userDao.uptUserKtz(map);
	}
	
	/**
	 * 修改课题组成员身份
	 * @param loginId
	 * @param ktzId
	 * @return
	 */
	public int uptUserKtzHeadman(int loginId, Integer headManFlag){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginId", loginId);
		map.put("headManFlag", headManFlag);
		return this.userDao.uptUserKtzHeadman(map);
	}
	
	/**
	 * 查询课题组成员,最多查询3个，用在个人主页右边展示用
	 * @param ktzId
	 * @return
	 */
	public List<UserEntity> searchKtzMember(int ktzId, int headManFlag, String host){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ktzId", ktzId);
		map.put("headManFlag", headManFlag);
		List<UserEntity> list = this.userDao.searchKtzMember(map);
		if (null != list){
			for (int i = 0; i < list.size(); i++) {
				String date = list.get(i).getAt();
				date = Helper.getShortDate1(date);
				list.get(i).setAt(date);
				int imgId = list.get(i).getUsrHd();
				if (imgId == 0) {
					imgId = 700;
				}
				String url = new Imager().get(imgId, ImageSize.L, host);
				list.get(i).setUsrHdStringM(url);
			}
		}
		return list;
	}
	
	public IUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public NoteImpl getNoteService() {
		return noteService;
	}

	public void setNoteService(NoteImpl noteService) {
		this.noteService = noteService;
	}
}
